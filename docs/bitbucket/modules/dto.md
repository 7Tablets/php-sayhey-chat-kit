[php-sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [DTO](dto.md)

# Namespace: DTO

## Index

### Classes

* [Auth](../classes/dto.auth.md)
* [Conversation](../classes/dto.conversation.md)
* [ConversationConversableUsersBatch](../classes/dto.conversationconversableusersbatch.md)
* [ConversationInfo](../classes/dto.conversationinfo.md)
* [ConversationUser](../classes/dto.conversationuser.md)
* [ConversationUsersBasic](../classes/dto.conversationusersbasic.md)
* [ConversationUsersBatch](../classes/dto.conversationusersbatch.md)
* [DefaultGroupProfileImage](../classes/dto.defaultgroupprofileimage.md)
* [GroupConversation](../classes/dto.groupconversation.md)
* [GroupConversationInfo](../classes/dto.groupconversationinfo.md)
* [IndividualConversation](../classes/dto.individualconversation.md)
* [Message](../classes/dto.message.md)
* [MessageDeleted](../classes/dto.messagedeleted.md)
* [SystemMessage](../classes/dto.systemmessage.md)
* [User](../classes/dto.user.md)
* [UserConversationInfo](../classes/dto.userconversationinfo.md)
* [UserSelf](../classes/dto.userself.md)

### Type aliases

* [GenericConversation](dto.md#markdown-header-genericconversation)
* [Media](dto.md#markdown-header-media)

## Type aliases

###  GenericConversation

Ƭ **GenericConversation**: *[GroupConversation](../classes/dto.groupconversation.md) | [IndividualConversation](../classes/dto.individualconversation.md)*

Defined in types.ts:421

___

###  Media

Ƭ **Media**: *[FileBase](../globals.md#markdown-header-filebase)*

Defined in types.ts:362
