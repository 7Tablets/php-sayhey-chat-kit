[php-sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [ErrorType](errortype.md)

# Namespace: ErrorType

## Index

### Enumerations

* [AuthErrorType](../enums/errortype.autherrortype.md)
* [ConversationErrorType](../enums/errortype.conversationerrortype.md)
* [DateErrorType](../enums/errortype.dateerrortype.md)
* [FileErrorType](../enums/errortype.fileerrortype.md)
* [GeneralErrorType](../enums/errortype.generalerrortype.md)
* [InternalErrorType](../enums/errortype.internalerrortype.md)
* [MessageErrorType](../enums/errortype.messageerrortype.md)
* [ServerErrorType](../enums/errortype.servererrortype.md)
* [UserErrorType](../enums/errortype.usererrortype.md)

### Type aliases

* [Type](errortype.md#markdown-header-type)

## Type aliases

###  Type

Ƭ **Type**: *[InternalErrorType](../enums/errortype.internalerrortype.md) | [AuthErrorType](../enums/errortype.autherrortype.md) | [FileErrorType](../enums/errortype.fileerrortype.md) | [ServerErrorType](../enums/errortype.servererrortype.md) | [GeneralErrorType](../enums/errortype.generalerrortype.md) | [MessageErrorType](../enums/errortype.messageerrortype.md) | [UserErrorType](../enums/errortype.usererrortype.md) | [ConversationErrorType](../enums/errortype.conversationerrortype.md)*

Defined in types.ts:18
