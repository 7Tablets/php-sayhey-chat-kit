[php-sayhey-chat-kit](README.md) › [Globals](globals.md)

# php-sayhey-chat-kit

## Index

### Namespaces

* [DTO](modules/dto.md)
* [Domain](modules/domain.md)
* [ErrorType](modules/errortype.md)

### Enumerations

* [ConversationType](enums/conversationtype.md)
* [ConversationUserQueryType](enums/conversationuserquerytype.md)
* [FileType](enums/filetype.md)
* [HttpMethod](enums/httpmethod.md)
* [ImageSizes](enums/imagesizes.md)
* [MessageType](enums/messagetype.md)
* [ReactionType](enums/reactiontype.md)
* [SystemMessageType](enums/systemmessagetype.md)

### Classes

* [AppError](classes/apperror.md)
* [AppMapper](classes/appmapper.md)
* [AudioBase](classes/audiobase.md)
* [AuthBase](classes/authbase.md)
* [ChatKitServer](classes/chatkitserver.md)
* [ConversationBase](classes/conversationbase.md)
* [ConversationConversableUsersBatchMeta](classes/conversationconversableusersbatchmeta.md)
* [ConversationMapper](classes/conversationmapper.md)
* [ConversationUsersBatchBase](classes/conversationusersbatchbase.md)
* [ConversationUsersBatchMeta](classes/conversationusersbatchmeta.md)
* [DefaultGroupProfileImageBase](classes/defaultgroupprofileimagebase.md)
* [DocumentBase](classes/documentbase.md)
* [ImageBase](classes/imagebase.md)
* [ImageInfoMedia](classes/imageinfomedia.md)
* [MediaBase](classes/mediabase.md)
* [MediaMapper](classes/mediamapper.md)
* [MessageBase](classes/messagebase.md)
* [MessageMapper](classes/messagemapper.md)
* [Paginatable](classes/paginatable.md)
* [ReactionsObject](classes/reactionsobject.md)
* [SubImageMinimal](classes/subimageminimal.md)
* [SystemMessageBase](classes/systemmessagebase.md)
* [UserBase](classes/userbase.md)
* [UserDetail](classes/userdetail.md)
* [UserMapper](classes/usermapper.md)
* [UserSimpleInfo](classes/usersimpleinfo.md)
* [VideoBase](classes/videobase.md)

### Type aliases

* [FileBase](globals.md#markdown-header-filebase)

### Variables

* [API_KEY_NAME](globals.md#markdown-header-const-api_key_name)
* [API_VERSION](globals.md#markdown-header-const-api_version)

### Functions

* [checkApiKey](globals.md#markdown-header-checkapikey)
* [requestFailed](globals.md#markdown-header-requestfailed)

### Object literals

* [Routes](globals.md#markdown-header-const-routes)

## Type aliases

###  FileBase

Ƭ **FileBase**: *[ImageBase](classes/imagebase.md) | [VideoBase](classes/videobase.md) | [AudioBase](classes/audiobase.md) | [DocumentBase](classes/documentbase.md)*

Defined in types.ts:281

## Variables

### `Const` API_KEY_NAME

• **API_KEY_NAME**: *"x-sayhey-server-api-key"* = "x-sayhey-server-api-key"

Defined in constants.ts:3

___

### `Const` API_VERSION

• **API_VERSION**: *"v1"* = "v1"

Defined in constants.ts:4

## Functions

###  checkApiKey

▸ **checkApiKey**(`config`: AxiosRequestConfig): *AxiosRequestConfig*

Defined in axios.ts:5

**Parameters:**

Name | Type |
------ | ------ |
`config` | AxiosRequestConfig |

**Returns:** *AxiosRequestConfig*

___

###  requestFailed

▸ **requestFailed**(`error`: AxiosError): *void*

Defined in axios.ts:12

**Parameters:**

Name | Type |
------ | ------ |
`error` | AxiosError |

**Returns:** *void*

## Object literals

### `Const` Routes

### ▪ **Routes**: *object*

Defined in constants.ts:6

▪ **AddUsersToConversation**: *object*

Defined in constants.ts:56

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Post

* **Endpoint**(`conversationId`: string): *string*

▪ **CreateGroupWithOwner**: *object*

Defined in constants.ts:78

* **Endpoint**: *string* = "/conversations/group-with-owner"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Post

▪ **DeleteAllUsersInConversation**: *object*

Defined in constants.ts:74

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Post

* **Endpoint**(`conversationId`: string): *string*

▪ **DeleteMessage**: *object*

Defined in constants.ts:110

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Delete

* **Endpoint**(`messageId`: string): *string*

▪ **DeleteUser**: *object*

Defined in constants.ts:114

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Delete

* **Endpoint**(`userId`: string): *string*

▪ **DisableUser**: *object*

Defined in constants.ts:98

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Put

* **Endpoint**(`userId`: string): *string*

▪ **EnableUser**: *object*

Defined in constants.ts:102

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Delete

* **Endpoint**(`userId`: string): *string*

▪ **GetConversationBasicUsers**: *object*

Defined in constants.ts:94

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

* **Endpoint**(`conversationId`: string): *string*

▪ **GetConversationConversableUsers**: *object*

Defined in constants.ts:48

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

* **Endpoint**(`conversationId`: string): *string*

▪ **GetConversationGroupImage**: *object*

Defined in constants.ts:118

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

* **Endpoint**(`conversationId`: string): *string*

▪ **GetConversationUsers**: *object*

Defined in constants.ts:52

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

* **Endpoint**(`conversationId`: string): *string*

▪ **GetDefaultProfileImagesForGroup**: *object*

Defined in constants.ts:7

* **Endpoint**: *string* = "/default-group-images"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

▪ **GetFileMessage**: *object*

Defined in constants.ts:122

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

* **Endpoint**(`messageId`: string, `fileLocalIdName`: string): *string*

▪ **GetMessages**: *object*

Defined in constants.ts:106

* **Endpoint**: *string* = "/messages"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

▪ **GetUserProfile**: *object*

Defined in constants.ts:28

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

* **Endpoint**(`userId`: string): *string*

▪ **GetVideoThumbnail**: *object*

Defined in constants.ts:127

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

* **Endpoint**(`messageId`: string, `fileLocalIdName`: string): *string*

▪ **MakeConversationOwner**: *object*

Defined in constants.ts:64

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Put

* **Endpoint**(`conversationId`: string, `userId`: string): *string*

▪ **RemoveConversationOwner**: *object*

Defined in constants.ts:69

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Delete

* **Endpoint**(`conversationId`: string, `userId`: string): *string*

▪ **RemoveGroupImage**: *object*

Defined in constants.ts:90

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Delete

* **Endpoint**(`conversationId`: string): *string*

▪ **RemoveUserProfileImage**: *object*

Defined in constants.ts:40

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Delete

* **Endpoint**(`userId`: string): *string*

▪ **RemoveUsersFromConversation**: *object*

Defined in constants.ts:60

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Post

* **Endpoint**(`conversationId`: string): *string*

▪ **SignInUserWithEmail**: *object*

Defined in constants.ts:20

* **Endpoint**: *string* = "/users/server-sign-in"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Post

▪ **SignInUserWithId**: *object*

Defined in constants.ts:24

* **Endpoint**: *string* = "/users/server-sign-in-id"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Post

▪ **SignUpUser**: *object*

Defined in constants.ts:16

* **Endpoint**: *string* = "/users/server-sign-up"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Post

▪ **UpdateGroupImage**: *object*

Defined in constants.ts:86

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Put

* **Endpoint**(`conversationId`: string): *string*

▪ **UpdateGroupInfo**: *object*

Defined in constants.ts:82

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Put

* **Endpoint**(`conversationId`: string): *string*

▪ **UpdateUserProfileImage**: *object*

Defined in constants.ts:36

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Put

* **Endpoint**(`userId`: string): *string*

▪ **UpdateUserProfileInfo**: *object*

Defined in constants.ts:32

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Put

* **Endpoint**(`userId`: string): *string*

▪ **UploadFile**: *object*

Defined in constants.ts:44

* **Endpoint**: *string* = "/files"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Post

▪ **ViewDefaultProfileImageForGroup**: *object*

Defined in constants.ts:11

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

* **Endpoint**(`filename`: string, `queryString`: string): *string*
