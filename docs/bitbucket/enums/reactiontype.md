[php-sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [ReactionType](reactiontype.md)

# Enumeration: ReactionType

## Index

### Enumeration members

* [Fist](reactiontype.md#markdown-header-fist)
* [Heart](reactiontype.md#markdown-header-heart)
* [Thumb](reactiontype.md#markdown-header-thumb)

## Enumeration members

###  Fist

• **Fist**: = "Fist"

Defined in types.ts:326

___

###  Heart

• **Heart**: = "Heart"

Defined in types.ts:325

___

###  Thumb

• **Thumb**: = "Thumb"

Defined in types.ts:324
