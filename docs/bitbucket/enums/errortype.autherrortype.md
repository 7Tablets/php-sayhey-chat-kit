[php-sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [ErrorType](../modules/errortype.md) › [AuthErrorType](errortype.autherrortype.md)

# Enumeration: AuthErrorType

## Index

### Enumeration members

* [AccountAlreadyExists](errortype.autherrortype.md#markdown-header-accountalreadyexists)
* [AppHasNoNotificationSetup](errortype.autherrortype.md#markdown-header-apphasnonotificationsetup)
* [HashComparison](errortype.autherrortype.md#markdown-header-hashcomparison)
* [HashGeneration](errortype.autherrortype.md#markdown-header-hashgeneration)
* [IncorrectPassword](errortype.autherrortype.md#markdown-header-incorrectpassword)
* [InvalidCredentials](errortype.autherrortype.md#markdown-header-invalidcredentials)
* [InvalidKeyErrorType](errortype.autherrortype.md#markdown-header-invalidkeyerrortype)
* [InvalidPassword](errortype.autherrortype.md#markdown-header-invalidpassword)
* [InvalidToken](errortype.autherrortype.md#markdown-header-invalidtoken)
* [InvalidUserType](errortype.autherrortype.md#markdown-header-invalidusertype)
* [InvalidWebhookKey](errortype.autherrortype.md#markdown-header-invalidwebhookkey)
* [MissingAuthHeaderField](errortype.autherrortype.md#markdown-header-missingauthheaderfield)
* [MissingParamsErrorType](errortype.autherrortype.md#markdown-header-missingparamserrortype)
* [NoAccessToUser](errortype.autherrortype.md#markdown-header-noaccesstouser)
* [NoAppAccessToUser](errortype.autherrortype.md#markdown-header-noappaccesstouser)
* [NoSamePassword](errortype.autherrortype.md#markdown-header-nosamepassword)
* [NotFoundErrorType](errortype.autherrortype.md#markdown-header-notfounderrortype)
* [NotPartOfApp](errortype.autherrortype.md#markdown-header-notpartofapp)
* [RefreshTokenAlreadyExchanged](errortype.autherrortype.md#markdown-header-refreshtokenalreadyexchanged)
* [RefreshTokenNotFound](errortype.autherrortype.md#markdown-header-refreshtokennotfound)
* [ServerNoAccessToUser](errortype.autherrortype.md#markdown-header-servernoaccesstouser)
* [TokenExpiredType](errortype.autherrortype.md#markdown-header-tokenexpiredtype)
* [TokenNotStarted](errortype.autherrortype.md#markdown-header-tokennotstarted)
* [UserDisabled](errortype.autherrortype.md#markdown-header-userdisabled)
* [UserNotFound](errortype.autherrortype.md#markdown-header-usernotfound)

## Enumeration members

###  AccountAlreadyExists

• **AccountAlreadyExists**: = "AuthErrorTypeAccountAlreadyExists"

Defined in types.ts:44

___

###  AppHasNoNotificationSetup

• **AppHasNoNotificationSetup**: = "AuthErrorTypeAppHasNoNotificationSetup"

Defined in types.ts:35

___

###  HashComparison

• **HashComparison**: = "AuthErrorTypeHashComparison"

Defined in types.ts:50

___

###  HashGeneration

• **HashGeneration**: = "AuthErrorTypeHashGeneration"

Defined in types.ts:49

___

###  IncorrectPassword

• **IncorrectPassword**: = "AuthErrorTypeIncorrectPassword"

Defined in types.ts:40

___

###  InvalidCredentials

• **InvalidCredentials**: = "AuthErrorTypeInvalidCredentials"

Defined in types.ts:48

___

###  InvalidKeyErrorType

• **InvalidKeyErrorType**: = "AuthErrorTypeInvalidKey"

Defined in types.ts:29

___

###  InvalidPassword

• **InvalidPassword**: = "AuthErrorTypeInvalidPassword"

Defined in types.ts:33

___

###  InvalidToken

• **InvalidToken**: = "AuthErrorTypeInvalidToken"

Defined in types.ts:46

___

###  InvalidUserType

• **InvalidUserType**: = "AuthErrorTypeInvalidUserType"

Defined in types.ts:30

___

###  InvalidWebhookKey

• **InvalidWebhookKey**: = "AuthErrorTypeInvalidWebhookKey"

Defined in types.ts:37

___

###  MissingAuthHeaderField

• **MissingAuthHeaderField**: = "AuthErrorTypeMissingAuthHeaderField"

Defined in types.ts:47

___

###  MissingParamsErrorType

• **MissingParamsErrorType**: = "AuthErrorTypeMissingParams"

Defined in types.ts:32

___

###  NoAccessToUser

• **NoAccessToUser**: = "AuthErrorTypeNoAccessToUser"

Defined in types.ts:38

___

###  NoAppAccessToUser

• **NoAppAccessToUser**: = "AuthErrorTypeNoAppAccessToUser"

Defined in types.ts:42

___

###  NoSamePassword

• **NoSamePassword**: = "AuthErrorTypeNoSamePassword"

Defined in types.ts:41

___

###  NotFoundErrorType

• **NotFoundErrorType**: = "AuthErrorTypeNotFound"

Defined in types.ts:31

___

###  NotPartOfApp

• **NotPartOfApp**: = "AuthErrorTypeNotPartOfApp"

Defined in types.ts:36

___

###  RefreshTokenAlreadyExchanged

• **RefreshTokenAlreadyExchanged**: = "AuthErrorTypeRefreshTokenAlreadyExchanged"

Defined in types.ts:51

___

###  RefreshTokenNotFound

• **RefreshTokenNotFound**: = "AuthErrorTypeRefreshTokenNotFound"

Defined in types.ts:52

___

###  ServerNoAccessToUser

• **ServerNoAccessToUser**: = "AuthErrorTypeServerNoAccessToUser"

Defined in types.ts:39

___

###  TokenExpiredType

• **TokenExpiredType**: = "AuthErrorTypeTokenExpired"

Defined in types.ts:34

___

###  TokenNotStarted

• **TokenNotStarted**: = "AuthErrorTypeTokenNotStarted"

Defined in types.ts:43

___

###  UserDisabled

• **UserDisabled**: = "AuthErrorTypeUserDisabled"

Defined in types.ts:53

___

###  UserNotFound

• **UserNotFound**: = "AuthErrorTypeUserNotFound"

Defined in types.ts:45
