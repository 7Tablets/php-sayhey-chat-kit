[php-sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [ImageSizes](imagesizes.md)

# Enumeration: ImageSizes

## Index

### Enumeration members

* [Large](imagesizes.md#markdown-header-large)
* [Medium](imagesizes.md#markdown-header-medium)
* [Small](imagesizes.md#markdown-header-small)
* [Tiny](imagesizes.md#markdown-header-tiny)
* [XLarge](imagesizes.md#markdown-header-xlarge)

## Enumeration members

###  Large

• **Large**: = "large"

Defined in types.ts:196

___

###  Medium

• **Medium**: = "medium"

Defined in types.ts:195

___

###  Small

• **Small**: = "small"

Defined in types.ts:194

___

###  Tiny

• **Tiny**: = "tiny"

Defined in types.ts:193

___

###  XLarge

• **XLarge**: = "xlarge"

Defined in types.ts:197
