[php-sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [HttpMethod](httpmethod.md)

# Enumeration: HttpMethod

## Index

### Enumeration members

* [Delete](httpmethod.md#markdown-header-delete)
* [Get](httpmethod.md#markdown-header-get)
* [Patch](httpmethod.md#markdown-header-patch)
* [Post](httpmethod.md#markdown-header-post)
* [Put](httpmethod.md#markdown-header-put)

## Enumeration members

###  Delete

• **Delete**: = "delete"

Defined in types.ts:4

___

###  Get

• **Get**: = "get"

Defined in types.ts:3

___

###  Patch

• **Patch**: = "patch"

Defined in types.ts:5

___

###  Post

• **Post**: = "post"

Defined in types.ts:2

___

###  Put

• **Put**: = "put"

Defined in types.ts:6
