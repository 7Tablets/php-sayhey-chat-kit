[php-sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [ErrorType](../modules/errortype.md) › [UserErrorType](errortype.usererrortype.md)

# Enumeration: UserErrorType

## Index

### Enumeration members

* [AlreadyDisabled](errortype.usererrortype.md#markdown-header-alreadydisabled)
* [AlreadyEnabled](errortype.usererrortype.md#markdown-header-alreadyenabled)
* [AlreadyMuted](errortype.usererrortype.md#markdown-header-alreadymuted)
* [NotOnMute](errortype.usererrortype.md#markdown-header-notonmute)

## Enumeration members

###  AlreadyDisabled

• **AlreadyDisabled**: = "UserErrorTypeAlreadyDisabled"

Defined in types.ts:97

___

###  AlreadyEnabled

• **AlreadyEnabled**: = "UserErrorTypeAlreadyEnabled"

Defined in types.ts:96

___

###  AlreadyMuted

• **AlreadyMuted**: = "UserErrorTypeAlreadyMuted"

Defined in types.ts:94

___

###  NotOnMute

• **NotOnMute**: = "UserErrorTypeNotOnMute"

Defined in types.ts:95
