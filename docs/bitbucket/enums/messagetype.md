[php-sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [MessageType](messagetype.md)

# Enumeration: MessageType

## Index

### Enumeration members

* [Audio](messagetype.md#markdown-header-audio)
* [Document](messagetype.md#markdown-header-document)
* [Emoji](messagetype.md#markdown-header-emoji)
* [Gif](messagetype.md#markdown-header-gif)
* [Image](messagetype.md#markdown-header-image)
* [System](messagetype.md#markdown-header-system)
* [Text](messagetype.md#markdown-header-text)
* [Video](messagetype.md#markdown-header-video)

## Enumeration members

###  Audio

• **Audio**: = "audio"

Defined in types.ts:320

___

###  Document

• **Document**: = "document"

Defined in types.ts:317

___

###  Emoji

• **Emoji**: = "emoji"

Defined in types.ts:319

___

###  Gif

• **Gif**: = "gif"

Defined in types.ts:318

___

###  Image

• **Image**: = "image"

Defined in types.ts:315

___

###  System

• **System**: = "system"

Defined in types.ts:313

___

###  Text

• **Text**: = "text"

Defined in types.ts:314

___

###  Video

• **Video**: = "video"

Defined in types.ts:316
