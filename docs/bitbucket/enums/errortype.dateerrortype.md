[php-sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [ErrorType](../modules/errortype.md) › [DateErrorType](errortype.dateerrortype.md)

# Enumeration: DateErrorType

## Index

### Enumeration members

* [InvalidFromDateError](errortype.dateerrortype.md#markdown-header-invalidfromdateerror)
* [InvalidToDateError](errortype.dateerrortype.md#markdown-header-invalidtodateerror)

## Enumeration members

###  InvalidFromDateError

• **InvalidFromDateError**: = "DateErrorTypeInvalidFromDateError"

Defined in types.ts:90

___

###  InvalidToDateError

• **InvalidToDateError**: = "DateErrorTypeInvalidToDateError"

Defined in types.ts:91
