[php-sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [ErrorType](../modules/errortype.md) › [ConversationErrorType](errortype.conversationerrortype.md)

# Enumeration: ConversationErrorType

## Index

### Enumeration members

* [AlreadyAnOwner](errortype.conversationerrortype.md#markdown-header-alreadyanowner)
* [AlreadyExists](errortype.conversationerrortype.md#markdown-header-alreadyexists)
* [AlreadyMuted](errortype.conversationerrortype.md#markdown-header-alreadymuted)
* [AlreadyPinned](errortype.conversationerrortype.md#markdown-header-alreadypinned)
* [AlreadyRemovedUsers](errortype.conversationerrortype.md#markdown-header-alreadyremovedusers)
* [CannotAddExistingUsers](errortype.conversationerrortype.md#markdown-header-cannotaddexistingusers)
* [CannotChangeAccessToSelf](errortype.conversationerrortype.md#markdown-header-cannotchangeaccesstoself)
* [CannotChatWithSelf](errortype.conversationerrortype.md#markdown-header-cannotchatwithself)
* [CannotConverseWithUser](errortype.conversationerrortype.md#markdown-header-cannotconversewithuser)
* [CannotRemoveOwners](errortype.conversationerrortype.md#markdown-header-cannotremoveowners)
* [InvalidConversationUserType](errortype.conversationerrortype.md#markdown-header-invalidconversationusertype)
* [InvalidType](errortype.conversationerrortype.md#markdown-header-invalidtype)
* [MustBeOwner](errortype.conversationerrortype.md#markdown-header-mustbeowner)
* [NoValidUsersToAdd](errortype.conversationerrortype.md#markdown-header-novaliduserstoadd)
* [NoValidUsersToRemove](errortype.conversationerrortype.md#markdown-header-novaliduserstoremove)
* [NotAnOwner](errortype.conversationerrortype.md#markdown-header-notanowner)
* [NotFound](errortype.conversationerrortype.md#markdown-header-notfound)
* [NotFoundForApp](errortype.conversationerrortype.md#markdown-header-notfoundforapp)
* [NotFoundForUser](errortype.conversationerrortype.md#markdown-header-notfoundforuser)
* [NotOnMute](errortype.conversationerrortype.md#markdown-header-notonmute)
* [NotPartOfApp](errortype.conversationerrortype.md#markdown-header-notpartofapp)
* [NotPinned](errortype.conversationerrortype.md#markdown-header-notpinned)
* [UsersNotInApp](errortype.conversationerrortype.md#markdown-header-usersnotinapp)
* [UsersNotInConversation](errortype.conversationerrortype.md#markdown-header-usersnotinconversation)

## Enumeration members

###  AlreadyAnOwner

• **AlreadyAnOwner**: = "ConversationErrorTypeAlreadyAnOwner"

Defined in types.ts:103

___

###  AlreadyExists

• **AlreadyExists**: = "ConversationErrorTypeAlreadyExists"

Defined in types.ts:110

___

###  AlreadyMuted

• **AlreadyMuted**: = "ConversationErrorTypeAlreadyMuted"

Defined in types.ts:112

___

###  AlreadyPinned

• **AlreadyPinned**: = "ConversationErrorTypeAlreadyPinned"

Defined in types.ts:114

___

###  AlreadyRemovedUsers

• **AlreadyRemovedUsers**: = "ConversationErrorTypeAlreadyRemovedUsers"

Defined in types.ts:121

___

###  CannotAddExistingUsers

• **CannotAddExistingUsers**: = "ConversationErrorTypeCannotAddExistingUsers"

Defined in types.ts:120

___

###  CannotChangeAccessToSelf

• **CannotChangeAccessToSelf**: = "ConversationErrorTypeCannotChangeAccessToSelf"

Defined in types.ts:119

___

###  CannotChatWithSelf

• **CannotChatWithSelf**: = "ConversationErrorTypeCannotChatWithSelf"

Defined in types.ts:111

___

###  CannotConverseWithUser

• **CannotConverseWithUser**: = "ConversationErrorTypeCannotConverseWithUser"

Defined in types.ts:101

___

###  CannotRemoveOwners

• **CannotRemoveOwners**: = "ConversationErrorTypeCannotRemoveOwners"

Defined in types.ts:123

___

###  InvalidConversationUserType

• **InvalidConversationUserType**: = "ConversationErrorTypeInvalidConversationUserType"

Defined in types.ts:106

___

###  InvalidType

• **InvalidType**: = "ConversationErrorTypeInvalidType"

Defined in types.ts:105

___

###  MustBeOwner

• **MustBeOwner**: = "ConversationErrorTypeMustBeOwner"

Defined in types.ts:107

___

###  NoValidUsersToAdd

• **NoValidUsersToAdd**: = "ConversationErrorTypeNoValidUsersToAdd"

Defined in types.ts:117

___

###  NoValidUsersToRemove

• **NoValidUsersToRemove**: = "ConversationErrorTypeNoValidUsersToRemove"

Defined in types.ts:118

___

###  NotAnOwner

• **NotAnOwner**: = "ConversationErrorTypeNotAnOwner"

Defined in types.ts:102

___

###  NotFound

• **NotFound**: = "ConversationErrorTypeNotFound"

Defined in types.ts:100

___

###  NotFoundForApp

• **NotFoundForApp**: = "ConversationErrorTypeNotFoundForApp"

Defined in types.ts:104

___

###  NotFoundForUser

• **NotFoundForUser**: = "ConversationErrorTypeNotFoundForUser"

Defined in types.ts:109

___

###  NotOnMute

• **NotOnMute**: = "ConversationErrorTypeNotOnMute"

Defined in types.ts:113

___

###  NotPartOfApp

• **NotPartOfApp**: = "ConversationErrorTypeNotPartOfApp"

Defined in types.ts:108

___

###  NotPinned

• **NotPinned**: = "ConversationErrorTypeNotPinned"

Defined in types.ts:115

___

###  UsersNotInApp

• **UsersNotInApp**: = "ConversationErrorTypeUsersNotInApp"

Defined in types.ts:116

___

###  UsersNotInConversation

• **UsersNotInConversation**: = "ConversationErrorTypeUsersNotInConversation"

Defined in types.ts:122
