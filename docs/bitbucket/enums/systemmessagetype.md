[php-sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [SystemMessageType](systemmessagetype.md)

# Enumeration: SystemMessageType

## Index

### Enumeration members

* [GroupCreated](systemmessagetype.md#markdown-header-groupcreated)
* [GroupDisabled](systemmessagetype.md#markdown-header-groupdisabled)
* [GroupNameChanged](systemmessagetype.md#markdown-header-groupnamechanged)
* [GroupPictureChanged](systemmessagetype.md#markdown-header-grouppicturechanged)
* [IndividualChatCreated](systemmessagetype.md#markdown-header-individualchatcreated)
* [MemberAdded](systemmessagetype.md#markdown-header-memberadded)
* [MemberRemoved](systemmessagetype.md#markdown-header-memberremoved)
* [OwnerAdded](systemmessagetype.md#markdown-header-owneradded)
* [OwnerRemoved](systemmessagetype.md#markdown-header-ownerremoved)

## Enumeration members

###  GroupCreated

• **GroupCreated**: = "GroupCreated"

Defined in types.ts:291

___

###  GroupDisabled

• **GroupDisabled**: = "GroupDisabled"

Defined in types.ts:290

___

###  GroupNameChanged

• **GroupNameChanged**: = "GroupNameChanged"

Defined in types.ts:289

___

###  GroupPictureChanged

• **GroupPictureChanged**: = "GroupPictureChanged"

Defined in types.ts:288

___

###  IndividualChatCreated

• **IndividualChatCreated**: = "IndividualChatCreated"

Defined in types.ts:292

___

###  MemberAdded

• **MemberAdded**: = "MemberAdded"

Defined in types.ts:285

___

###  MemberRemoved

• **MemberRemoved**: = "MemberRemoved"

Defined in types.ts:284

___

###  OwnerAdded

• **OwnerAdded**: = "OwnerAdded"

Defined in types.ts:287

___

###  OwnerRemoved

• **OwnerRemoved**: = "OwnerRemoved"

Defined in types.ts:286
