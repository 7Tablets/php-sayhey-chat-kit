[php-sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [ImageInfoMedia](imageinfomedia.md)

# Class: ImageInfoMedia

## Hierarchy

* **ImageInfoMedia**

## Index

### Properties

* [height](imageinfomedia.md#markdown-header-height)
* [subImagesInfo](imageinfomedia.md#markdown-header-subimagesinfo)
* [width](imageinfomedia.md#markdown-header-width)

## Properties

###  height

• **height**: *number*

Defined in types.ts:209

___

###  subImagesInfo

• **subImagesInfo**: *[SubImageMinimal](subimageminimal.md)[]*

Defined in types.ts:210

___

###  width

• **width**: *number*

Defined in types.ts:208
