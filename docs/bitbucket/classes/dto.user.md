[php-sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [DTO](../modules/dto.md) › [User](dto.user.md)

# Class: User

## Hierarchy

  ↳ [UserDetail](userdetail.md)

  ↳ **User**

  ↳ [UserSelf](dto.userself.md)

  ↳ [ConversationUser](dto.conversationuser.md)

## Index

### Properties

* [deletedAt](dto.user.md#markdown-header-deletedat)
* [disabled](dto.user.md#markdown-header-disabled)
* [email](dto.user.md#markdown-header-email)
* [firstName](dto.user.md#markdown-header-firstname)
* [id](dto.user.md#markdown-header-id)
* [lastName](dto.user.md#markdown-header-lastname)
* [picture](dto.user.md#markdown-header-picture)
* [refId](dto.user.md#markdown-header-refid)

## Properties

###  deletedAt

• **deletedAt**: *string | null*

Defined in types.ts:366

___

###  disabled

• **disabled**: *boolean*

Defined in types.ts:365

___

###  email

• **email**: *string*

*Inherited from [UserDetail](userdetail.md).[email](userdetail.md#markdown-header-email)*

Defined in types.ts:182

___

###  firstName

• **firstName**: *string*

*Inherited from [UserBase](userbase.md).[firstName](userbase.md#markdown-header-firstname)*

Defined in types.ts:176

___

###  id

• **id**: *string*

*Inherited from [UserBase](userbase.md).[id](userbase.md#markdown-header-id)*

Defined in types.ts:175

___

###  lastName

• **lastName**: *string*

*Inherited from [UserBase](userbase.md).[lastName](userbase.md#markdown-header-lastname)*

Defined in types.ts:177

___

###  picture

• **picture**: *[Media](../modules/dto.md#markdown-header-media) | null*

Defined in types.ts:364

___

###  refId

• **refId**: *string | null*

*Inherited from [UserBase](userbase.md).[refId](userbase.md#markdown-header-refid)*

Defined in types.ts:178
