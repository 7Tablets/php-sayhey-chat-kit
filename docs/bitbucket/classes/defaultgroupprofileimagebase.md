[php-sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [DefaultGroupProfileImageBase](defaultgroupprofileimagebase.md)

# Class: DefaultGroupProfileImageBase

## Hierarchy

* **DefaultGroupProfileImageBase**

  ↳ [DefaultGroupProfileImage](dto.defaultgroupprofileimage.md)

  ↳ [DefaultGroupProfileImage](domain.defaultgroupprofileimage.md)

## Index

### Properties

* [filename](defaultgroupprofileimagebase.md#markdown-header-filename)
* [original](defaultgroupprofileimagebase.md#markdown-header-original)
* [thumbnail](defaultgroupprofileimagebase.md#markdown-header-thumbnail)

## Properties

###  filename

• **filename**: *string*

Defined in types.ts:163

___

###  original

• **original**: *string*

Defined in types.ts:164

___

###  thumbnail

• **thumbnail**: *string*

Defined in types.ts:165
