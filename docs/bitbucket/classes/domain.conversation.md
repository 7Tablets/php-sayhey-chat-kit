[php-sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [Domain](../modules/domain.md) › [Conversation](domain.conversation.md)

# Class: Conversation

## Hierarchy

* [ConversationBase](conversationbase.md)

  ↳ **Conversation**

  ↳ [GroupConversation](domain.groupconversation.md)

  ↳ [IndividualConversation](domain.individualconversation.md)

## Index

### Properties

* [badge](domain.conversation.md#markdown-header-badge)
* [createdAt](domain.conversation.md#markdown-header-createdat)
* [deletedFromConversationAt](domain.conversation.md#markdown-header-deletedfromconversationat)
* [endBefore](domain.conversation.md#markdown-header-endbefore)
* [id](domain.conversation.md#markdown-header-id)
* [isOwner](domain.conversation.md#markdown-header-isowner)
* [lastMessage](domain.conversation.md#markdown-header-lastmessage)
* [lastReadMessageId](domain.conversation.md#markdown-header-lastreadmessageid)
* [muteUntil](domain.conversation.md#markdown-header-muteuntil)
* [pinnedAt](domain.conversation.md#markdown-header-pinnedat)
* [serverCreated](domain.conversation.md#markdown-header-servercreated)
* [serverManaged](domain.conversation.md#markdown-header-servermanaged)
* [startAfter](domain.conversation.md#markdown-header-startafter)
* [type](domain.conversation.md#markdown-header-type)
* [updatedAt](domain.conversation.md#markdown-header-updatedat)
* [visible](domain.conversation.md#markdown-header-visible)

## Properties

###  badge

• **badge**: *number | null*

Defined in types.ts:480

___

###  createdAt

• **createdAt**: *Date*

Defined in types.ts:484

___

###  deletedFromConversationAt

• **deletedFromConversationAt**: *Date | null*

Defined in types.ts:483

___

###  endBefore

• **endBefore**: *Date | null*

Defined in types.ts:482

___

###  id

• **id**: *string*

*Inherited from [ConversationBase](conversationbase.md).[id](conversationbase.md#markdown-header-id)*

Defined in types.ts:350

___

###  isOwner

• **isOwner**: *boolean*

*Inherited from [ConversationBase](conversationbase.md).[isOwner](conversationbase.md#markdown-header-isowner)*

Defined in types.ts:353

___

###  lastMessage

• **lastMessage**: *[Message](domain.message.md) | null*

Defined in types.ts:486

___

###  lastReadMessageId

• **lastReadMessageId**: *string | null*

*Inherited from [ConversationBase](conversationbase.md).[lastReadMessageId](conversationbase.md#markdown-header-lastreadmessageid)*

Defined in types.ts:351

___

###  muteUntil

• **muteUntil**: *Date | null*

Defined in types.ts:478

___

###  pinnedAt

• **pinnedAt**: *Date | null*

Defined in types.ts:479

___

###  serverCreated

• **serverCreated**: *boolean*

*Inherited from [ConversationBase](conversationbase.md).[serverCreated](conversationbase.md#markdown-header-servercreated)*

Defined in types.ts:355

___

###  serverManaged

• **serverManaged**: *boolean*

*Inherited from [ConversationBase](conversationbase.md).[serverManaged](conversationbase.md#markdown-header-servermanaged)*

Defined in types.ts:356

___

###  startAfter

• **startAfter**: *Date | null*

Defined in types.ts:481

___

###  type

• **type**: *[Group](../enums/conversationtype.md#markdown-header-group) | [Individual](../enums/conversationtype.md#markdown-header-individual)*

*Inherited from [ConversationBase](conversationbase.md).[type](conversationbase.md#markdown-header-type)*

Defined in types.ts:352

___

###  updatedAt

• **updatedAt**: *Date*

Defined in types.ts:485

___

###  visible

• **visible**: *boolean*

*Inherited from [ConversationBase](conversationbase.md).[visible](conversationbase.md#markdown-header-visible)*

Defined in types.ts:354
