[php-sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [UserSimpleInfo](usersimpleinfo.md)

# Class: UserSimpleInfo

## Hierarchy

* **UserSimpleInfo**

## Index

### Properties

* [firstName](usersimpleinfo.md#markdown-header-firstname)
* [id](usersimpleinfo.md#markdown-header-id)
* [lastName](usersimpleinfo.md#markdown-header-lastname)
* [refId](usersimpleinfo.md#markdown-header-refid)

## Properties

###  firstName

• **firstName**: *string*

Defined in types.ts:296

___

###  id

• **id**: *string*

Defined in types.ts:298

___

###  lastName

• **lastName**: *string*

Defined in types.ts:297

___

###  refId

• **refId**: *string | null*

Defined in types.ts:299
