[php-sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [ImageBase](imagebase.md)

# Class: ImageBase

## Hierarchy

* [MediaBase](mediabase.md)

  ↳ **ImageBase**

## Index

### Properties

* [encoding](imagebase.md#markdown-header-encoding)
* [extension](imagebase.md#markdown-header-extension)
* [fileSize](imagebase.md#markdown-header-filesize)
* [fileType](imagebase.md#markdown-header-filetype)
* [filename](imagebase.md#markdown-header-filename)
* [imageInfo](imagebase.md#markdown-header-imageinfo)
* [mimeType](imagebase.md#markdown-header-mimetype)
* [urlPath](imagebase.md#markdown-header-urlpath)
* [videoInfo](imagebase.md#markdown-header-videoinfo)

## Properties

###  encoding

• **encoding**: *string*

*Inherited from [MediaBase](mediabase.md).[encoding](mediabase.md#markdown-header-encoding)*

Defined in types.ts:246

___

###  extension

• **extension**: *string*

*Inherited from [MediaBase](mediabase.md).[extension](mediabase.md#markdown-header-extension)*

Defined in types.ts:245

___

###  fileSize

• **fileSize**: *number*

*Inherited from [MediaBase](mediabase.md).[fileSize](mediabase.md#markdown-header-filesize)*

Defined in types.ts:248

___

###  fileType

• **fileType**: *[FileType](../enums/filetype.md)* = FileType.Image

Defined in types.ts:252

___

###  filename

• **filename**: *string*

*Inherited from [MediaBase](mediabase.md).[filename](mediabase.md#markdown-header-filename)*

Defined in types.ts:247

___

###  imageInfo

• **imageInfo**: *[ImageInfoMedia](imageinfomedia.md)*

Defined in types.ts:253

___

###  mimeType

• **mimeType**: *string*

*Inherited from [MediaBase](mediabase.md).[mimeType](mediabase.md#markdown-header-mimetype)*

Defined in types.ts:244

___

###  urlPath

• **urlPath**: *string*

*Inherited from [MediaBase](mediabase.md).[urlPath](mediabase.md#markdown-header-urlpath)*

Defined in types.ts:243

___

###  videoInfo

• **videoInfo**: *null* = null

Defined in types.ts:254
