[php-sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [ConversationBase](conversationbase.md)

# Class: ConversationBase

## Hierarchy

* **ConversationBase**

  ↳ [Conversation](dto.conversation.md)

  ↳ [Conversation](domain.conversation.md)

## Index

### Properties

* [id](conversationbase.md#markdown-header-id)
* [isOwner](conversationbase.md#markdown-header-isowner)
* [lastReadMessageId](conversationbase.md#markdown-header-lastreadmessageid)
* [serverCreated](conversationbase.md#markdown-header-servercreated)
* [serverManaged](conversationbase.md#markdown-header-servermanaged)
* [type](conversationbase.md#markdown-header-type)
* [visible](conversationbase.md#markdown-header-visible)

## Properties

###  id

• **id**: *string*

Defined in types.ts:350

___

###  isOwner

• **isOwner**: *boolean*

Defined in types.ts:353

___

###  lastReadMessageId

• **lastReadMessageId**: *string | null*

Defined in types.ts:351

___

###  serverCreated

• **serverCreated**: *boolean*

Defined in types.ts:355

___

###  serverManaged

• **serverManaged**: *boolean*

Defined in types.ts:356

___

###  type

• **type**: *[Group](../enums/conversationtype.md#markdown-header-group) | [Individual](../enums/conversationtype.md#markdown-header-individual)*

Defined in types.ts:352

___

###  visible

• **visible**: *boolean*

Defined in types.ts:354
