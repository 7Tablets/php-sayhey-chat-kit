[php-sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [DTO](../modules/dto.md) › [IndividualConversation](dto.individualconversation.md)

# Class: IndividualConversation

## Hierarchy

  ↳ [Conversation](dto.conversation.md)

  ↳ **IndividualConversation**

## Index

### Properties

* [badge](dto.individualconversation.md#markdown-header-badge)
* [createdAt](dto.individualconversation.md#markdown-header-createdat)
* [deletedFromConversationAt](dto.individualconversation.md#markdown-header-deletedfromconversationat)
* [endBefore](dto.individualconversation.md#markdown-header-endbefore)
* [group](dto.individualconversation.md#markdown-header-optional-group)
* [id](dto.individualconversation.md#markdown-header-id)
* [isOwner](dto.individualconversation.md#markdown-header-isowner)
* [lastMessage](dto.individualconversation.md#markdown-header-lastmessage)
* [lastReadMessageId](dto.individualconversation.md#markdown-header-lastreadmessageid)
* [muteUntil](dto.individualconversation.md#markdown-header-muteuntil)
* [pinnedAt](dto.individualconversation.md#markdown-header-pinnedat)
* [serverCreated](dto.individualconversation.md#markdown-header-servercreated)
* [serverManaged](dto.individualconversation.md#markdown-header-servermanaged)
* [startAfter](dto.individualconversation.md#markdown-header-startafter)
* [type](dto.individualconversation.md#markdown-header-type)
* [updatedAt](dto.individualconversation.md#markdown-header-updatedat)
* [user](dto.individualconversation.md#markdown-header-user)
* [visible](dto.individualconversation.md#markdown-header-visible)

## Properties

###  badge

• **badge**: *number | null*

*Inherited from [Conversation](dto.conversation.md).[badge](dto.conversation.md#markdown-header-badge)*

Defined in types.ts:403

___

###  createdAt

• **createdAt**: *string*

*Inherited from [Conversation](dto.conversation.md).[createdAt](dto.conversation.md#markdown-header-createdat)*

Defined in types.ts:407

___

###  deletedFromConversationAt

• **deletedFromConversationAt**: *string | null*

*Inherited from [Conversation](dto.conversation.md).[deletedFromConversationAt](dto.conversation.md#markdown-header-deletedfromconversationat)*

Defined in types.ts:406

___

###  endBefore

• **endBefore**: *string | null*

*Inherited from [Conversation](dto.conversation.md).[endBefore](dto.conversation.md#markdown-header-endbefore)*

Defined in types.ts:405

___

### `Optional` group

• **group**? : *[ConversationInfo](dto.conversationinfo.md)*

*Inherited from [Conversation](dto.conversation.md).[group](dto.conversation.md#markdown-header-optional-group)*

Defined in types.ts:410

___

###  id

• **id**: *string*

*Inherited from [ConversationBase](conversationbase.md).[id](conversationbase.md#markdown-header-id)*

Defined in types.ts:350

___

###  isOwner

• **isOwner**: *boolean*

*Inherited from [ConversationBase](conversationbase.md).[isOwner](conversationbase.md#markdown-header-isowner)*

Defined in types.ts:353

___

###  lastMessage

• **lastMessage**: *[Message](dto.message.md) | null*

*Inherited from [Conversation](dto.conversation.md).[lastMessage](dto.conversation.md#markdown-header-lastmessage)*

Defined in types.ts:409

___

###  lastReadMessageId

• **lastReadMessageId**: *string | null*

*Inherited from [ConversationBase](conversationbase.md).[lastReadMessageId](conversationbase.md#markdown-header-lastreadmessageid)*

Defined in types.ts:351

___

###  muteUntil

• **muteUntil**: *string | null*

*Inherited from [Conversation](dto.conversation.md).[muteUntil](dto.conversation.md#markdown-header-muteuntil)*

Defined in types.ts:401

___

###  pinnedAt

• **pinnedAt**: *string | null*

*Inherited from [Conversation](dto.conversation.md).[pinnedAt](dto.conversation.md#markdown-header-pinnedat)*

Defined in types.ts:402

___

###  serverCreated

• **serverCreated**: *boolean*

*Inherited from [ConversationBase](conversationbase.md).[serverCreated](conversationbase.md#markdown-header-servercreated)*

Defined in types.ts:355

___

###  serverManaged

• **serverManaged**: *boolean*

*Inherited from [ConversationBase](conversationbase.md).[serverManaged](conversationbase.md#markdown-header-servermanaged)*

Defined in types.ts:356

___

###  startAfter

• **startAfter**: *string | null*

*Inherited from [Conversation](dto.conversation.md).[startAfter](dto.conversation.md#markdown-header-startafter)*

Defined in types.ts:404

___

###  type

• **type**: *[Individual](../enums/conversationtype.md#markdown-header-individual)*

*Overrides [ConversationBase](conversationbase.md).[type](conversationbase.md#markdown-header-type)*

Defined in types.ts:418

___

###  updatedAt

• **updatedAt**: *string*

*Inherited from [Conversation](dto.conversation.md).[updatedAt](dto.conversation.md#markdown-header-updatedat)*

Defined in types.ts:408

___

###  user

• **user**: *[UserConversationInfo](dto.userconversationinfo.md)*

*Overrides [Conversation](dto.conversation.md).[user](dto.conversation.md#markdown-header-optional-user)*

Defined in types.ts:419

___

###  visible

• **visible**: *boolean*

*Inherited from [ConversationBase](conversationbase.md).[visible](conversationbase.md#markdown-header-visible)*

Defined in types.ts:354
