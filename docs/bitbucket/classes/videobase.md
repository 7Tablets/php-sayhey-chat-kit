[php-sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [VideoBase](videobase.md)

# Class: VideoBase

## Hierarchy

* [MediaBase](mediabase.md)

  ↳ **VideoBase**

## Index

### Properties

* [encoding](videobase.md#markdown-header-encoding)
* [extension](videobase.md#markdown-header-extension)
* [fileSize](videobase.md#markdown-header-filesize)
* [fileType](videobase.md#markdown-header-filetype)
* [filename](videobase.md#markdown-header-filename)
* [imageInfo](videobase.md#markdown-header-imageinfo)
* [mimeType](videobase.md#markdown-header-mimetype)
* [urlPath](videobase.md#markdown-header-urlpath)
* [videoInfo](videobase.md#markdown-header-videoinfo)

## Properties

###  encoding

• **encoding**: *string*

*Inherited from [MediaBase](mediabase.md).[encoding](mediabase.md#markdown-header-encoding)*

Defined in types.ts:246

___

###  extension

• **extension**: *string*

*Inherited from [MediaBase](mediabase.md).[extension](mediabase.md#markdown-header-extension)*

Defined in types.ts:245

___

###  fileSize

• **fileSize**: *number*

*Inherited from [MediaBase](mediabase.md).[fileSize](mediabase.md#markdown-header-filesize)*

Defined in types.ts:248

___

###  fileType

• **fileType**: *[FileType](../enums/filetype.md)* = FileType.Video

Defined in types.ts:258

___

###  filename

• **filename**: *string*

*Inherited from [MediaBase](mediabase.md).[filename](mediabase.md#markdown-header-filename)*

Defined in types.ts:247

___

###  imageInfo

• **imageInfo**: *null* = null

Defined in types.ts:259

___

###  mimeType

• **mimeType**: *string*

*Inherited from [MediaBase](mediabase.md).[mimeType](mediabase.md#markdown-header-mimetype)*

Defined in types.ts:244

___

###  urlPath

• **urlPath**: *string*

*Inherited from [MediaBase](mediabase.md).[urlPath](mediabase.md#markdown-header-urlpath)*

Defined in types.ts:243

___

###  videoInfo

• **videoInfo**: *object*

Defined in types.ts:260

#### Type declaration:

* **duration**: *number*

* **thumbnail**: *[ImageBase](imagebase.md) | null*
