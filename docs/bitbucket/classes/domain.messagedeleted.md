[php-sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [Domain](../modules/domain.md) › [MessageDeleted](domain.messagedeleted.md)

# Class: MessageDeleted

## Hierarchy

* **MessageDeleted**

## Index

### Properties

* [conversationId](domain.messagedeleted.md#markdown-header-conversationid)
* [deletedAt](domain.messagedeleted.md#markdown-header-deletedat)
* [messageId](domain.messagedeleted.md#markdown-header-messageid)

## Properties

###  conversationId

• **conversationId**: *string*

Defined in types.ts:498

___

###  deletedAt

• **deletedAt**: *Date*

Defined in types.ts:500

___

###  messageId

• **messageId**: *string*

Defined in types.ts:499
