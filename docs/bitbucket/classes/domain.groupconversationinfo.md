[php-sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [Domain](../modules/domain.md) › [GroupConversationInfo](domain.groupconversationinfo.md)

# Class: GroupConversationInfo

## Hierarchy

* [ConversationInfo](domain.conversationinfo.md)

  ↳ **GroupConversationInfo**

## Index

### Properties

* [name](domain.groupconversationinfo.md#markdown-header-name)
* [picture](domain.groupconversationinfo.md#markdown-header-picture)

## Properties

###  name

• **name**: *string*

*Inherited from [ConversationInfo](domain.conversationinfo.md).[name](domain.conversationinfo.md#markdown-header-name)*

Defined in types.ts:469

___

###  picture

• **picture**: *[Media](../modules/domain.md#markdown-header-media) | null*

*Inherited from [ConversationInfo](domain.conversationinfo.md).[picture](domain.conversationinfo.md#markdown-header-picture)*

Defined in types.ts:468
