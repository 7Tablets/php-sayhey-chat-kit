[php-sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [ConversationUsersBatchMeta](conversationusersbatchmeta.md)

# Class: ConversationUsersBatchMeta

## Hierarchy

* [Paginatable](paginatable.md)

  ↳ **ConversationUsersBatchMeta**

  ↳ [ConversationUsersBatchBase](conversationusersbatchbase.md)

## Index

### Properties

* [completed](conversationusersbatchmeta.md#markdown-header-completed)
* [conversationId](conversationusersbatchmeta.md#markdown-header-conversationid)
* [includeRefIdInSearch](conversationusersbatchmeta.md#markdown-header-includerefidinsearch)
* [limit](conversationusersbatchmeta.md#markdown-header-limit)
* [nextOffset](conversationusersbatchmeta.md#markdown-header-nextoffset)
* [offset](conversationusersbatchmeta.md#markdown-header-offset)
* [search](conversationusersbatchmeta.md#markdown-header-search)
* [totalCount](conversationusersbatchmeta.md#markdown-header-totalcount)

## Properties

###  completed

• **completed**: *boolean*

*Inherited from [Paginatable](paginatable.md).[completed](paginatable.md#markdown-header-completed)*

Defined in types.ts:219

___

###  conversationId

• **conversationId**: *string*

Defined in types.ts:229

___

###  includeRefIdInSearch

• **includeRefIdInSearch**: *boolean*

Defined in types.ts:228

___

###  limit

• **limit**: *number*

*Inherited from [Paginatable](paginatable.md).[limit](paginatable.md#markdown-header-limit)*

Defined in types.ts:218

___

###  nextOffset

• **nextOffset**: *number*

*Inherited from [Paginatable](paginatable.md).[nextOffset](paginatable.md#markdown-header-nextoffset)*

Defined in types.ts:217

___

###  offset

• **offset**: *number*

*Inherited from [Paginatable](paginatable.md).[offset](paginatable.md#markdown-header-offset)*

Defined in types.ts:216

___

###  search

• **search**: *string | null*

*Inherited from [Paginatable](paginatable.md).[search](paginatable.md#markdown-header-search)*

Defined in types.ts:214

___

###  totalCount

• **totalCount**: *number*

*Inherited from [Paginatable](paginatable.md).[totalCount](paginatable.md#markdown-header-totalcount)*

Defined in types.ts:215
