[php-sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [Domain](../modules/domain.md) › [DefaultGroupProfileImage](domain.defaultgroupprofileimage.md)

# Class: DefaultGroupProfileImage

## Hierarchy

* [DefaultGroupProfileImageBase](defaultgroupprofileimagebase.md)

  ↳ **DefaultGroupProfileImage**

## Index

### Properties

* [filename](domain.defaultgroupprofileimage.md#markdown-header-filename)
* [original](domain.defaultgroupprofileimage.md#markdown-header-original)
* [thumbnail](domain.defaultgroupprofileimage.md#markdown-header-thumbnail)

## Properties

###  filename

• **filename**: *string*

*Inherited from [DefaultGroupProfileImageBase](defaultgroupprofileimagebase.md).[filename](defaultgroupprofileimagebase.md#markdown-header-filename)*

Defined in types.ts:163

___

###  original

• **original**: *string*

*Inherited from [DefaultGroupProfileImageBase](defaultgroupprofileimagebase.md).[original](defaultgroupprofileimagebase.md#markdown-header-original)*

Defined in types.ts:164

___

###  thumbnail

• **thumbnail**: *string*

*Inherited from [DefaultGroupProfileImageBase](defaultgroupprofileimagebase.md).[thumbnail](defaultgroupprofileimagebase.md#markdown-header-thumbnail)*

Defined in types.ts:165
