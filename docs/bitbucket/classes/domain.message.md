[php-sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [Domain](../modules/domain.md) › [Message](domain.message.md)

# Class: Message

## Hierarchy

* [MessageBase](messagebase.md)

  ↳ **Message**

## Index

### Properties

* [conversationId](domain.message.md#markdown-header-conversationid)
* [createdAt](domain.message.md#markdown-header-createdat)
* [deletedAt](domain.message.md#markdown-header-deletedat)
* [file](domain.message.md#markdown-header-file)
* [id](domain.message.md#markdown-header-id)
* [reactions](domain.message.md#markdown-header-reactions)
* [refUrl](domain.message.md#markdown-header-refurl)
* [sender](domain.message.md#markdown-header-sender)
* [sent](domain.message.md#markdown-header-sent)
* [sequence](domain.message.md#markdown-header-sequence)
* [systemMessage](domain.message.md#markdown-header-systemmessage)
* [text](domain.message.md#markdown-header-text)
* [type](domain.message.md#markdown-header-type)

## Properties

###  conversationId

• **conversationId**: *string*

*Inherited from [MessageBase](messagebase.md).[conversationId](messagebase.md#markdown-header-conversationid)*

Defined in types.ts:336

___

###  createdAt

• **createdAt**: *Date*

Defined in types.ts:460

___

###  deletedAt

• **deletedAt**: *Date | null*

Defined in types.ts:465

___

###  file

• **file**: *[Media](../modules/domain.md#markdown-header-media) | null*

Defined in types.ts:462

___

###  id

• **id**: *string*

*Inherited from [MessageBase](messagebase.md).[id](messagebase.md#markdown-header-id)*

Defined in types.ts:335

___

###  reactions

• **reactions**: *[ReactionsObject](reactionsobject.md)*

*Inherited from [MessageBase](messagebase.md).[reactions](messagebase.md#markdown-header-reactions)*

Defined in types.ts:341

___

###  refUrl

• **refUrl**: *string | null*

*Inherited from [MessageBase](messagebase.md).[refUrl](messagebase.md#markdown-header-refurl)*

Defined in types.ts:339

___

###  sender

• **sender**: *[User](domain.user.md) | null*

Defined in types.ts:461

___

###  sent

• **sent**: *boolean*

Defined in types.ts:463

___

###  sequence

• **sequence**: *number*

*Inherited from [MessageBase](messagebase.md).[sequence](messagebase.md#markdown-header-sequence)*

Defined in types.ts:340

___

###  systemMessage

• **systemMessage**: *[SystemMessage](domain.systemmessage.md) | null*

Defined in types.ts:464

___

###  text

• **text**: *string | null*

*Inherited from [MessageBase](messagebase.md).[text](messagebase.md#markdown-header-text)*

Defined in types.ts:338

___

###  type

• **type**: *[MessageType](../enums/messagetype.md)*

*Inherited from [MessageBase](messagebase.md).[type](messagebase.md#markdown-header-type)*

Defined in types.ts:337
