[php-sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [UserDetail](userdetail.md)

# Class: UserDetail

## Hierarchy

* [UserBase](userbase.md)

  ↳ **UserDetail**

  ↳ [User](dto.user.md)

  ↳ [User](domain.user.md)

## Index

### Properties

* [email](userdetail.md#markdown-header-email)
* [firstName](userdetail.md#markdown-header-firstname)
* [id](userdetail.md#markdown-header-id)
* [lastName](userdetail.md#markdown-header-lastname)
* [refId](userdetail.md#markdown-header-refid)

## Properties

###  email

• **email**: *string*

Defined in types.ts:182

___

###  firstName

• **firstName**: *string*

*Inherited from [UserBase](userbase.md).[firstName](userbase.md#markdown-header-firstname)*

Defined in types.ts:176

___

###  id

• **id**: *string*

*Inherited from [UserBase](userbase.md).[id](userbase.md#markdown-header-id)*

Defined in types.ts:175

___

###  lastName

• **lastName**: *string*

*Inherited from [UserBase](userbase.md).[lastName](userbase.md#markdown-header-lastname)*

Defined in types.ts:177

___

###  refId

• **refId**: *string | null*

*Inherited from [UserBase](userbase.md).[refId](userbase.md#markdown-header-refid)*

Defined in types.ts:178
