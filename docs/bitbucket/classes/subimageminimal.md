[php-sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [SubImageMinimal](subimageminimal.md)

# Class: SubImageMinimal

## Hierarchy

* **SubImageMinimal**

## Index

### Properties

* [height](subimageminimal.md#markdown-header-height)
* [imageSize](subimageminimal.md#markdown-header-imagesize)
* [postfix](subimageminimal.md#markdown-header-postfix)
* [width](subimageminimal.md#markdown-header-width)

## Properties

###  height

• **height**: *number*

Defined in types.ts:203

___

###  imageSize

• **imageSize**: *[ImageSizes](../enums/imagesizes.md)*

Defined in types.ts:201

___

###  postfix

• **postfix**: *string*

Defined in types.ts:204

___

###  width

• **width**: *number*

Defined in types.ts:202
