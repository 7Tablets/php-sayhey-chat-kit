[php-sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [ConversationMapper](conversationmapper.md)

# Class: ConversationMapper

## Hierarchy

* **ConversationMapper**

## Index

### Methods

* [toConversationConversableUsersBatchDomain](conversationmapper.md#markdown-header-static-toconversationconversableusersbatchdomain)
* [toConversationUsersBatchDomain](conversationmapper.md#markdown-header-static-toconversationusersbatchdomain)
* [toGenericConversationDomain](conversationmapper.md#markdown-header-static-togenericconversationdomain)
* [toGroupConversationInfoDomain](conversationmapper.md#markdown-header-static-togroupconversationinfodomain)
* [toIndividualConversationInfoDomain](conversationmapper.md#markdown-header-static-toindividualconversationinfodomain)

## Methods

### `Static` toConversationConversableUsersBatchDomain

▸ **toConversationConversableUsersBatchDomain**(`batch`: [ConversationConversableUsersBatch](dto.conversationconversableusersbatch.md)): *[ConversationConversableUsersBatch](domain.conversationconversableusersbatch.md)*

Defined in mapper.ts:84

**Parameters:**

Name | Type |
------ | ------ |
`batch` | [ConversationConversableUsersBatch](dto.conversationconversableusersbatch.md) |

**Returns:** *[ConversationConversableUsersBatch](domain.conversationconversableusersbatch.md)*

___

### `Static` toConversationUsersBatchDomain

▸ **toConversationUsersBatchDomain**(`batch`: [ConversationUsersBatch](dto.conversationusersbatch.md)): *[ConversationUsersBatch](domain.conversationusersbatch.md)*

Defined in mapper.ts:93

**Parameters:**

Name | Type |
------ | ------ |
`batch` | [ConversationUsersBatch](dto.conversationusersbatch.md) |

**Returns:** *[ConversationUsersBatch](domain.conversationusersbatch.md)*

___

### `Static` toGenericConversationDomain

▸ **toGenericConversationDomain**(`conversation`: [GenericConversation](../modules/dto.md#markdown-header-genericconversation)): *[GenericConversation](../modules/domain.md#markdown-header-genericconversation)*

Defined in mapper.ts:118

**Parameters:**

Name | Type |
------ | ------ |
`conversation` | [GenericConversation](../modules/dto.md#markdown-header-genericconversation) |

**Returns:** *[GenericConversation](../modules/domain.md#markdown-header-genericconversation)*

___

### `Static` toGroupConversationInfoDomain

▸ **toGroupConversationInfoDomain**(`info`: [GroupConversationInfo](dto.groupconversationinfo.md)): *[GroupConversationInfo](domain.groupconversationinfo.md)*

Defined in mapper.ts:102

**Parameters:**

Name | Type |
------ | ------ |
`info` | [GroupConversationInfo](dto.groupconversationinfo.md) |

**Returns:** *[GroupConversationInfo](domain.groupconversationinfo.md)*

___

### `Static` toIndividualConversationInfoDomain

▸ **toIndividualConversationInfoDomain**(`info`: [UserConversationInfo](dto.userconversationinfo.md)): *[UserConversationInfo](domain.userconversationinfo.md)*

Defined in mapper.ts:110

**Parameters:**

Name | Type |
------ | ------ |
`info` | [UserConversationInfo](dto.userconversationinfo.md) |

**Returns:** *[UserConversationInfo](domain.userconversationinfo.md)*
