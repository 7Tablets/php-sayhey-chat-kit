[php-sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [DTO](../modules/dto.md) › [UserConversationInfo](dto.userconversationinfo.md)

# Class: UserConversationInfo

## Hierarchy

* [ConversationInfo](dto.conversationinfo.md)

  ↳ **UserConversationInfo**

## Index

### Properties

* [email](dto.userconversationinfo.md#markdown-header-email)
* [id](dto.userconversationinfo.md#markdown-header-id)
* [name](dto.userconversationinfo.md#markdown-header-name)
* [picture](dto.userconversationinfo.md#markdown-header-picture)
* [refId](dto.userconversationinfo.md#markdown-header-refid)

## Properties

###  email

• **email**: *string*

Defined in types.ts:397

___

###  id

• **id**: *string*

Defined in types.ts:396

___

###  name

• **name**: *string*

*Inherited from [ConversationInfo](dto.conversationinfo.md).[name](dto.conversationinfo.md#markdown-header-name)*

Defined in types.ts:392

___

###  picture

• **picture**: *[Media](../modules/dto.md#markdown-header-media) | null*

*Inherited from [ConversationInfo](dto.conversationinfo.md).[picture](dto.conversationinfo.md#markdown-header-picture)*

Defined in types.ts:391

___

###  refId

• **refId**: *string | null*

Defined in types.ts:398
