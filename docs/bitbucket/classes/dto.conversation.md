[php-sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [DTO](../modules/dto.md) › [Conversation](dto.conversation.md)

# Class: Conversation

## Hierarchy

* [ConversationBase](conversationbase.md)

  ↳ **Conversation**

  ↳ [GroupConversation](dto.groupconversation.md)

  ↳ [IndividualConversation](dto.individualconversation.md)

## Index

### Properties

* [badge](dto.conversation.md#markdown-header-badge)
* [createdAt](dto.conversation.md#markdown-header-createdat)
* [deletedFromConversationAt](dto.conversation.md#markdown-header-deletedfromconversationat)
* [endBefore](dto.conversation.md#markdown-header-endbefore)
* [group](dto.conversation.md#markdown-header-optional-group)
* [id](dto.conversation.md#markdown-header-id)
* [isOwner](dto.conversation.md#markdown-header-isowner)
* [lastMessage](dto.conversation.md#markdown-header-lastmessage)
* [lastReadMessageId](dto.conversation.md#markdown-header-lastreadmessageid)
* [muteUntil](dto.conversation.md#markdown-header-muteuntil)
* [pinnedAt](dto.conversation.md#markdown-header-pinnedat)
* [serverCreated](dto.conversation.md#markdown-header-servercreated)
* [serverManaged](dto.conversation.md#markdown-header-servermanaged)
* [startAfter](dto.conversation.md#markdown-header-startafter)
* [type](dto.conversation.md#markdown-header-type)
* [updatedAt](dto.conversation.md#markdown-header-updatedat)
* [user](dto.conversation.md#markdown-header-optional-user)
* [visible](dto.conversation.md#markdown-header-visible)

## Properties

###  badge

• **badge**: *number | null*

Defined in types.ts:403

___

###  createdAt

• **createdAt**: *string*

Defined in types.ts:407

___

###  deletedFromConversationAt

• **deletedFromConversationAt**: *string | null*

Defined in types.ts:406

___

###  endBefore

• **endBefore**: *string | null*

Defined in types.ts:405

___

### `Optional` group

• **group**? : *[ConversationInfo](dto.conversationinfo.md)*

Defined in types.ts:410

___

###  id

• **id**: *string*

*Inherited from [ConversationBase](conversationbase.md).[id](conversationbase.md#markdown-header-id)*

Defined in types.ts:350

___

###  isOwner

• **isOwner**: *boolean*

*Inherited from [ConversationBase](conversationbase.md).[isOwner](conversationbase.md#markdown-header-isowner)*

Defined in types.ts:353

___

###  lastMessage

• **lastMessage**: *[Message](dto.message.md) | null*

Defined in types.ts:409

___

###  lastReadMessageId

• **lastReadMessageId**: *string | null*

*Inherited from [ConversationBase](conversationbase.md).[lastReadMessageId](conversationbase.md#markdown-header-lastreadmessageid)*

Defined in types.ts:351

___

###  muteUntil

• **muteUntil**: *string | null*

Defined in types.ts:401

___

###  pinnedAt

• **pinnedAt**: *string | null*

Defined in types.ts:402

___

###  serverCreated

• **serverCreated**: *boolean*

*Inherited from [ConversationBase](conversationbase.md).[serverCreated](conversationbase.md#markdown-header-servercreated)*

Defined in types.ts:355

___

###  serverManaged

• **serverManaged**: *boolean*

*Inherited from [ConversationBase](conversationbase.md).[serverManaged](conversationbase.md#markdown-header-servermanaged)*

Defined in types.ts:356

___

###  startAfter

• **startAfter**: *string | null*

Defined in types.ts:404

___

###  type

• **type**: *[Group](../enums/conversationtype.md#markdown-header-group) | [Individual](../enums/conversationtype.md#markdown-header-individual)*

*Inherited from [ConversationBase](conversationbase.md).[type](conversationbase.md#markdown-header-type)*

Defined in types.ts:352

___

###  updatedAt

• **updatedAt**: *string*

Defined in types.ts:408

___

### `Optional` user

• **user**? : *[UserConversationInfo](dto.userconversationinfo.md)*

Defined in types.ts:411

___

###  visible

• **visible**: *boolean*

*Inherited from [ConversationBase](conversationbase.md).[visible](conversationbase.md#markdown-header-visible)*

Defined in types.ts:354
