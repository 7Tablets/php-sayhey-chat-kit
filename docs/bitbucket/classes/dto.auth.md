[php-sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [DTO](../modules/dto.md) › [Auth](dto.auth.md)

# Class: Auth

## Hierarchy

* [AuthBase](authbase.md)

  ↳ **Auth**

## Index

### Properties

* [accessToken](dto.auth.md#markdown-header-accesstoken)
* [publisherToken](dto.auth.md#markdown-header-publishertoken)
* [refreshToken](dto.auth.md#markdown-header-refreshtoken)

## Properties

###  accessToken

• **accessToken**: *string*

*Inherited from [AuthBase](authbase.md).[accessToken](authbase.md#markdown-header-accesstoken)*

Defined in types.ts:169

___

###  publisherToken

• **publisherToken**: *string*

*Inherited from [AuthBase](authbase.md).[publisherToken](authbase.md#markdown-header-publishertoken)*

Defined in types.ts:170

___

###  refreshToken

• **refreshToken**: *string*

*Inherited from [AuthBase](authbase.md).[refreshToken](authbase.md#markdown-header-refreshtoken)*

Defined in types.ts:171
