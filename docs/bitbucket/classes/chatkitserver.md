[php-sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [ChatKitServer](chatkitserver.md)

# Class: ChatKitServer

## Hierarchy

* **ChatKitServer**

## Index

### Constructors

* [constructor](chatkitserver.md#markdown-header-private-constructor)

### Properties

* [_initialized](chatkitserver.md#markdown-header-static-private-_initialized)
* [apiKey](chatkitserver.md#markdown-header-static-apikey)
* [apiVersion](chatkitserver.md#markdown-header-static-apiversion)
* [appId](chatkitserver.md#markdown-header-static-appid)
* [httpClient](chatkitserver.md#markdown-header-static-private-httpclient)

### Methods

* [addUsersToConversation](chatkitserver.md#markdown-header-static-adduserstoconversation)
* [createGroupWithOwner](chatkitserver.md#markdown-header-static-creategroupwithowner)
* [deleteAllUsersInConversation](chatkitserver.md#markdown-header-static-deleteallusersinconversation)
* [deleteMessage](chatkitserver.md#markdown-header-static-deletemessage)
* [deleteUser](chatkitserver.md#markdown-header-static-deleteuser)
* [disableUser](chatkitserver.md#markdown-header-static-disableuser)
* [enableUser](chatkitserver.md#markdown-header-static-enableuser)
* [getConversationBasicUsers](chatkitserver.md#markdown-header-static-getconversationbasicusers)
* [getConversationConversableUsers](chatkitserver.md#markdown-header-static-getconversationconversableusers)
* [getConversationUsers](chatkitserver.md#markdown-header-static-getconversationusers)
* [getDefaultProfileImagesForGroup](chatkitserver.md#markdown-header-static-getdefaultprofileimagesforgroup)
* [getFile](chatkitserver.md#markdown-header-static-getfile)
* [getFileMessage](chatkitserver.md#markdown-header-static-getfilemessage)
* [getGroupProfileImage](chatkitserver.md#markdown-header-static-getgroupprofileimage)
* [getMessages](chatkitserver.md#markdown-header-static-getmessages)
* [getUserProfile](chatkitserver.md#markdown-header-static-getuserprofile)
* [getVideoThumbnail](chatkitserver.md#markdown-header-static-getvideothumbnail)
* [guard](chatkitserver.md#markdown-header-static-private-guard)
* [initialize](chatkitserver.md#markdown-header-static-initialize)
* [makeConversationOwner](chatkitserver.md#markdown-header-static-makeconversationowner)
* [removeConversationOwner](chatkitserver.md#markdown-header-static-removeconversationowner)
* [removeGroupImage](chatkitserver.md#markdown-header-static-removegroupimage)
* [removeUserProfileImage](chatkitserver.md#markdown-header-static-removeuserprofileimage)
* [removeUsersFromConversation](chatkitserver.md#markdown-header-static-removeusersfromconversation)
* [signInUserWithEmail](chatkitserver.md#markdown-header-static-signinuserwithemail)
* [signInUserWithId](chatkitserver.md#markdown-header-static-signinuserwithid)
* [signUpUser](chatkitserver.md#markdown-header-static-signupuser)
* [streamFile](chatkitserver.md#markdown-header-static-streamfile)
* [updateGroupImage](chatkitserver.md#markdown-header-static-updategroupimage)
* [updateGroupInfo](chatkitserver.md#markdown-header-static-updategroupinfo)
* [updateUserProfileImage](chatkitserver.md#markdown-header-static-updateuserprofileimage)
* [updateUserProfileInfo](chatkitserver.md#markdown-header-static-updateuserprofileinfo)
* [uploadFile](chatkitserver.md#markdown-header-static-uploadfile)
* [uploadFileWithStream](chatkitserver.md#markdown-header-static-uploadfilewithstream)
* [viewDefaultProfileImageForGroup](chatkitserver.md#markdown-header-static-viewdefaultprofileimageforgroup)

## Constructors

### `Private` constructor

\+ **new ChatKitServer**(): *[ChatKitServer](chatkitserver.md)*

Defined in sayhey-chat-kit.ts:39

**Returns:** *[ChatKitServer](chatkitserver.md)*

## Properties

### `Static` `Private` _initialized

▪ **_initialized**: *boolean* = false

Defined in sayhey-chat-kit.ts:15

___

### `Static` apiKey

▪ **apiKey**: *string*

Defined in sayhey-chat-kit.ts:9

___

### `Static` apiVersion

▪ **apiVersion**: *string*

Defined in sayhey-chat-kit.ts:13

___

### `Static` appId

▪ **appId**: *string*

Defined in sayhey-chat-kit.ts:11

___

### `Static` `Private` httpClient

▪ **httpClient**: *AxiosInstance*

Defined in sayhey-chat-kit.ts:17

## Methods

### `Static` addUsersToConversation

▸ **addUsersToConversation**(`params`: object): *Promise‹Result‹boolean, [AppError](apperror.md)››*

Defined in sayhey-chat-kit.ts:377

**Parameters:**

▪ **params**: *object*

Name | Type |
------ | ------ |
`conversationId` | string |
`force?` | undefined &#124; false &#124; true |
`memberIds` | string[] |

**Returns:** *Promise‹Result‹boolean, [AppError](apperror.md)››*

___

### `Static` createGroupWithOwner

▸ **createGroupWithOwner**(`params`: object): *Promise‹Result‹[GroupConversation](domain.groupconversation.md), [AppError](apperror.md)››*

Defined in sayhey-chat-kit.ts:475

**Parameters:**

▪ **params**: *object*

Name | Type |
------ | ------ |
`groupImageId` | string &#124; null |
`groupName` | string |
`memberIds` | string[] |
`ownerId` | string |
`serverManaged?` | undefined &#124; false &#124; true |

**Returns:** *Promise‹Result‹[GroupConversation](domain.groupconversation.md), [AppError](apperror.md)››*

___

### `Static` deleteAllUsersInConversation

▸ **deleteAllUsersInConversation**(`params`: object): *Promise‹Result‹boolean, [AppError](apperror.md)››*

Defined in sayhey-chat-kit.ts:459

**Parameters:**

▪ **params**: *object*

Name | Type |
------ | ------ |
`conversationId` | string |

**Returns:** *Promise‹Result‹boolean, [AppError](apperror.md)››*

___

### `Static` deleteMessage

▸ **deleteMessage**(`messageId`: string): *Promise‹Result‹[MessageDeleted](domain.messagedeleted.md), [AppError](apperror.md)››*

Defined in sayhey-chat-kit.ts:578

**Parameters:**

Name | Type |
------ | ------ |
`messageId` | string |

**Returns:** *Promise‹Result‹[MessageDeleted](domain.messagedeleted.md), [AppError](apperror.md)››*

___

### `Static` deleteUser

▸ **deleteUser**(`userId`: string): *Promise‹Result‹boolean, [AppError](apperror.md)››*

Defined in sayhey-chat-kit.ts:292

**Parameters:**

Name | Type |
------ | ------ |
`userId` | string |

**Returns:** *Promise‹Result‹boolean, [AppError](apperror.md)››*

___

### `Static` disableUser

▸ **disableUser**(`userId`: string): *Promise‹Result‹boolean, [AppError](apperror.md)››*

Defined in sayhey-chat-kit.ts:266

**Parameters:**

Name | Type |
------ | ------ |
`userId` | string |

**Returns:** *Promise‹Result‹boolean, [AppError](apperror.md)››*

___

### `Static` enableUser

▸ **enableUser**(`userId`: string): *Promise‹Result‹boolean, [AppError](apperror.md)››*

Defined in sayhey-chat-kit.ts:279

**Parameters:**

Name | Type |
------ | ------ |
`userId` | string |

**Returns:** *Promise‹Result‹boolean, [AppError](apperror.md)››*

___

### `Static` getConversationBasicUsers

▸ **getConversationBasicUsers**(`params`: object): *Promise‹Result‹[ConversationUsersBasic](domain.conversationusersbasic.md)[], [AppError](apperror.md)››*

Defined in sayhey-chat-kit.ts:357

**Parameters:**

▪ **params**: *object*

Name | Type |
------ | ------ |
`conversationId` | string |
`type?` | "all" &#124; "current" &#124; "default" |

**Returns:** *Promise‹Result‹[ConversationUsersBasic](domain.conversationusersbasic.md)[], [AppError](apperror.md)››*

___

### `Static` getConversationConversableUsers

▸ **getConversationConversableUsers**(`params`: object): *Promise‹Result‹[ConversationConversableUsersBatch](domain.conversationconversableusersbatch.md), [AppError](apperror.md)››*

Defined in sayhey-chat-kit.ts:309

Conversation APIs

**Parameters:**

▪ **params**: *object*

Name | Type |
------ | ------ |
`conversationId` | string |
`includeRefIdInSearch?` | undefined &#124; false &#124; true |
`limit?` | undefined &#124; number |
`offset?` | undefined &#124; number |
`search?` | undefined &#124; string |

**Returns:** *Promise‹Result‹[ConversationConversableUsersBatch](domain.conversationconversableusersbatch.md), [AppError](apperror.md)››*

___

### `Static` getConversationUsers

▸ **getConversationUsers**(`params`: object): *Promise‹Result‹[ConversationUsersBatch](domain.conversationusersbatch.md), [AppError](apperror.md)››*

Defined in sayhey-chat-kit.ts:332

**Parameters:**

▪ **params**: *object*

Name | Type |
------ | ------ |
`conversationId` | string |
`includeRefIdInSearch?` | undefined &#124; false &#124; true |
`limit?` | undefined &#124; number |
`offset?` | undefined &#124; number |
`search?` | undefined &#124; string |
`type?` | "all" &#124; "current" &#124; "default" |

**Returns:** *Promise‹Result‹[ConversationUsersBatch](domain.conversationusersbatch.md), [AppError](apperror.md)››*

___

### `Static` getDefaultProfileImagesForGroup

▸ **getDefaultProfileImagesForGroup**(): *Promise‹Result‹[DefaultGroupProfileImage](domain.defaultgroupprofileimage.md)[], [AppError](apperror.md)››*

Defined in sayhey-chat-kit.ts:110

App APIs

**Returns:** *Promise‹Result‹[DefaultGroupProfileImage](domain.defaultgroupprofileimage.md)[], [AppError](apperror.md)››*

___

### `Static` getFile

▸ **getFile**(`url`: string, `method`: [HttpMethod](../enums/httpmethod.md)): *Promise‹Blob›*

Defined in sayhey-chat-kit.ts:60

**Parameters:**

Name | Type |
------ | ------ |
`url` | string |
`method` | [HttpMethod](../enums/httpmethod.md) |

**Returns:** *Promise‹Blob›*

___

### `Static` getFileMessage

▸ **getFileMessage**‹**I**, **R**›(`options`: object): *Promise‹Result‹R, [AppError](apperror.md)››*

Defined in sayhey-chat-kit.ts:620

**Type parameters:**

▪ **I**: *boolean*

▪ **R**: *string | ReadStream*

**Parameters:**

▪ **options**: *object*

Name | Type |
------ | ------ |
`fileLocalIdName` | string |
`messageId` | string |
`stream?` | I |

**Returns:** *Promise‹Result‹R, [AppError](apperror.md)››*

___

### `Static` getGroupProfileImage

▸ **getGroupProfileImage**‹**I**, **R**›(`options`: object): *Promise‹Result‹R, [AppError](apperror.md)››*

Defined in sayhey-chat-kit.ts:596

Media APIs

**Type parameters:**

▪ **I**: *boolean*

▪ **R**: *string | ReadStream*

**Parameters:**

▪ **options**: *object*

Name | Type |
------ | ------ |
`conversationId` | string |
`stream?` | I |

**Returns:** *Promise‹Result‹R, [AppError](apperror.md)››*

___

### `Static` getMessages

▸ **getMessages**(`params`: object): *Promise‹Result‹[Message](domain.message.md)[], [AppError](apperror.md)››*

Defined in sayhey-chat-kit.ts:549

Message APIs

**Parameters:**

▪ **params**: *object*

Name | Type |
------ | ------ |
`beforeSequence?` | undefined &#124; string |
`conversationId?` | undefined &#124; string |
`conversationType?` | [ConversationType](../enums/conversationtype.md) |
`fromDate?` | Date |
`galleryOnly?` | undefined &#124; false &#124; true |
`includeDeleted?` | undefined &#124; false &#124; true |
`limit?` | undefined &#124; number |
`search?` | undefined &#124; string |
`toDate?` | Date |
`userId?` | undefined &#124; string |

**Returns:** *Promise‹Result‹[Message](domain.message.md)[], [AppError](apperror.md)››*

___

### `Static` getUserProfile

▸ **getUserProfile**(`userId`: string): *Promise‹Result‹[UserSelf](domain.userself.md), [AppError](apperror.md)››*

Defined in sayhey-chat-kit.ts:201

**Parameters:**

Name | Type |
------ | ------ |
`userId` | string |

**Returns:** *Promise‹Result‹[UserSelf](domain.userself.md), [AppError](apperror.md)››*

___

### `Static` getVideoThumbnail

▸ **getVideoThumbnail**‹**I**, **R**›(`options`: object): *Promise‹Result‹R, [AppError](apperror.md)››*

Defined in sayhey-chat-kit.ts:648

**Type parameters:**

▪ **I**: *boolean*

▪ **R**: *string | ReadStream*

**Parameters:**

▪ **options**: *object*

Name | Type |
------ | ------ |
`fileLocalIdName` | string |
`messageId` | string |
`stream?` | I |

**Returns:** *Promise‹Result‹R, [AppError](apperror.md)››*

___

### `Static` `Private` guard

▸ **guard**‹**T**›(`cb`: function): *Promise‹Result‹T, [AppError](apperror.md)››*

Defined in sayhey-chat-kit.ts:19

**Type parameters:**

▪ **T**

**Parameters:**

▪ **cb**: *function*

▸ (...`args`: any[]): *Promise‹T | [AppError](apperror.md)›*

**Parameters:**

Name | Type |
------ | ------ |
`...args` | any[] |

**Returns:** *Promise‹Result‹T, [AppError](apperror.md)››*

___

### `Static` initialize

▸ **initialize**(`options`: object): *void*

Defined in sayhey-chat-kit.ts:43

**Parameters:**

▪ **options**: *object*

Name | Type |
------ | ------ |
`apiVersion?` | undefined &#124; string |
`sayheyApiKey` | string |
`sayheyAppId` | string |
`sayheyUrl` | string |

**Returns:** *void*

___

### `Static` makeConversationOwner

▸ **makeConversationOwner**(`params`: object): *Promise‹Result‹boolean, [AppError](apperror.md)››*

Defined in sayhey-chat-kit.ts:425

**Parameters:**

▪ **params**: *object*

Name | Type |
------ | ------ |
`conversationId` | string |
`userId` | string |

**Returns:** *Promise‹Result‹boolean, [AppError](apperror.md)››*

___

### `Static` removeConversationOwner

▸ **removeConversationOwner**(`params`: object): *Promise‹Result‹boolean, [AppError](apperror.md)››*

Defined in sayhey-chat-kit.ts:442

**Parameters:**

▪ **params**: *object*

Name | Type |
------ | ------ |
`conversationId` | string |
`userId` | string |

**Returns:** *Promise‹Result‹boolean, [AppError](apperror.md)››*

___

### `Static` removeGroupImage

▸ **removeGroupImage**(`conversationId`: string): *Promise‹Result‹boolean, [AppError](apperror.md)››*

Defined in sayhey-chat-kit.ts:533

**Parameters:**

Name | Type |
------ | ------ |
`conversationId` | string |

**Returns:** *Promise‹Result‹boolean, [AppError](apperror.md)››*

___

### `Static` removeUserProfileImage

▸ **removeUserProfileImage**(`userId`: string): *Promise‹Result‹boolean, [AppError](apperror.md)››*

Defined in sayhey-chat-kit.ts:253

**Parameters:**

Name | Type |
------ | ------ |
`userId` | string |

**Returns:** *Promise‹Result‹boolean, [AppError](apperror.md)››*

___

### `Static` removeUsersFromConversation

▸ **removeUsersFromConversation**(`params`: object): *Promise‹Result‹boolean, [AppError](apperror.md)››*

Defined in sayhey-chat-kit.ts:401

**Parameters:**

▪ **params**: *object*

Name | Type |
------ | ------ |
`conversationId` | string |
`force?` | undefined &#124; false &#124; true |
`memberIds` | string |

**Returns:** *Promise‹Result‹boolean, [AppError](apperror.md)››*

___

### `Static` signInUserWithEmail

▸ **signInUserWithEmail**(`email`: string): *Promise‹Result‹[Auth](domain.auth.md), [AppError](apperror.md)››*

Defined in sayhey-chat-kit.ts:169

**Parameters:**

Name | Type |
------ | ------ |
`email` | string |

**Returns:** *Promise‹Result‹[Auth](domain.auth.md), [AppError](apperror.md)››*

___

### `Static` signInUserWithId

▸ **signInUserWithId**(`id`: string): *Promise‹Result‹[Auth](domain.auth.md), [AppError](apperror.md)››*

Defined in sayhey-chat-kit.ts:185

**Parameters:**

Name | Type |
------ | ------ |
`id` | string |

**Returns:** *Promise‹Result‹[Auth](domain.auth.md), [AppError](apperror.md)››*

___

### `Static` signUpUser

▸ **signUpUser**(`params`: object): *Promise‹Result‹[Auth](domain.auth.md), [AppError](apperror.md)››*

Defined in sayhey-chat-kit.ts:148

User APIs

**Parameters:**

▪ **params**: *object*

Name | Type |
------ | ------ |
`email` | string |
`firstName` | string |
`lastName` | string |
`password?` | undefined &#124; string |
`profileImageId?` | string &#124; null |
`refId?` | undefined &#124; string |

**Returns:** *Promise‹Result‹[Auth](domain.auth.md), [AppError](apperror.md)››*

___

### `Static` streamFile

▸ **streamFile**(`url`: string, `method`: [HttpMethod](../enums/httpmethod.md)): *Promise‹ReadStream›*

Defined in sayhey-chat-kit.ts:69

**Parameters:**

Name | Type |
------ | ------ |
`url` | string |
`method` | [HttpMethod](../enums/httpmethod.md) |

**Returns:** *Promise‹ReadStream›*

___

### `Static` updateGroupImage

▸ **updateGroupImage**(`params`: object): *Promise‹Result‹boolean, [AppError](apperror.md)››*

Defined in sayhey-chat-kit.ts:515

**Parameters:**

▪ **params**: *object*

Name | Type |
------ | ------ |
`conversationId` | string |
`groupImageId` | string |

**Returns:** *Promise‹Result‹boolean, [AppError](apperror.md)››*

___

### `Static` updateGroupInfo

▸ **updateGroupInfo**(`params`: object): *Promise‹Result‹boolean, [AppError](apperror.md)››*

Defined in sayhey-chat-kit.ts:497

**Parameters:**

▪ **params**: *object*

Name | Type |
------ | ------ |
`conversationId` | string |
`groupName` | string |

**Returns:** *Promise‹Result‹boolean, [AppError](apperror.md)››*

___

### `Static` updateUserProfileImage

▸ **updateUserProfileImage**(`params`: object): *Promise‹Result‹boolean, [AppError](apperror.md)››*

Defined in sayhey-chat-kit.ts:235

**Parameters:**

▪ **params**: *object*

Name | Type |
------ | ------ |
`profileImageId` | string |
`userId` | string |

**Returns:** *Promise‹Result‹boolean, [AppError](apperror.md)››*

___

### `Static` updateUserProfileInfo

▸ **updateUserProfileInfo**(`params`: object): *Promise‹Result‹boolean, [AppError](apperror.md)››*

Defined in sayhey-chat-kit.ts:214

**Parameters:**

▪ **params**: *object*

Name | Type |
------ | ------ |
`email?` | undefined &#124; string |
`firstName?` | undefined &#124; string |
`lastName?` | undefined &#124; string |
`refId?` | string &#124; null |
`userId` | string |

**Returns:** *Promise‹Result‹boolean, [AppError](apperror.md)››*

___

### `Static` uploadFile

▸ **uploadFile**(`file`: Blob, `filename`: string): *Promise‹string›*

Defined in sayhey-chat-kit.ts:78

**Parameters:**

Name | Type |
------ | ------ |
`file` | Blob |
`filename` | string |

**Returns:** *Promise‹string›*

___

### `Static` uploadFileWithStream

▸ **uploadFileWithStream**(`readableStream`: ReadStream): *Promise‹string›*

Defined in sayhey-chat-kit.ts:92

**Parameters:**

Name | Type |
------ | ------ |
`readableStream` | ReadStream |

**Returns:** *Promise‹string›*

___

### `Static` viewDefaultProfileImageForGroup

▸ **viewDefaultProfileImageForGroup**‹**I**, **R**›(`options`: object): *Promise‹Result‹R, [AppError](apperror.md)››*

Defined in sayhey-chat-kit.ts:125

**Type parameters:**

▪ **I**: *boolean*

▪ **R**: *Blob | ReadStream*

**Parameters:**

▪ **options**: *object*

Name | Type |
------ | ------ |
`filename` | string |
`original?` | undefined &#124; false &#124; true |
`stream?` | I |

**Returns:** *Promise‹Result‹R, [AppError](apperror.md)››*
