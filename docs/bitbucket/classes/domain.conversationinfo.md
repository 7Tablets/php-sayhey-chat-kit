[php-sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [Domain](../modules/domain.md) › [ConversationInfo](domain.conversationinfo.md)

# Class: ConversationInfo

## Hierarchy

* **ConversationInfo**

  ↳ [GroupConversationInfo](domain.groupconversationinfo.md)

  ↳ [UserConversationInfo](domain.userconversationinfo.md)

## Index

### Properties

* [name](domain.conversationinfo.md#markdown-header-name)
* [picture](domain.conversationinfo.md#markdown-header-picture)

## Properties

###  name

• **name**: *string*

Defined in types.ts:469

___

###  picture

• **picture**: *[Media](../modules/domain.md#markdown-header-media) | null*

Defined in types.ts:468
