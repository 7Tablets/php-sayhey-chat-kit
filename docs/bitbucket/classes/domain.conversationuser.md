[php-sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [Domain](../modules/domain.md) › [ConversationUser](domain.conversationuser.md)

# Class: ConversationUser

## Hierarchy

  ↳ [User](domain.user.md)

  ↳ **ConversationUser**

## Index

### Properties

* [deletedAt](domain.conversationuser.md#markdown-header-deletedat)
* [deletedFromConversationAt](domain.conversationuser.md#markdown-header-deletedfromconversationat)
* [disabled](domain.conversationuser.md#markdown-header-disabled)
* [email](domain.conversationuser.md#markdown-header-email)
* [firstName](domain.conversationuser.md#markdown-header-firstname)
* [id](domain.conversationuser.md#markdown-header-id)
* [isOwner](domain.conversationuser.md#markdown-header-isowner)
* [lastName](domain.conversationuser.md#markdown-header-lastname)
* [picture](domain.conversationuser.md#markdown-header-picture)
* [refId](domain.conversationuser.md#markdown-header-refid)

## Properties

###  deletedAt

• **deletedAt**: *Date | null*

*Inherited from [User](domain.user.md).[deletedAt](domain.user.md#markdown-header-deletedat)*

Defined in types.ts:443

___

###  deletedFromConversationAt

• **deletedFromConversationAt**: *Date | null*

Defined in types.ts:452

___

###  disabled

• **disabled**: *boolean*

*Inherited from [User](domain.user.md).[disabled](domain.user.md#markdown-header-disabled)*

Defined in types.ts:442

___

###  email

• **email**: *string*

*Inherited from [UserDetail](userdetail.md).[email](userdetail.md#markdown-header-email)*

Defined in types.ts:182

___

###  firstName

• **firstName**: *string*

*Inherited from [UserBase](userbase.md).[firstName](userbase.md#markdown-header-firstname)*

Defined in types.ts:176

___

###  id

• **id**: *string*

*Inherited from [UserBase](userbase.md).[id](userbase.md#markdown-header-id)*

Defined in types.ts:175

___

###  isOwner

• **isOwner**: *boolean*

Defined in types.ts:453

___

###  lastName

• **lastName**: *string*

*Inherited from [UserBase](userbase.md).[lastName](userbase.md#markdown-header-lastname)*

Defined in types.ts:177

___

###  picture

• **picture**: *[Media](../modules/domain.md#markdown-header-media) | null*

*Inherited from [User](domain.user.md).[picture](domain.user.md#markdown-header-picture)*

Defined in types.ts:441

___

###  refId

• **refId**: *string | null*

*Inherited from [UserBase](userbase.md).[refId](userbase.md#markdown-header-refid)*

Defined in types.ts:178
