[php-sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [UserBase](userbase.md)

# Class: UserBase

## Hierarchy

* **UserBase**

  ↳ [UserDetail](userdetail.md)

## Index

### Properties

* [firstName](userbase.md#markdown-header-firstname)
* [id](userbase.md#markdown-header-id)
* [lastName](userbase.md#markdown-header-lastname)
* [refId](userbase.md#markdown-header-refid)

## Properties

###  firstName

• **firstName**: *string*

Defined in types.ts:176

___

###  id

• **id**: *string*

Defined in types.ts:175

___

###  lastName

• **lastName**: *string*

Defined in types.ts:177

___

###  refId

• **refId**: *string | null*

Defined in types.ts:178
