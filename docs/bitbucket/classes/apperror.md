[php-sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [AppError](apperror.md)

# Class: AppError

## Hierarchy

* **AppError**

## Index

### Constructors

* [constructor](apperror.md#markdown-header-constructor)

### Properties

* [authFailed](apperror.md#markdown-header-optional-authfailed)
* [error](apperror.md#markdown-header-optional-error)
* [exchange](apperror.md#markdown-header-optional-exchange)
* [message](apperror.md#markdown-header-optional-message)
* [statusCode](apperror.md#markdown-header-optional-statuscode)
* [type](apperror.md#markdown-header-type)

### Methods

* [setErrorActions](apperror.md#markdown-header-seterroractions)
* [setMessage](apperror.md#markdown-header-setmessage)
* [setStatusCode](apperror.md#markdown-header-setstatuscode)

## Constructors

###  constructor

\+ **new AppError**(`err?`: string | Error, `type?`: [Type](../modules/errortype.md#markdown-header-type)): *[AppError](apperror.md)*

Defined in types.ts:139

**Parameters:**

Name | Type |
------ | ------ |
`err?` | string &#124; Error |
`type?` | [Type](../modules/errortype.md#markdown-header-type) |

**Returns:** *[AppError](apperror.md)*

## Properties

### `Optional` authFailed

• **authFailed**? : *undefined | false | true*

Defined in types.ts:132

___

### `Optional` error

• **error**? : *Error*

Defined in types.ts:130

___

### `Optional` exchange

• **exchange**? : *undefined | false | true*

Defined in types.ts:133

___

### `Optional` message

• **message**? : *undefined | string*

Defined in types.ts:131

___

### `Optional` statusCode

• **statusCode**? : *undefined | number*

Defined in types.ts:129

___

###  type

• **type**: *[Type](../modules/errortype.md#markdown-header-type)* = ErrorType.InternalErrorType.Unknown

Defined in types.ts:128

## Methods

###  setErrorActions

▸ **setErrorActions**(`params`: object): *void*

Defined in types.ts:135

**Parameters:**

▪ **params**: *object*

Name | Type |
------ | ------ |
`authFailed?` | undefined &#124; false &#124; true |
`exchange?` | undefined &#124; false &#124; true |

**Returns:** *void*

___

###  setMessage

▸ **setMessage**(`msg`: string): *void*

Defined in types.ts:157

**Parameters:**

Name | Type |
------ | ------ |
`msg` | string |

**Returns:** *void*

___

###  setStatusCode

▸ **setStatusCode**(`statusCode?`: undefined | number): *void*

Defined in types.ts:153

**Parameters:**

Name | Type |
------ | ------ |
`statusCode?` | undefined &#124; number |

**Returns:** *void*
