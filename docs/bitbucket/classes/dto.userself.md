[php-sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [DTO](../modules/dto.md) › [UserSelf](dto.userself.md)

# Class: UserSelf

## Hierarchy

  ↳ [User](dto.user.md)

  ↳ **UserSelf**

## Index

### Properties

* [deletedAt](dto.userself.md#markdown-header-deletedat)
* [disabled](dto.userself.md#markdown-header-disabled)
* [email](dto.userself.md#markdown-header-email)
* [firstName](dto.userself.md#markdown-header-firstname)
* [id](dto.userself.md#markdown-header-id)
* [lastName](dto.userself.md#markdown-header-lastname)
* [muteAllUntil](dto.userself.md#markdown-header-mutealluntil)
* [picture](dto.userself.md#markdown-header-picture)
* [refId](dto.userself.md#markdown-header-refid)

## Properties

###  deletedAt

• **deletedAt**: *string | null*

*Inherited from [User](dto.user.md).[deletedAt](dto.user.md#markdown-header-deletedat)*

Defined in types.ts:366

___

###  disabled

• **disabled**: *boolean*

*Inherited from [User](dto.user.md).[disabled](dto.user.md#markdown-header-disabled)*

Defined in types.ts:365

___

###  email

• **email**: *string*

*Inherited from [UserDetail](userdetail.md).[email](userdetail.md#markdown-header-email)*

Defined in types.ts:182

___

###  firstName

• **firstName**: *string*

*Inherited from [UserBase](userbase.md).[firstName](userbase.md#markdown-header-firstname)*

Defined in types.ts:176

___

###  id

• **id**: *string*

*Inherited from [UserBase](userbase.md).[id](userbase.md#markdown-header-id)*

Defined in types.ts:175

___

###  lastName

• **lastName**: *string*

*Inherited from [UserBase](userbase.md).[lastName](userbase.md#markdown-header-lastname)*

Defined in types.ts:177

___

###  muteAllUntil

• **muteAllUntil**: *string | null*

Defined in types.ts:369

___

###  picture

• **picture**: *[Media](../modules/dto.md#markdown-header-media) | null*

*Inherited from [User](dto.user.md).[picture](dto.user.md#markdown-header-picture)*

Defined in types.ts:364

___

###  refId

• **refId**: *string | null*

*Inherited from [UserBase](userbase.md).[refId](userbase.md#markdown-header-refid)*

Defined in types.ts:178
