[php-sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [Paginatable](paginatable.md)

# Class: Paginatable

## Hierarchy

* **Paginatable**

  ↳ [ConversationConversableUsersBatchMeta](conversationconversableusersbatchmeta.md)

  ↳ [ConversationUsersBatchMeta](conversationusersbatchmeta.md)

## Index

### Properties

* [completed](paginatable.md#markdown-header-completed)
* [limit](paginatable.md#markdown-header-limit)
* [nextOffset](paginatable.md#markdown-header-nextoffset)
* [offset](paginatable.md#markdown-header-offset)
* [search](paginatable.md#markdown-header-search)
* [totalCount](paginatable.md#markdown-header-totalcount)

## Properties

###  completed

• **completed**: *boolean*

Defined in types.ts:219

___

###  limit

• **limit**: *number*

Defined in types.ts:218

___

###  nextOffset

• **nextOffset**: *number*

Defined in types.ts:217

___

###  offset

• **offset**: *number*

Defined in types.ts:216

___

###  search

• **search**: *string | null*

Defined in types.ts:214

___

###  totalCount

• **totalCount**: *number*

Defined in types.ts:215
