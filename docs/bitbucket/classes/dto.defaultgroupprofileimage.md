[php-sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [DTO](../modules/dto.md) › [DefaultGroupProfileImage](dto.defaultgroupprofileimage.md)

# Class: DefaultGroupProfileImage

## Hierarchy

* [DefaultGroupProfileImageBase](defaultgroupprofileimagebase.md)

  ↳ **DefaultGroupProfileImage**

## Index

### Properties

* [filename](dto.defaultgroupprofileimage.md#markdown-header-filename)
* [original](dto.defaultgroupprofileimage.md#markdown-header-original)
* [thumbnail](dto.defaultgroupprofileimage.md#markdown-header-thumbnail)

## Properties

###  filename

• **filename**: *string*

*Inherited from [DefaultGroupProfileImageBase](defaultgroupprofileimagebase.md).[filename](defaultgroupprofileimagebase.md#markdown-header-filename)*

Defined in types.ts:163

___

###  original

• **original**: *string*

*Inherited from [DefaultGroupProfileImageBase](defaultgroupprofileimagebase.md).[original](defaultgroupprofileimagebase.md#markdown-header-original)*

Defined in types.ts:164

___

###  thumbnail

• **thumbnail**: *string*

*Inherited from [DefaultGroupProfileImageBase](defaultgroupprofileimagebase.md).[thumbnail](defaultgroupprofileimagebase.md#markdown-header-thumbnail)*

Defined in types.ts:165
