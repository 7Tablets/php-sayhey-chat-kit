[php-sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [DocumentBase](documentbase.md)

# Class: DocumentBase

## Hierarchy

* [MediaBase](mediabase.md)

  ↳ **DocumentBase**

## Index

### Properties

* [encoding](documentbase.md#markdown-header-encoding)
* [extension](documentbase.md#markdown-header-extension)
* [fileSize](documentbase.md#markdown-header-filesize)
* [fileType](documentbase.md#markdown-header-filetype)
* [filename](documentbase.md#markdown-header-filename)
* [imageInfo](documentbase.md#markdown-header-imageinfo)
* [mimeType](documentbase.md#markdown-header-mimetype)
* [urlPath](documentbase.md#markdown-header-urlpath)
* [videoInfo](documentbase.md#markdown-header-videoinfo)

## Properties

###  encoding

• **encoding**: *string*

*Inherited from [MediaBase](mediabase.md).[encoding](mediabase.md#markdown-header-encoding)*

Defined in types.ts:246

___

###  extension

• **extension**: *string*

*Inherited from [MediaBase](mediabase.md).[extension](mediabase.md#markdown-header-extension)*

Defined in types.ts:245

___

###  fileSize

• **fileSize**: *number*

*Inherited from [MediaBase](mediabase.md).[fileSize](mediabase.md#markdown-header-filesize)*

Defined in types.ts:248

___

###  fileType

• **fileType**: *[FileType](../enums/filetype.md)* = FileType.Other

Defined in types.ts:276

___

###  filename

• **filename**: *string*

*Inherited from [MediaBase](mediabase.md).[filename](mediabase.md#markdown-header-filename)*

Defined in types.ts:247

___

###  imageInfo

• **imageInfo**: *null* = null

Defined in types.ts:277

___

###  mimeType

• **mimeType**: *string*

*Inherited from [MediaBase](mediabase.md).[mimeType](mediabase.md#markdown-header-mimetype)*

Defined in types.ts:244

___

###  urlPath

• **urlPath**: *string*

*Inherited from [MediaBase](mediabase.md).[urlPath](mediabase.md#markdown-header-urlpath)*

Defined in types.ts:243

___

###  videoInfo

• **videoInfo**: *null* = null

Defined in types.ts:278
