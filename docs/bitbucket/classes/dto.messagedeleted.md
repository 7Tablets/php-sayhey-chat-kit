[php-sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [DTO](../modules/dto.md) › [MessageDeleted](dto.messagedeleted.md)

# Class: MessageDeleted

## Hierarchy

* **MessageDeleted**

## Index

### Properties

* [conversationId](dto.messagedeleted.md#markdown-header-conversationid)
* [deletedAt](dto.messagedeleted.md#markdown-header-deletedat)
* [messageId](dto.messagedeleted.md#markdown-header-messageid)

## Properties

###  conversationId

• **conversationId**: *string*

Defined in types.ts:423

___

###  deletedAt

• **deletedAt**: *string*

Defined in types.ts:425

___

###  messageId

• **messageId**: *string*

Defined in types.ts:424
