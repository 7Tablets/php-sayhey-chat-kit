[php-sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [DTO](../modules/dto.md) › [SystemMessage](dto.systemmessage.md)

# Class: SystemMessage

## Hierarchy

* [SystemMessageBase](systemmessagebase.md)

  ↳ **SystemMessage**

## Index

### Properties

* [actorCount](dto.systemmessage.md#markdown-header-actorcount)
* [actors](dto.systemmessage.md#markdown-header-actors)
* [id](dto.systemmessage.md#markdown-header-id)
* [initiator](dto.systemmessage.md#markdown-header-initiator)
* [refValue](dto.systemmessage.md#markdown-header-refvalue)
* [systemMessageType](dto.systemmessage.md#markdown-header-systemmessagetype)
* [text](dto.systemmessage.md#markdown-header-optional-text)

## Properties

###  actorCount

• **actorCount**: *number*

*Inherited from [SystemMessageBase](systemmessagebase.md).[actorCount](systemmessagebase.md#markdown-header-actorcount)*

Defined in types.ts:306

___

###  actors

• **actors**: *[UserSimpleInfo](usersimpleinfo.md)[]*

*Inherited from [SystemMessageBase](systemmessagebase.md).[actors](systemmessagebase.md#markdown-header-actors)*

Defined in types.ts:308

___

###  id

• **id**: *string*

*Inherited from [SystemMessageBase](systemmessagebase.md).[id](systemmessagebase.md#markdown-header-id)*

Defined in types.ts:304

___

###  initiator

• **initiator**: *[UserSimpleInfo](usersimpleinfo.md) | null*

*Inherited from [SystemMessageBase](systemmessagebase.md).[initiator](systemmessagebase.md#markdown-header-initiator)*

Defined in types.ts:307

___

###  refValue

• **refValue**: *string | null*

*Inherited from [SystemMessageBase](systemmessagebase.md).[refValue](systemmessagebase.md#markdown-header-refvalue)*

Defined in types.ts:305

___

###  systemMessageType

• **systemMessageType**: *[SystemMessageType](../enums/systemmessagetype.md)*

*Inherited from [SystemMessageBase](systemmessagebase.md).[systemMessageType](systemmessagebase.md#markdown-header-systemmessagetype)*

Defined in types.ts:303

___

### `Optional` text

• **text**? : *string | null*

*Inherited from [SystemMessageBase](systemmessagebase.md).[text](systemmessagebase.md#markdown-header-optional-text)*

Defined in types.ts:309
