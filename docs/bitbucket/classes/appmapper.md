[php-sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [AppMapper](appmapper.md)

# Class: AppMapper

## Hierarchy

* **AppMapper**

## Index

### Methods

* [toAuthDomain](appmapper.md#markdown-header-static-toauthdomain)
* [toProfileImageDomainList](appmapper.md#markdown-header-static-toprofileimagedomainlist)

## Methods

### `Static` toAuthDomain

▸ **toAuthDomain**(`authDetails`: [Auth](dto.auth.md)): *[Auth](domain.auth.md)*

Defined in mapper.ts:10

**Parameters:**

Name | Type |
------ | ------ |
`authDetails` | [Auth](dto.auth.md) |

**Returns:** *[Auth](domain.auth.md)*

___

### `Static` toProfileImageDomainList

▸ **toProfileImageDomainList**(`imageList`: [DefaultGroupProfileImage](dto.defaultgroupprofileimage.md)[]): *[DefaultGroupProfileImage](domain.defaultgroupprofileimage.md)[]*

Defined in mapper.ts:4

**Parameters:**

Name | Type |
------ | ------ |
`imageList` | [DefaultGroupProfileImage](dto.defaultgroupprofileimage.md)[] |

**Returns:** *[DefaultGroupProfileImage](domain.defaultgroupprofileimage.md)[]*
