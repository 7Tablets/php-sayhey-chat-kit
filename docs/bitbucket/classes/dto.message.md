[php-sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [DTO](../modules/dto.md) › [Message](dto.message.md)

# Class: Message

## Hierarchy

* [MessageBase](messagebase.md)

  ↳ **Message**

## Index

### Properties

* [conversationId](dto.message.md#markdown-header-conversationid)
* [createdAt](dto.message.md#markdown-header-createdat)
* [deletedAt](dto.message.md#markdown-header-deletedat)
* [file](dto.message.md#markdown-header-file)
* [id](dto.message.md#markdown-header-id)
* [reactions](dto.message.md#markdown-header-reactions)
* [refUrl](dto.message.md#markdown-header-refurl)
* [sender](dto.message.md#markdown-header-sender)
* [sent](dto.message.md#markdown-header-sent)
* [sequence](dto.message.md#markdown-header-sequence)
* [systemMessage](dto.message.md#markdown-header-systemmessage)
* [text](dto.message.md#markdown-header-text)
* [type](dto.message.md#markdown-header-type)

## Properties

###  conversationId

• **conversationId**: *string*

*Inherited from [MessageBase](messagebase.md).[conversationId](messagebase.md#markdown-header-conversationid)*

Defined in types.ts:336

___

###  createdAt

• **createdAt**: *string*

Defined in types.ts:383

___

###  deletedAt

• **deletedAt**: *string | null*

Defined in types.ts:388

___

###  file

• **file**: *[Media](../modules/dto.md#markdown-header-media) | null*

Defined in types.ts:385

___

###  id

• **id**: *string*

*Inherited from [MessageBase](messagebase.md).[id](messagebase.md#markdown-header-id)*

Defined in types.ts:335

___

###  reactions

• **reactions**: *[ReactionsObject](reactionsobject.md)*

*Inherited from [MessageBase](messagebase.md).[reactions](messagebase.md#markdown-header-reactions)*

Defined in types.ts:341

___

###  refUrl

• **refUrl**: *string | null*

*Inherited from [MessageBase](messagebase.md).[refUrl](messagebase.md#markdown-header-refurl)*

Defined in types.ts:339

___

###  sender

• **sender**: *[User](dto.user.md) | null*

Defined in types.ts:384

___

###  sent

• **sent**: *boolean*

Defined in types.ts:386

___

###  sequence

• **sequence**: *number*

*Inherited from [MessageBase](messagebase.md).[sequence](messagebase.md#markdown-header-sequence)*

Defined in types.ts:340

___

###  systemMessage

• **systemMessage**: *[SystemMessage](dto.systemmessage.md) | null*

Defined in types.ts:387

___

###  text

• **text**: *string | null*

*Inherited from [MessageBase](messagebase.md).[text](messagebase.md#markdown-header-text)*

Defined in types.ts:338

___

###  type

• **type**: *[MessageType](../enums/messagetype.md)*

*Inherited from [MessageBase](messagebase.md).[type](messagebase.md#markdown-header-type)*

Defined in types.ts:337
