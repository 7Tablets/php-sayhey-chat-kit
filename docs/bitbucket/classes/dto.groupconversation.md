[php-sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [DTO](../modules/dto.md) › [GroupConversation](dto.groupconversation.md)

# Class: GroupConversation

## Hierarchy

  ↳ [Conversation](dto.conversation.md)

  ↳ **GroupConversation**

## Index

### Properties

* [badge](dto.groupconversation.md#markdown-header-badge)
* [createdAt](dto.groupconversation.md#markdown-header-createdat)
* [deletedFromConversationAt](dto.groupconversation.md#markdown-header-deletedfromconversationat)
* [endBefore](dto.groupconversation.md#markdown-header-endbefore)
* [group](dto.groupconversation.md#markdown-header-group)
* [id](dto.groupconversation.md#markdown-header-id)
* [isOwner](dto.groupconversation.md#markdown-header-isowner)
* [lastMessage](dto.groupconversation.md#markdown-header-lastmessage)
* [lastReadMessageId](dto.groupconversation.md#markdown-header-lastreadmessageid)
* [muteUntil](dto.groupconversation.md#markdown-header-muteuntil)
* [pinnedAt](dto.groupconversation.md#markdown-header-pinnedat)
* [serverCreated](dto.groupconversation.md#markdown-header-servercreated)
* [serverManaged](dto.groupconversation.md#markdown-header-servermanaged)
* [startAfter](dto.groupconversation.md#markdown-header-startafter)
* [type](dto.groupconversation.md#markdown-header-type)
* [updatedAt](dto.groupconversation.md#markdown-header-updatedat)
* [user](dto.groupconversation.md#markdown-header-optional-user)
* [visible](dto.groupconversation.md#markdown-header-visible)

## Properties

###  badge

• **badge**: *number | null*

*Inherited from [Conversation](dto.conversation.md).[badge](dto.conversation.md#markdown-header-badge)*

Defined in types.ts:403

___

###  createdAt

• **createdAt**: *string*

*Inherited from [Conversation](dto.conversation.md).[createdAt](dto.conversation.md#markdown-header-createdat)*

Defined in types.ts:407

___

###  deletedFromConversationAt

• **deletedFromConversationAt**: *string | null*

*Inherited from [Conversation](dto.conversation.md).[deletedFromConversationAt](dto.conversation.md#markdown-header-deletedfromconversationat)*

Defined in types.ts:406

___

###  endBefore

• **endBefore**: *string | null*

*Inherited from [Conversation](dto.conversation.md).[endBefore](dto.conversation.md#markdown-header-endbefore)*

Defined in types.ts:405

___

###  group

• **group**: *[GroupConversationInfo](dto.groupconversationinfo.md)*

*Overrides [Conversation](dto.conversation.md).[group](dto.conversation.md#markdown-header-optional-group)*

Defined in types.ts:415

___

###  id

• **id**: *string*

*Inherited from [ConversationBase](conversationbase.md).[id](conversationbase.md#markdown-header-id)*

Defined in types.ts:350

___

###  isOwner

• **isOwner**: *boolean*

*Inherited from [ConversationBase](conversationbase.md).[isOwner](conversationbase.md#markdown-header-isowner)*

Defined in types.ts:353

___

###  lastMessage

• **lastMessage**: *[Message](dto.message.md) | null*

*Inherited from [Conversation](dto.conversation.md).[lastMessage](dto.conversation.md#markdown-header-lastmessage)*

Defined in types.ts:409

___

###  lastReadMessageId

• **lastReadMessageId**: *string | null*

*Inherited from [ConversationBase](conversationbase.md).[lastReadMessageId](conversationbase.md#markdown-header-lastreadmessageid)*

Defined in types.ts:351

___

###  muteUntil

• **muteUntil**: *string | null*

*Inherited from [Conversation](dto.conversation.md).[muteUntil](dto.conversation.md#markdown-header-muteuntil)*

Defined in types.ts:401

___

###  pinnedAt

• **pinnedAt**: *string | null*

*Inherited from [Conversation](dto.conversation.md).[pinnedAt](dto.conversation.md#markdown-header-pinnedat)*

Defined in types.ts:402

___

###  serverCreated

• **serverCreated**: *boolean*

*Inherited from [ConversationBase](conversationbase.md).[serverCreated](conversationbase.md#markdown-header-servercreated)*

Defined in types.ts:355

___

###  serverManaged

• **serverManaged**: *boolean*

*Inherited from [ConversationBase](conversationbase.md).[serverManaged](conversationbase.md#markdown-header-servermanaged)*

Defined in types.ts:356

___

###  startAfter

• **startAfter**: *string | null*

*Inherited from [Conversation](dto.conversation.md).[startAfter](dto.conversation.md#markdown-header-startafter)*

Defined in types.ts:404

___

###  type

• **type**: *[Group](../enums/conversationtype.md#markdown-header-group)*

*Overrides [ConversationBase](conversationbase.md).[type](conversationbase.md#markdown-header-type)*

Defined in types.ts:414

___

###  updatedAt

• **updatedAt**: *string*

*Inherited from [Conversation](dto.conversation.md).[updatedAt](dto.conversation.md#markdown-header-updatedat)*

Defined in types.ts:408

___

### `Optional` user

• **user**? : *[UserConversationInfo](dto.userconversationinfo.md)*

*Inherited from [Conversation](dto.conversation.md).[user](dto.conversation.md#markdown-header-optional-user)*

Defined in types.ts:411

___

###  visible

• **visible**: *boolean*

*Inherited from [ConversationBase](conversationbase.md).[visible](conversationbase.md#markdown-header-visible)*

Defined in types.ts:354
