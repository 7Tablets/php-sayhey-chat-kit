[php-sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [Domain](../modules/domain.md) › [User](domain.user.md)

# Class: User

## Hierarchy

  ↳ [UserDetail](userdetail.md)

  ↳ **User**

  ↳ [UserSelf](domain.userself.md)

  ↳ [ConversationUser](domain.conversationuser.md)

## Index

### Properties

* [deletedAt](domain.user.md#markdown-header-deletedat)
* [disabled](domain.user.md#markdown-header-disabled)
* [email](domain.user.md#markdown-header-email)
* [firstName](domain.user.md#markdown-header-firstname)
* [id](domain.user.md#markdown-header-id)
* [lastName](domain.user.md#markdown-header-lastname)
* [picture](domain.user.md#markdown-header-picture)
* [refId](domain.user.md#markdown-header-refid)

## Properties

###  deletedAt

• **deletedAt**: *Date | null*

Defined in types.ts:443

___

###  disabled

• **disabled**: *boolean*

Defined in types.ts:442

___

###  email

• **email**: *string*

*Inherited from [UserDetail](userdetail.md).[email](userdetail.md#markdown-header-email)*

Defined in types.ts:182

___

###  firstName

• **firstName**: *string*

*Inherited from [UserBase](userbase.md).[firstName](userbase.md#markdown-header-firstname)*

Defined in types.ts:176

___

###  id

• **id**: *string*

*Inherited from [UserBase](userbase.md).[id](userbase.md#markdown-header-id)*

Defined in types.ts:175

___

###  lastName

• **lastName**: *string*

*Inherited from [UserBase](userbase.md).[lastName](userbase.md#markdown-header-lastname)*

Defined in types.ts:177

___

###  picture

• **picture**: *[Media](../modules/domain.md#markdown-header-media) | null*

Defined in types.ts:441

___

###  refId

• **refId**: *string | null*

*Inherited from [UserBase](userbase.md).[refId](userbase.md#markdown-header-refid)*

Defined in types.ts:178
