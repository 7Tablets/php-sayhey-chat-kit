[php-sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [DTO](../modules/dto.md) › [ConversationInfo](dto.conversationinfo.md)

# Class: ConversationInfo

## Hierarchy

* **ConversationInfo**

  ↳ [GroupConversationInfo](dto.groupconversationinfo.md)

  ↳ [UserConversationInfo](dto.userconversationinfo.md)

## Index

### Properties

* [name](dto.conversationinfo.md#markdown-header-name)
* [picture](dto.conversationinfo.md#markdown-header-picture)

## Properties

###  name

• **name**: *string*

Defined in types.ts:392

___

###  picture

• **picture**: *[Media](../modules/dto.md#markdown-header-media) | null*

Defined in types.ts:391
