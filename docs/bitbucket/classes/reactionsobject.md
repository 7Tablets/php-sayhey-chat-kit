[php-sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [ReactionsObject](reactionsobject.md)

# Class: ReactionsObject

## Hierarchy

* **ReactionsObject**

## Index

### Properties

* [[ReactionType.Fist]](reactionsobject.md#markdown-header-[reactiontype.fist])
* [[ReactionType.Heart]](reactionsobject.md#markdown-header-[reactiontype.heart])
* [[ReactionType.Thumb]](reactionsobject.md#markdown-header-[reactiontype.thumb])

## Properties

###  [ReactionType.Fist]

• **[ReactionType.Fist]**: *number* = 0

Defined in types.ts:329

___

###  [ReactionType.Heart]

• **[ReactionType.Heart]**: *number* = 0

Defined in types.ts:330

___

###  [ReactionType.Thumb]

• **[ReactionType.Thumb]**: *number* = 0

Defined in types.ts:331
