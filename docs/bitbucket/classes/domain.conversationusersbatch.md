[php-sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [Domain](../modules/domain.md) › [ConversationUsersBatch](domain.conversationusersbatch.md)

# Class: ConversationUsersBatch

## Hierarchy

  ↳ [ConversationUsersBatchBase](conversationusersbatchbase.md)

  ↳ **ConversationUsersBatch**

## Index

### Properties

* [completed](domain.conversationusersbatch.md#markdown-header-completed)
* [conversationId](domain.conversationusersbatch.md#markdown-header-conversationid)
* [includeRefIdInSearch](domain.conversationusersbatch.md#markdown-header-includerefidinsearch)
* [limit](domain.conversationusersbatch.md#markdown-header-limit)
* [nextOffset](domain.conversationusersbatch.md#markdown-header-nextoffset)
* [offset](domain.conversationusersbatch.md#markdown-header-offset)
* [results](domain.conversationusersbatch.md#markdown-header-results)
* [search](domain.conversationusersbatch.md#markdown-header-search)
* [totalCount](domain.conversationusersbatch.md#markdown-header-totalcount)
* [type](domain.conversationusersbatch.md#markdown-header-type)

## Properties

###  completed

• **completed**: *boolean*

*Inherited from [Paginatable](paginatable.md).[completed](paginatable.md#markdown-header-completed)*

Defined in types.ts:219

___

###  conversationId

• **conversationId**: *string*

*Inherited from [ConversationUsersBatchMeta](conversationusersbatchmeta.md).[conversationId](conversationusersbatchmeta.md#markdown-header-conversationid)*

Defined in types.ts:229

___

###  includeRefIdInSearch

• **includeRefIdInSearch**: *boolean*

*Inherited from [ConversationUsersBatchMeta](conversationusersbatchmeta.md).[includeRefIdInSearch](conversationusersbatchmeta.md#markdown-header-includerefidinsearch)*

Defined in types.ts:228

___

###  limit

• **limit**: *number*

*Inherited from [Paginatable](paginatable.md).[limit](paginatable.md#markdown-header-limit)*

Defined in types.ts:218

___

###  nextOffset

• **nextOffset**: *number*

*Inherited from [Paginatable](paginatable.md).[nextOffset](paginatable.md#markdown-header-nextoffset)*

Defined in types.ts:217

___

###  offset

• **offset**: *number*

*Inherited from [Paginatable](paginatable.md).[offset](paginatable.md#markdown-header-offset)*

Defined in types.ts:216

___

###  results

• **results**: *[ConversationUser](domain.conversationuser.md)[]*

Defined in types.ts:456

___

###  search

• **search**: *string | null*

*Inherited from [Paginatable](paginatable.md).[search](paginatable.md#markdown-header-search)*

Defined in types.ts:214

___

###  totalCount

• **totalCount**: *number*

*Inherited from [Paginatable](paginatable.md).[totalCount](paginatable.md#markdown-header-totalcount)*

Defined in types.ts:215

___

###  type

• **type**: *[ConversationUserQueryType](../enums/conversationuserquerytype.md)*

*Inherited from [ConversationUsersBatchBase](conversationusersbatchbase.md).[type](conversationusersbatchbase.md#markdown-header-type)*

Defined in types.ts:239
