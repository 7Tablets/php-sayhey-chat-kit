[php-sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [Domain](../modules/domain.md) › [UserConversationInfo](domain.userconversationinfo.md)

# Class: UserConversationInfo

## Hierarchy

* [ConversationInfo](domain.conversationinfo.md)

  ↳ **UserConversationInfo**

## Index

### Properties

* [email](domain.userconversationinfo.md#markdown-header-email)
* [id](domain.userconversationinfo.md#markdown-header-id)
* [name](domain.userconversationinfo.md#markdown-header-name)
* [picture](domain.userconversationinfo.md#markdown-header-picture)
* [refId](domain.userconversationinfo.md#markdown-header-refid)

## Properties

###  email

• **email**: *string*

Defined in types.ts:474

___

###  id

• **id**: *string*

Defined in types.ts:473

___

###  name

• **name**: *string*

*Inherited from [ConversationInfo](domain.conversationinfo.md).[name](domain.conversationinfo.md#markdown-header-name)*

Defined in types.ts:469

___

###  picture

• **picture**: *[Media](../modules/domain.md#markdown-header-media) | null*

*Inherited from [ConversationInfo](domain.conversationinfo.md).[picture](domain.conversationinfo.md#markdown-header-picture)*

Defined in types.ts:468

___

###  refId

• **refId**: *string | null*

Defined in types.ts:475
