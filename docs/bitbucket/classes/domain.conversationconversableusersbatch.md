[php-sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [Domain](../modules/domain.md) › [ConversationConversableUsersBatch](domain.conversationconversableusersbatch.md)

# Class: ConversationConversableUsersBatch

## Hierarchy

  ↳ [ConversationConversableUsersBatchMeta](conversationconversableusersbatchmeta.md)

  ↳ **ConversationConversableUsersBatch**

## Index

### Properties

* [completed](domain.conversationconversableusersbatch.md#markdown-header-completed)
* [conversationId](domain.conversationconversableusersbatch.md#markdown-header-conversationid)
* [includeRefIdInSearch](domain.conversationconversableusersbatch.md#markdown-header-includerefidinsearch)
* [limit](domain.conversationconversableusersbatch.md#markdown-header-limit)
* [nextOffset](domain.conversationconversableusersbatch.md#markdown-header-nextoffset)
* [offset](domain.conversationconversableusersbatch.md#markdown-header-offset)
* [results](domain.conversationconversableusersbatch.md#markdown-header-results)
* [search](domain.conversationconversableusersbatch.md#markdown-header-search)
* [totalCount](domain.conversationconversableusersbatch.md#markdown-header-totalcount)

## Properties

###  completed

• **completed**: *boolean*

*Inherited from [Paginatable](paginatable.md).[completed](paginatable.md#markdown-header-completed)*

Defined in types.ts:219

___

###  conversationId

• **conversationId**: *string*

*Inherited from [ConversationConversableUsersBatchMeta](conversationconversableusersbatchmeta.md).[conversationId](conversationconversableusersbatchmeta.md#markdown-header-conversationid)*

Defined in types.ts:224

___

###  includeRefIdInSearch

• **includeRefIdInSearch**: *boolean*

*Inherited from [ConversationConversableUsersBatchMeta](conversationconversableusersbatchmeta.md).[includeRefIdInSearch](conversationconversableusersbatchmeta.md#markdown-header-includerefidinsearch)*

Defined in types.ts:223

___

###  limit

• **limit**: *number*

*Inherited from [Paginatable](paginatable.md).[limit](paginatable.md#markdown-header-limit)*

Defined in types.ts:218

___

###  nextOffset

• **nextOffset**: *number*

*Inherited from [Paginatable](paginatable.md).[nextOffset](paginatable.md#markdown-header-nextoffset)*

Defined in types.ts:217

___

###  offset

• **offset**: *number*

*Inherited from [Paginatable](paginatable.md).[offset](paginatable.md#markdown-header-offset)*

Defined in types.ts:216

___

###  results

• **results**: *[User](domain.user.md)[]*

Defined in types.ts:449

___

###  search

• **search**: *string | null*

*Inherited from [Paginatable](paginatable.md).[search](paginatable.md#markdown-header-search)*

Defined in types.ts:214

___

###  totalCount

• **totalCount**: *number*

*Inherited from [Paginatable](paginatable.md).[totalCount](paginatable.md#markdown-header-totalcount)*

Defined in types.ts:215
