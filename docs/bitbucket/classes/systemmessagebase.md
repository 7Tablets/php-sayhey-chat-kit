[php-sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [SystemMessageBase](systemmessagebase.md)

# Class: SystemMessageBase

## Hierarchy

* **SystemMessageBase**

  ↳ [SystemMessage](dto.systemmessage.md)

  ↳ [SystemMessage](domain.systemmessage.md)

## Index

### Properties

* [actorCount](systemmessagebase.md#markdown-header-actorcount)
* [actors](systemmessagebase.md#markdown-header-actors)
* [id](systemmessagebase.md#markdown-header-id)
* [initiator](systemmessagebase.md#markdown-header-initiator)
* [refValue](systemmessagebase.md#markdown-header-refvalue)
* [systemMessageType](systemmessagebase.md#markdown-header-systemmessagetype)
* [text](systemmessagebase.md#markdown-header-optional-text)

## Properties

###  actorCount

• **actorCount**: *number*

Defined in types.ts:306

___

###  actors

• **actors**: *[UserSimpleInfo](usersimpleinfo.md)[]*

Defined in types.ts:308

___

###  id

• **id**: *string*

Defined in types.ts:304

___

###  initiator

• **initiator**: *[UserSimpleInfo](usersimpleinfo.md) | null*

Defined in types.ts:307

___

###  refValue

• **refValue**: *string | null*

Defined in types.ts:305

___

###  systemMessageType

• **systemMessageType**: *[SystemMessageType](../enums/systemmessagetype.md)*

Defined in types.ts:303

___

### `Optional` text

• **text**? : *string | null*

Defined in types.ts:309
