[php-sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [MediaMapper](mediamapper.md)

# Class: MediaMapper

## Hierarchy

* **MediaMapper**

## Index

### Methods

* [toMediaDomain](mediamapper.md#markdown-header-static-tomediadomain)

## Methods

### `Static` toMediaDomain

▸ **toMediaDomain**(`media`: [Media](../modules/dto.md#markdown-header-media)): *[Media](../modules/domain.md#markdown-header-media)*

Defined in mapper.ts:56

**Parameters:**

Name | Type |
------ | ------ |
`media` | [Media](../modules/dto.md#markdown-header-media) |

**Returns:** *[Media](../modules/domain.md#markdown-header-media)*
