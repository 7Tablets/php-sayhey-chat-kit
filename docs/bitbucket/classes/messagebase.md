[php-sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [MessageBase](messagebase.md)

# Class: MessageBase

## Hierarchy

* **MessageBase**

  ↳ [Message](dto.message.md)

  ↳ [Message](domain.message.md)

## Index

### Properties

* [conversationId](messagebase.md#markdown-header-conversationid)
* [id](messagebase.md#markdown-header-id)
* [reactions](messagebase.md#markdown-header-reactions)
* [refUrl](messagebase.md#markdown-header-refurl)
* [sequence](messagebase.md#markdown-header-sequence)
* [text](messagebase.md#markdown-header-text)
* [type](messagebase.md#markdown-header-type)

## Properties

###  conversationId

• **conversationId**: *string*

Defined in types.ts:336

___

###  id

• **id**: *string*

Defined in types.ts:335

___

###  reactions

• **reactions**: *[ReactionsObject](reactionsobject.md)*

Defined in types.ts:341

___

###  refUrl

• **refUrl**: *string | null*

Defined in types.ts:339

___

###  sequence

• **sequence**: *number*

Defined in types.ts:340

___

###  text

• **text**: *string | null*

Defined in types.ts:338

___

###  type

• **type**: *[MessageType](../enums/messagetype.md)*

Defined in types.ts:337
