[php-sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [DTO](../modules/dto.md) › [GroupConversationInfo](dto.groupconversationinfo.md)

# Class: GroupConversationInfo

## Hierarchy

* [ConversationInfo](dto.conversationinfo.md)

  ↳ **GroupConversationInfo**

## Index

### Properties

* [name](dto.groupconversationinfo.md#markdown-header-name)
* [picture](dto.groupconversationinfo.md#markdown-header-picture)

## Properties

###  name

• **name**: *string*

*Inherited from [ConversationInfo](dto.conversationinfo.md).[name](dto.conversationinfo.md#markdown-header-name)*

Defined in types.ts:392

___

###  picture

• **picture**: *[Media](../modules/dto.md#markdown-header-media) | null*

*Inherited from [ConversationInfo](dto.conversationinfo.md).[picture](dto.conversationinfo.md#markdown-header-picture)*

Defined in types.ts:391
