[php-sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [UserMapper](usermapper.md)

# Class: UserMapper

## Hierarchy

* **UserMapper**

## Index

### Methods

* [toConversationUserBasicDomain](usermapper.md#markdown-header-static-toconversationuserbasicdomain)
* [toConversationUserDomain](usermapper.md#markdown-header-static-toconversationuserdomain)
* [toUserDomain](usermapper.md#markdown-header-static-touserdomain)
* [toUserSelfDomain](usermapper.md#markdown-header-static-touserselfdomain)

## Methods

### `Static` toConversationUserBasicDomain

▸ **toConversationUserBasicDomain**(`user`: [ConversationUsersBasic](dto.conversationusersbasic.md)): *[ConversationUsersBasic](domain.conversationusersbasic.md)*

Defined in mapper.ts:41

**Parameters:**

Name | Type |
------ | ------ |
`user` | [ConversationUsersBasic](dto.conversationusersbasic.md) |

**Returns:** *[ConversationUsersBasic](domain.conversationusersbasic.md)*

___

### `Static` toConversationUserDomain

▸ **toConversationUserDomain**(`user`: [ConversationUser](dto.conversationuser.md)): *[ConversationUser](domain.conversationuser.md)*

Defined in mapper.ts:24

**Parameters:**

Name | Type |
------ | ------ |
`user` | [ConversationUser](dto.conversationuser.md) |

**Returns:** *[ConversationUser](domain.conversationuser.md)*

___

### `Static` toUserDomain

▸ **toUserDomain**(`user`: [User](dto.user.md)): *[User](domain.user.md)*

Defined in mapper.ts:16

**Parameters:**

Name | Type |
------ | ------ |
`user` | [User](dto.user.md) |

**Returns:** *[User](domain.user.md)*

___

### `Static` toUserSelfDomain

▸ **toUserSelfDomain**(`user`: [UserSelf](dto.userself.md)): *[UserSelf](domain.userself.md)*

Defined in mapper.ts:34

**Parameters:**

Name | Type |
------ | ------ |
`user` | [UserSelf](dto.userself.md) |

**Returns:** *[UserSelf](domain.userself.md)*
