[php-sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [Domain](../modules/domain.md) › [GroupConversation](domain.groupconversation.md)

# Class: GroupConversation

## Hierarchy

  ↳ [Conversation](domain.conversation.md)

  ↳ **GroupConversation**

## Index

### Properties

* [badge](domain.groupconversation.md#markdown-header-badge)
* [createdAt](domain.groupconversation.md#markdown-header-createdat)
* [deletedFromConversationAt](domain.groupconversation.md#markdown-header-deletedfromconversationat)
* [endBefore](domain.groupconversation.md#markdown-header-endbefore)
* [group](domain.groupconversation.md#markdown-header-group)
* [id](domain.groupconversation.md#markdown-header-id)
* [isOwner](domain.groupconversation.md#markdown-header-isowner)
* [lastMessage](domain.groupconversation.md#markdown-header-lastmessage)
* [lastReadMessageId](domain.groupconversation.md#markdown-header-lastreadmessageid)
* [muteUntil](domain.groupconversation.md#markdown-header-muteuntil)
* [pinnedAt](domain.groupconversation.md#markdown-header-pinnedat)
* [serverCreated](domain.groupconversation.md#markdown-header-servercreated)
* [serverManaged](domain.groupconversation.md#markdown-header-servermanaged)
* [startAfter](domain.groupconversation.md#markdown-header-startafter)
* [type](domain.groupconversation.md#markdown-header-type)
* [updatedAt](domain.groupconversation.md#markdown-header-updatedat)
* [visible](domain.groupconversation.md#markdown-header-visible)

## Properties

###  badge

• **badge**: *number | null*

*Inherited from [Conversation](domain.conversation.md).[badge](domain.conversation.md#markdown-header-badge)*

Defined in types.ts:480

___

###  createdAt

• **createdAt**: *Date*

*Inherited from [Conversation](domain.conversation.md).[createdAt](domain.conversation.md#markdown-header-createdat)*

Defined in types.ts:484

___

###  deletedFromConversationAt

• **deletedFromConversationAt**: *Date | null*

*Inherited from [Conversation](domain.conversation.md).[deletedFromConversationAt](domain.conversation.md#markdown-header-deletedfromconversationat)*

Defined in types.ts:483

___

###  endBefore

• **endBefore**: *Date | null*

*Inherited from [Conversation](domain.conversation.md).[endBefore](domain.conversation.md#markdown-header-endbefore)*

Defined in types.ts:482

___

###  group

• **group**: *[GroupConversationInfo](domain.groupconversationinfo.md)*

Defined in types.ts:490

___

###  id

• **id**: *string*

*Inherited from [ConversationBase](conversationbase.md).[id](conversationbase.md#markdown-header-id)*

Defined in types.ts:350

___

###  isOwner

• **isOwner**: *boolean*

*Inherited from [ConversationBase](conversationbase.md).[isOwner](conversationbase.md#markdown-header-isowner)*

Defined in types.ts:353

___

###  lastMessage

• **lastMessage**: *[Message](domain.message.md) | null*

*Inherited from [Conversation](domain.conversation.md).[lastMessage](domain.conversation.md#markdown-header-lastmessage)*

Defined in types.ts:486

___

###  lastReadMessageId

• **lastReadMessageId**: *string | null*

*Inherited from [ConversationBase](conversationbase.md).[lastReadMessageId](conversationbase.md#markdown-header-lastreadmessageid)*

Defined in types.ts:351

___

###  muteUntil

• **muteUntil**: *Date | null*

*Inherited from [Conversation](domain.conversation.md).[muteUntil](domain.conversation.md#markdown-header-muteuntil)*

Defined in types.ts:478

___

###  pinnedAt

• **pinnedAt**: *Date | null*

*Inherited from [Conversation](domain.conversation.md).[pinnedAt](domain.conversation.md#markdown-header-pinnedat)*

Defined in types.ts:479

___

###  serverCreated

• **serverCreated**: *boolean*

*Inherited from [ConversationBase](conversationbase.md).[serverCreated](conversationbase.md#markdown-header-servercreated)*

Defined in types.ts:355

___

###  serverManaged

• **serverManaged**: *boolean*

*Inherited from [ConversationBase](conversationbase.md).[serverManaged](conversationbase.md#markdown-header-servermanaged)*

Defined in types.ts:356

___

###  startAfter

• **startAfter**: *Date | null*

*Inherited from [Conversation](domain.conversation.md).[startAfter](domain.conversation.md#markdown-header-startafter)*

Defined in types.ts:481

___

###  type

• **type**: *[Group](../enums/conversationtype.md#markdown-header-group)*

*Overrides [ConversationBase](conversationbase.md).[type](conversationbase.md#markdown-header-type)*

Defined in types.ts:489

___

###  updatedAt

• **updatedAt**: *Date*

*Inherited from [Conversation](domain.conversation.md).[updatedAt](domain.conversation.md#markdown-header-updatedat)*

Defined in types.ts:485

___

###  visible

• **visible**: *boolean*

*Inherited from [ConversationBase](conversationbase.md).[visible](conversationbase.md#markdown-header-visible)*

Defined in types.ts:354
