[php-sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [DTO](../modules/dto.md) › [ConversationUsersBasic](dto.conversationusersbasic.md)

# Class: ConversationUsersBasic

## Hierarchy

* **ConversationUsersBasic**

## Index

### Properties

* [deletedFromConversationAt](dto.conversationusersbasic.md#markdown-header-deletedfromconversationat)
* [id](dto.conversationusersbasic.md#markdown-header-id)
* [isOwner](dto.conversationusersbasic.md#markdown-header-isowner)
* [userDeletedAt](dto.conversationusersbasic.md#markdown-header-userdeletedat)
* [userDisabled](dto.conversationusersbasic.md#markdown-header-userdisabled)

## Properties

###  deletedFromConversationAt

• **deletedFromConversationAt**: *string | null*

Defined in types.ts:430

___

###  id

• **id**: *string*

Defined in types.ts:429

___

###  isOwner

• **isOwner**: *boolean*

Defined in types.ts:428

___

###  userDeletedAt

• **userDeletedAt**: *string | null*

Defined in types.ts:432

___

###  userDisabled

• **userDisabled**: *boolean*

Defined in types.ts:431
