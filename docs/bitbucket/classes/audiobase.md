[php-sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [AudioBase](audiobase.md)

# Class: AudioBase

## Hierarchy

* [MediaBase](mediabase.md)

  ↳ **AudioBase**

## Index

### Properties

* [encoding](audiobase.md#markdown-header-encoding)
* [extension](audiobase.md#markdown-header-extension)
* [fileSize](audiobase.md#markdown-header-filesize)
* [fileType](audiobase.md#markdown-header-filetype)
* [filename](audiobase.md#markdown-header-filename)
* [imageInfo](audiobase.md#markdown-header-imageinfo)
* [mimeType](audiobase.md#markdown-header-mimetype)
* [urlPath](audiobase.md#markdown-header-urlpath)
* [videoInfo](audiobase.md#markdown-header-videoinfo)

## Properties

###  encoding

• **encoding**: *string*

*Inherited from [MediaBase](mediabase.md).[encoding](mediabase.md#markdown-header-encoding)*

Defined in types.ts:246

___

###  extension

• **extension**: *string*

*Inherited from [MediaBase](mediabase.md).[extension](mediabase.md#markdown-header-extension)*

Defined in types.ts:245

___

###  fileSize

• **fileSize**: *number*

*Inherited from [MediaBase](mediabase.md).[fileSize](mediabase.md#markdown-header-filesize)*

Defined in types.ts:248

___

###  fileType

• **fileType**: *[FileType](../enums/filetype.md)* = FileType.Audio

Defined in types.ts:267

___

###  filename

• **filename**: *string*

*Inherited from [MediaBase](mediabase.md).[filename](mediabase.md#markdown-header-filename)*

Defined in types.ts:247

___

###  imageInfo

• **imageInfo**: *null* = null

Defined in types.ts:268

___

###  mimeType

• **mimeType**: *string*

*Inherited from [MediaBase](mediabase.md).[mimeType](mediabase.md#markdown-header-mimetype)*

Defined in types.ts:244

___

###  urlPath

• **urlPath**: *string*

*Inherited from [MediaBase](mediabase.md).[urlPath](mediabase.md#markdown-header-urlpath)*

Defined in types.ts:243

___

###  videoInfo

• **videoInfo**: *object*

Defined in types.ts:269

#### Type declaration:

* **duration**: *number*

* **thumbnail**: *null*
