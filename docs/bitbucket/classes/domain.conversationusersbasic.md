[php-sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [Domain](../modules/domain.md) › [ConversationUsersBasic](domain.conversationusersbasic.md)

# Class: ConversationUsersBasic

## Hierarchy

* **ConversationUsersBasic**

## Index

### Properties

* [deletedFromConversationAt](domain.conversationusersbasic.md#markdown-header-deletedfromconversationat)
* [id](domain.conversationusersbasic.md#markdown-header-id)
* [isOwner](domain.conversationusersbasic.md#markdown-header-isowner)
* [userDeletedAt](domain.conversationusersbasic.md#markdown-header-userdeletedat)
* [userDisabled](domain.conversationusersbasic.md#markdown-header-userdisabled)

## Properties

###  deletedFromConversationAt

• **deletedFromConversationAt**: *Date | null*

Defined in types.ts:505

___

###  id

• **id**: *string*

Defined in types.ts:504

___

###  isOwner

• **isOwner**: *boolean*

Defined in types.ts:503

___

###  userDeletedAt

• **userDeletedAt**: *Date | null*

Defined in types.ts:507

___

###  userDisabled

• **userDisabled**: *boolean*

Defined in types.ts:506
