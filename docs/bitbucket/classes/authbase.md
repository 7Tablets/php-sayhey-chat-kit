[php-sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [AuthBase](authbase.md)

# Class: AuthBase

## Hierarchy

* **AuthBase**

  ↳ [Auth](dto.auth.md)

  ↳ [Auth](domain.auth.md)

## Index

### Properties

* [accessToken](authbase.md#markdown-header-accesstoken)
* [publisherToken](authbase.md#markdown-header-publishertoken)
* [refreshToken](authbase.md#markdown-header-refreshtoken)

## Properties

###  accessToken

• **accessToken**: *string*

Defined in types.ts:169

___

###  publisherToken

• **publisherToken**: *string*

Defined in types.ts:170

___

###  refreshToken

• **refreshToken**: *string*

Defined in types.ts:171
