[php-sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [Domain](../modules/domain.md) › [IndividualConversation](domain.individualconversation.md)

# Class: IndividualConversation

## Hierarchy

  ↳ [Conversation](domain.conversation.md)

  ↳ **IndividualConversation**

## Index

### Properties

* [badge](domain.individualconversation.md#markdown-header-badge)
* [createdAt](domain.individualconversation.md#markdown-header-createdat)
* [deletedFromConversationAt](domain.individualconversation.md#markdown-header-deletedfromconversationat)
* [endBefore](domain.individualconversation.md#markdown-header-endbefore)
* [id](domain.individualconversation.md#markdown-header-id)
* [isOwner](domain.individualconversation.md#markdown-header-isowner)
* [lastMessage](domain.individualconversation.md#markdown-header-lastmessage)
* [lastReadMessageId](domain.individualconversation.md#markdown-header-lastreadmessageid)
* [muteUntil](domain.individualconversation.md#markdown-header-muteuntil)
* [pinnedAt](domain.individualconversation.md#markdown-header-pinnedat)
* [serverCreated](domain.individualconversation.md#markdown-header-servercreated)
* [serverManaged](domain.individualconversation.md#markdown-header-servermanaged)
* [startAfter](domain.individualconversation.md#markdown-header-startafter)
* [type](domain.individualconversation.md#markdown-header-type)
* [updatedAt](domain.individualconversation.md#markdown-header-updatedat)
* [user](domain.individualconversation.md#markdown-header-user)
* [visible](domain.individualconversation.md#markdown-header-visible)

## Properties

###  badge

• **badge**: *number | null*

*Inherited from [Conversation](domain.conversation.md).[badge](domain.conversation.md#markdown-header-badge)*

Defined in types.ts:480

___

###  createdAt

• **createdAt**: *Date*

*Inherited from [Conversation](domain.conversation.md).[createdAt](domain.conversation.md#markdown-header-createdat)*

Defined in types.ts:484

___

###  deletedFromConversationAt

• **deletedFromConversationAt**: *Date | null*

*Inherited from [Conversation](domain.conversation.md).[deletedFromConversationAt](domain.conversation.md#markdown-header-deletedfromconversationat)*

Defined in types.ts:483

___

###  endBefore

• **endBefore**: *Date | null*

*Inherited from [Conversation](domain.conversation.md).[endBefore](domain.conversation.md#markdown-header-endbefore)*

Defined in types.ts:482

___

###  id

• **id**: *string*

*Inherited from [ConversationBase](conversationbase.md).[id](conversationbase.md#markdown-header-id)*

Defined in types.ts:350

___

###  isOwner

• **isOwner**: *boolean*

*Inherited from [ConversationBase](conversationbase.md).[isOwner](conversationbase.md#markdown-header-isowner)*

Defined in types.ts:353

___

###  lastMessage

• **lastMessage**: *[Message](domain.message.md) | null*

*Inherited from [Conversation](domain.conversation.md).[lastMessage](domain.conversation.md#markdown-header-lastmessage)*

Defined in types.ts:486

___

###  lastReadMessageId

• **lastReadMessageId**: *string | null*

*Inherited from [ConversationBase](conversationbase.md).[lastReadMessageId](conversationbase.md#markdown-header-lastreadmessageid)*

Defined in types.ts:351

___

###  muteUntil

• **muteUntil**: *Date | null*

*Inherited from [Conversation](domain.conversation.md).[muteUntil](domain.conversation.md#markdown-header-muteuntil)*

Defined in types.ts:478

___

###  pinnedAt

• **pinnedAt**: *Date | null*

*Inherited from [Conversation](domain.conversation.md).[pinnedAt](domain.conversation.md#markdown-header-pinnedat)*

Defined in types.ts:479

___

###  serverCreated

• **serverCreated**: *boolean*

*Inherited from [ConversationBase](conversationbase.md).[serverCreated](conversationbase.md#markdown-header-servercreated)*

Defined in types.ts:355

___

###  serverManaged

• **serverManaged**: *boolean*

*Inherited from [ConversationBase](conversationbase.md).[serverManaged](conversationbase.md#markdown-header-servermanaged)*

Defined in types.ts:356

___

###  startAfter

• **startAfter**: *Date | null*

*Inherited from [Conversation](domain.conversation.md).[startAfter](domain.conversation.md#markdown-header-startafter)*

Defined in types.ts:481

___

###  type

• **type**: *[Individual](../enums/conversationtype.md#markdown-header-individual)*

*Overrides [ConversationBase](conversationbase.md).[type](conversationbase.md#markdown-header-type)*

Defined in types.ts:493

___

###  updatedAt

• **updatedAt**: *Date*

*Inherited from [Conversation](domain.conversation.md).[updatedAt](domain.conversation.md#markdown-header-updatedat)*

Defined in types.ts:485

___

###  user

• **user**: *[UserConversationInfo](domain.userconversationinfo.md)*

Defined in types.ts:494

___

###  visible

• **visible**: *boolean*

*Inherited from [ConversationBase](conversationbase.md).[visible](conversationbase.md#markdown-header-visible)*

Defined in types.ts:354
