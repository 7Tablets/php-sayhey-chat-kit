[php-sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [MessageMapper](messagemapper.md)

# Class: MessageMapper

## Hierarchy

* **MessageMapper**

## Index

### Methods

* [toMessageDeletedDomain](messagemapper.md#markdown-header-static-tomessagedeleteddomain)
* [toMessageDomain](messagemapper.md#markdown-header-static-tomessagedomain)

## Methods

### `Static` toMessageDeletedDomain

▸ **toMessageDeletedDomain**(`messageEvent`: [MessageDeleted](dto.messagedeleted.md)): *[MessageDeleted](domain.messagedeleted.md)*

Defined in mapper.ts:75

**Parameters:**

Name | Type |
------ | ------ |
`messageEvent` | [MessageDeleted](dto.messagedeleted.md) |

**Returns:** *[MessageDeleted](domain.messagedeleted.md)*

___

### `Static` toMessageDomain

▸ **toMessageDomain**(`message`: [Message](dto.message.md)): *[Message](domain.message.md)*

Defined in mapper.ts:64

**Parameters:**

Name | Type |
------ | ------ |
`message` | [Message](dto.message.md) |

**Returns:** *[Message](domain.message.md)*
