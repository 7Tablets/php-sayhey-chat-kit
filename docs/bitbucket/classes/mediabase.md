[php-sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [MediaBase](mediabase.md)

# Class: MediaBase

## Hierarchy

* **MediaBase**

  ↳ [ImageBase](imagebase.md)

  ↳ [VideoBase](videobase.md)

  ↳ [AudioBase](audiobase.md)

  ↳ [DocumentBase](documentbase.md)

## Index

### Properties

* [encoding](mediabase.md#markdown-header-encoding)
* [extension](mediabase.md#markdown-header-extension)
* [fileSize](mediabase.md#markdown-header-filesize)
* [filename](mediabase.md#markdown-header-filename)
* [mimeType](mediabase.md#markdown-header-mimetype)
* [urlPath](mediabase.md#markdown-header-urlpath)

## Properties

###  encoding

• **encoding**: *string*

Defined in types.ts:246

___

###  extension

• **extension**: *string*

Defined in types.ts:245

___

###  fileSize

• **fileSize**: *number*

Defined in types.ts:248

___

###  filename

• **filename**: *string*

Defined in types.ts:247

___

###  mimeType

• **mimeType**: *string*

Defined in types.ts:244

___

###  urlPath

• **urlPath**: *string*

Defined in types.ts:243
