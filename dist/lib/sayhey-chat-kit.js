"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
var types_1 = require("./types");
var neverthrow_1 = require("neverthrow");
var constants_1 = require("./constants");
var axios_1 = require("./axios");
var mapper_1 = require("./mapper");
var ChatKitServer = /** @class */ (function () {
    function ChatKitServer() {
    }
    ChatKitServer.guard = function (cb) {
        return __awaiter(this, void 0, void 0, function () {
            var res, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!ChatKitServer._initialized) {
                            return [2 /*return*/, neverthrow_1.err(new types_1.AppError('ChatKit has not been initialized!', types_1.ErrorType.InternalErrorType.Uninitialized))];
                        }
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, cb()];
                    case 2:
                        res = _a.sent();
                        if (res instanceof types_1.AppError) {
                            return [2 /*return*/, neverthrow_1.err(res)];
                        }
                        return [2 /*return*/, neverthrow_1.ok(res)];
                    case 3:
                        e_1 = _a.sent();
                        if (e_1 instanceof types_1.AppError) {
                            return [2 /*return*/, neverthrow_1.err(e_1)];
                        }
                        return [2 /*return*/, neverthrow_1.err(new types_1.AppError('Unknown error', types_1.ErrorType.InternalErrorType.Unknown))];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    ChatKitServer.initialize = function (options) {
        var sayheyApiKey = options.sayheyApiKey, sayheyAppId = options.sayheyAppId, apiVersion = options.apiVersion, sayheyUrl = options.sayheyUrl;
        ChatKitServer.apiKey = sayheyApiKey;
        ChatKitServer.appId = sayheyAppId;
        ChatKitServer.apiVersion = apiVersion || constants_1.API_VERSION;
        ChatKitServer.httpClient = axios_1.default({
            baseURL: sayheyUrl + "/" + ChatKitServer.apiVersion + "/apps/" + ChatKitServer.appId,
            apiKey: ChatKitServer.apiKey
        });
        ChatKitServer._initialized = true;
    };
    ChatKitServer.getFile = function (url, method) {
        return __awaiter(this, void 0, void 0, function () {
            var data;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, ChatKitServer.httpClient({
                            url: url,
                            method: method,
                            responseType: 'blob'
                        })];
                    case 1:
                        data = (_a.sent()).data;
                        return [2 /*return*/, data];
                }
            });
        });
    };
    ChatKitServer.streamFile = function (url, method) {
        return __awaiter(this, void 0, void 0, function () {
            var data;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, ChatKitServer.httpClient({
                            url: url,
                            method: method,
                            responseType: 'stream'
                        })];
                    case 1:
                        data = (_a.sent()).data;
                        return [2 /*return*/, data];
                }
            });
        });
    };
    ChatKitServer.uploadFile = function (file, filename) {
        return __awaiter(this, void 0, void 0, function () {
            var _a, Endpoint, Method, body, data;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = constants_1.Routes.UploadFile, Endpoint = _a.Endpoint, Method = _a.Method;
                        body = new FormData();
                        body.append('file', file, filename);
                        return [4 /*yield*/, ChatKitServer.httpClient({
                                url: Endpoint,
                                method: Method,
                                data: body
                            })];
                    case 1:
                        data = (_b.sent()).data;
                        return [2 /*return*/, data.id];
                }
            });
        });
    };
    ChatKitServer.uploadFileWithStream = function (readableStream) {
        return __awaiter(this, void 0, void 0, function () {
            var _a, Endpoint, Method, body, data;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = constants_1.Routes.UploadFile, Endpoint = _a.Endpoint, Method = _a.Method;
                        body = new FormData();
                        body.append('file', readableStream);
                        return [4 /*yield*/, ChatKitServer.httpClient({
                                url: Endpoint,
                                method: Method,
                                data: body
                            })];
                    case 1:
                        data = (_b.sent()).data;
                        return [2 /*return*/, data.id];
                }
            });
        });
    };
    /**
     * App APIs
     */
    ChatKitServer.getDefaultProfileImagesForGroup = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, ChatKitServer.guard(function getDefaultProfileImagesForGroup() {
                        return __awaiter(this, void 0, void 0, function () {
                            var _a, Endpoint, Method, data;
                            return __generator(this, function (_b) {
                                switch (_b.label) {
                                    case 0:
                                        _a = constants_1.Routes.GetDefaultProfileImagesForGroup, Endpoint = _a.Endpoint, Method = _a.Method;
                                        return [4 /*yield*/, ChatKitServer.httpClient({
                                                url: Endpoint,
                                                method: Method
                                            })];
                                    case 1:
                                        data = (_b.sent()).data;
                                        return [2 /*return*/, mapper_1.AppMapper.toProfileImageDomainList(data)];
                                }
                            });
                        });
                    })];
            });
        });
    };
    ChatKitServer.viewDefaultProfileImageForGroup = function (options) {
        return __awaiter(this, void 0, void 0, function () {
            var filename, original, stream;
            return __generator(this, function (_a) {
                filename = options.filename, original = options.original, stream = options.stream;
                return [2 /*return*/, ChatKitServer.guard(function viewDefaultProfileImageForGroup() {
                        return __awaiter(this, void 0, void 0, function () {
                            var _a, Endpoint, Method, url, data_1, data;
                            return __generator(this, function (_b) {
                                switch (_b.label) {
                                    case 0:
                                        _a = constants_1.Routes.ViewDefaultProfileImageForGroup, Endpoint = _a.Endpoint, Method = _a.Method;
                                        url = Endpoint(filename, !original ? '?size=thumbnail' : '');
                                        if (!stream) return [3 /*break*/, 2];
                                        return [4 /*yield*/, ChatKitServer.streamFile(url, Method)];
                                    case 1:
                                        data_1 = _b.sent();
                                        return [2 /*return*/, data_1];
                                    case 2: return [4 /*yield*/, ChatKitServer.getFile(url, Method)];
                                    case 3:
                                        data = _b.sent();
                                        return [2 /*return*/, data];
                                }
                            });
                        });
                    })];
            });
        });
    };
    /**
     * User APIs
     */
    ChatKitServer.signUpUser = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, ChatKitServer.guard(function signUpUser() {
                        return __awaiter(this, void 0, void 0, function () {
                            var _a, Endpoint, Method, data;
                            return __generator(this, function (_b) {
                                switch (_b.label) {
                                    case 0:
                                        _a = constants_1.Routes.SignUpUser, Endpoint = _a.Endpoint, Method = _a.Method;
                                        return [4 /*yield*/, ChatKitServer.httpClient({
                                                url: Endpoint,
                                                method: Method,
                                                data: params
                                            })];
                                    case 1:
                                        data = (_b.sent()).data;
                                        return [2 /*return*/, mapper_1.AppMapper.toAuthDomain(data)];
                                }
                            });
                        });
                    })];
            });
        });
    };
    ChatKitServer.signInUserWithEmail = function (email) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, ChatKitServer.guard(function signInUserWithEmail() {
                        return __awaiter(this, void 0, void 0, function () {
                            var _a, Endpoint, Method, data;
                            return __generator(this, function (_b) {
                                switch (_b.label) {
                                    case 0:
                                        _a = constants_1.Routes.SignInUserWithEmail, Endpoint = _a.Endpoint, Method = _a.Method;
                                        return [4 /*yield*/, ChatKitServer.httpClient({
                                                url: Endpoint,
                                                method: Method,
                                                data: {
                                                    email: email
                                                }
                                            })];
                                    case 1:
                                        data = (_b.sent()).data;
                                        return [2 /*return*/, mapper_1.AppMapper.toAuthDomain(data)];
                                }
                            });
                        });
                    })];
            });
        });
    };
    ChatKitServer.signInUserWithId = function (id) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, ChatKitServer.guard(function signInUserWithId() {
                        return __awaiter(this, void 0, void 0, function () {
                            var _a, Endpoint, Method, data;
                            return __generator(this, function (_b) {
                                switch (_b.label) {
                                    case 0:
                                        _a = constants_1.Routes.SignInUserWithId, Endpoint = _a.Endpoint, Method = _a.Method;
                                        return [4 /*yield*/, ChatKitServer.httpClient({
                                                url: Endpoint,
                                                method: Method,
                                                data: {
                                                    id: id
                                                }
                                            })];
                                    case 1:
                                        data = (_b.sent()).data;
                                        return [2 /*return*/, mapper_1.AppMapper.toAuthDomain(data)];
                                }
                            });
                        });
                    })];
            });
        });
    };
    ChatKitServer.getUserProfile = function (userId) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, ChatKitServer.guard(function getUserProfile() {
                        return __awaiter(this, void 0, void 0, function () {
                            var _a, Endpoint, Method, data;
                            return __generator(this, function (_b) {
                                switch (_b.label) {
                                    case 0:
                                        _a = constants_1.Routes.GetUserProfile, Endpoint = _a.Endpoint, Method = _a.Method;
                                        return [4 /*yield*/, ChatKitServer.httpClient({
                                                url: Endpoint(userId),
                                                method: Method
                                            })];
                                    case 1:
                                        data = (_b.sent()).data;
                                        return [2 /*return*/, mapper_1.UserMapper.toUserSelfDomain(data)];
                                }
                            });
                        });
                    })];
            });
        });
    };
    ChatKitServer.updateUserProfileInfo = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var userId, body;
            return __generator(this, function (_a) {
                userId = params.userId, body = __rest(params, ["userId"]);
                return [2 /*return*/, ChatKitServer.guard(function updateUserProfileInfo() {
                        return __awaiter(this, void 0, void 0, function () {
                            var _a, Endpoint, Method;
                            return __generator(this, function (_b) {
                                switch (_b.label) {
                                    case 0:
                                        _a = constants_1.Routes.UpdateUserProfileInfo, Endpoint = _a.Endpoint, Method = _a.Method;
                                        return [4 /*yield*/, ChatKitServer.httpClient({
                                                url: Endpoint(userId),
                                                method: Method,
                                                data: body
                                            })];
                                    case 1:
                                        _b.sent();
                                        return [2 /*return*/, true];
                                }
                            });
                        });
                    })];
            });
        });
    };
    ChatKitServer.updateUserProfileImage = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var userId, body;
            return __generator(this, function (_a) {
                userId = params.userId, body = __rest(params, ["userId"]);
                return [2 /*return*/, ChatKitServer.guard(function updateUserProfileImage() {
                        return __awaiter(this, void 0, void 0, function () {
                            var _a, Endpoint, Method;
                            return __generator(this, function (_b) {
                                switch (_b.label) {
                                    case 0:
                                        _a = constants_1.Routes.UpdateUserProfileImage, Endpoint = _a.Endpoint, Method = _a.Method;
                                        return [4 /*yield*/, ChatKitServer.httpClient({
                                                url: Endpoint(userId),
                                                method: Method,
                                                data: body
                                            })];
                                    case 1:
                                        _b.sent();
                                        return [2 /*return*/, true];
                                }
                            });
                        });
                    })];
            });
        });
    };
    ChatKitServer.removeUserProfileImage = function (userId) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, ChatKitServer.guard(function removeUserProfileImage() {
                        return __awaiter(this, void 0, void 0, function () {
                            var _a, Endpoint, Method;
                            return __generator(this, function (_b) {
                                switch (_b.label) {
                                    case 0:
                                        _a = constants_1.Routes.UpdateUserProfileImage, Endpoint = _a.Endpoint, Method = _a.Method;
                                        return [4 /*yield*/, ChatKitServer.httpClient({
                                                url: Endpoint(userId),
                                                method: Method
                                            })];
                                    case 1:
                                        _b.sent();
                                        return [2 /*return*/, true];
                                }
                            });
                        });
                    })];
            });
        });
    };
    ChatKitServer.disableUser = function (userId) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, ChatKitServer.guard(function disableUser() {
                        return __awaiter(this, void 0, void 0, function () {
                            var _a, Endpoint, Method;
                            return __generator(this, function (_b) {
                                switch (_b.label) {
                                    case 0:
                                        _a = constants_1.Routes.DisableUser, Endpoint = _a.Endpoint, Method = _a.Method;
                                        return [4 /*yield*/, ChatKitServer.httpClient({
                                                url: Endpoint(userId),
                                                method: Method
                                            })];
                                    case 1:
                                        _b.sent();
                                        return [2 /*return*/, true];
                                }
                            });
                        });
                    })];
            });
        });
    };
    ChatKitServer.enableUser = function (userId) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, ChatKitServer.guard(function enableUser() {
                        return __awaiter(this, void 0, void 0, function () {
                            var _a, Endpoint, Method;
                            return __generator(this, function (_b) {
                                switch (_b.label) {
                                    case 0:
                                        _a = constants_1.Routes.EnableUser, Endpoint = _a.Endpoint, Method = _a.Method;
                                        return [4 /*yield*/, ChatKitServer.httpClient({
                                                url: Endpoint(userId),
                                                method: Method
                                            })];
                                    case 1:
                                        _b.sent();
                                        return [2 /*return*/, true];
                                }
                            });
                        });
                    })];
            });
        });
    };
    ChatKitServer.deleteUser = function (userId) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, ChatKitServer.guard(function deleteUser() {
                        return __awaiter(this, void 0, void 0, function () {
                            var _a, Endpoint, Method;
                            return __generator(this, function (_b) {
                                switch (_b.label) {
                                    case 0:
                                        _a = constants_1.Routes.DeleteUser, Endpoint = _a.Endpoint, Method = _a.Method;
                                        return [4 /*yield*/, ChatKitServer.httpClient({
                                                url: Endpoint(userId),
                                                method: Method
                                            })];
                                    case 1:
                                        _b.sent();
                                        return [2 /*return*/, true];
                                }
                            });
                        });
                    })];
            });
        });
    };
    /**
     * Conversation APIs
     */
    ChatKitServer.getConversationConversableUsers = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var conversationId, queryString;
            return __generator(this, function (_a) {
                conversationId = params.conversationId, queryString = __rest(params, ["conversationId"]);
                return [2 /*return*/, ChatKitServer.guard(function getConversationConversableUsers() {
                        return __awaiter(this, void 0, void 0, function () {
                            var _a, Endpoint, Method, data;
                            return __generator(this, function (_b) {
                                switch (_b.label) {
                                    case 0:
                                        _a = constants_1.Routes.GetConversationConversableUsers, Endpoint = _a.Endpoint, Method = _a.Method;
                                        return [4 /*yield*/, ChatKitServer.httpClient({
                                                url: Endpoint(conversationId),
                                                method: Method,
                                                params: queryString
                                            })];
                                    case 1:
                                        data = (_b.sent()).data;
                                        return [2 /*return*/, mapper_1.ConversationMapper.toConversationConversableUsersBatchDomain(data)];
                                }
                            });
                        });
                    })];
            });
        });
    };
    ChatKitServer.getConversationUsers = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var conversationId, _a, type, otherParams;
            return __generator(this, function (_b) {
                conversationId = params.conversationId, _a = params.type, type = _a === void 0 ? 'current' : _a, otherParams = __rest(params, ["conversationId", "type"]);
                return [2 /*return*/, ChatKitServer.guard(function getConversationUsers() {
                        return __awaiter(this, void 0, void 0, function () {
                            var _a, Endpoint, Method, data;
                            return __generator(this, function (_b) {
                                switch (_b.label) {
                                    case 0:
                                        _a = constants_1.Routes.GetConversationUsers, Endpoint = _a.Endpoint, Method = _a.Method;
                                        return [4 /*yield*/, ChatKitServer.httpClient({
                                                url: Endpoint(conversationId),
                                                method: Method,
                                                params: __assign(__assign({}, otherParams), { type: type })
                                            })];
                                    case 1:
                                        data = (_b.sent()).data;
                                        return [2 /*return*/, mapper_1.ConversationMapper.toConversationUsersBatchDomain(data)];
                                }
                            });
                        });
                    })];
            });
        });
    };
    ChatKitServer.getConversationBasicUsers = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var conversationId, _a, type;
            return __generator(this, function (_b) {
                conversationId = params.conversationId, _a = params.type, type = _a === void 0 ? 'current' : _a;
                return [2 /*return*/, ChatKitServer.guard(function getConversationUsers() {
                        return __awaiter(this, void 0, void 0, function () {
                            var _a, Endpoint, Method, data;
                            return __generator(this, function (_b) {
                                switch (_b.label) {
                                    case 0:
                                        _a = constants_1.Routes.GetConversationBasicUsers, Endpoint = _a.Endpoint, Method = _a.Method;
                                        return [4 /*yield*/, ChatKitServer.httpClient({
                                                url: Endpoint(conversationId),
                                                method: Method,
                                                params: {
                                                    type: type
                                                }
                                            })];
                                    case 1:
                                        data = (_b.sent()).data;
                                        return [2 /*return*/, data.map(mapper_1.UserMapper.toConversationUserBasicDomain)];
                                }
                            });
                        });
                    })];
            });
        });
    };
    ChatKitServer.addUsersToConversation = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var conversationId, force, memberIds;
            return __generator(this, function (_a) {
                conversationId = params.conversationId, force = params.force, memberIds = params.memberIds;
                return [2 /*return*/, ChatKitServer.guard(function addUsersToConversation() {
                        return __awaiter(this, void 0, void 0, function () {
                            var _a, Endpoint, Method;
                            return __generator(this, function (_b) {
                                switch (_b.label) {
                                    case 0:
                                        _a = constants_1.Routes.AddUsersToConversation, Endpoint = _a.Endpoint, Method = _a.Method;
                                        return [4 /*yield*/, ChatKitServer.httpClient({
                                                url: Endpoint(conversationId),
                                                method: Method,
                                                data: {
                                                    memberIds: memberIds
                                                },
                                                params: {
                                                    force: force
                                                }
                                            })];
                                    case 1:
                                        _b.sent();
                                        return [2 /*return*/, true];
                                }
                            });
                        });
                    })];
            });
        });
    };
    ChatKitServer.removeUsersFromConversation = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var conversationId, force, memberIds;
            return __generator(this, function (_a) {
                conversationId = params.conversationId, force = params.force, memberIds = params.memberIds;
                return [2 /*return*/, ChatKitServer.guard(function removeUsersToConversation() {
                        return __awaiter(this, void 0, void 0, function () {
                            var _a, Endpoint, Method;
                            return __generator(this, function (_b) {
                                switch (_b.label) {
                                    case 0:
                                        _a = constants_1.Routes.RemoveUsersFromConversation, Endpoint = _a.Endpoint, Method = _a.Method;
                                        return [4 /*yield*/, ChatKitServer.httpClient({
                                                url: Endpoint(conversationId),
                                                method: Method,
                                                data: {
                                                    memberIds: memberIds
                                                },
                                                params: {
                                                    force: force
                                                }
                                            })];
                                    case 1:
                                        _b.sent();
                                        return [2 /*return*/, true];
                                }
                            });
                        });
                    })];
            });
        });
    };
    ChatKitServer.makeConversationOwner = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var conversationId, userId;
            return __generator(this, function (_a) {
                conversationId = params.conversationId, userId = params.userId;
                return [2 /*return*/, ChatKitServer.guard(function makeConversationOwner() {
                        return __awaiter(this, void 0, void 0, function () {
                            var _a, Endpoint, Method;
                            return __generator(this, function (_b) {
                                switch (_b.label) {
                                    case 0:
                                        _a = constants_1.Routes.MakeConversationOwner, Endpoint = _a.Endpoint, Method = _a.Method;
                                        return [4 /*yield*/, ChatKitServer.httpClient({
                                                url: Endpoint(conversationId, userId),
                                                method: Method
                                            })];
                                    case 1:
                                        _b.sent();
                                        return [2 /*return*/, true];
                                }
                            });
                        });
                    })];
            });
        });
    };
    ChatKitServer.removeConversationOwner = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var conversationId, userId;
            return __generator(this, function (_a) {
                conversationId = params.conversationId, userId = params.userId;
                return [2 /*return*/, ChatKitServer.guard(function removeConversationOwner() {
                        return __awaiter(this, void 0, void 0, function () {
                            var _a, Endpoint, Method;
                            return __generator(this, function (_b) {
                                switch (_b.label) {
                                    case 0:
                                        _a = constants_1.Routes.RemoveConversationOwner, Endpoint = _a.Endpoint, Method = _a.Method;
                                        return [4 /*yield*/, ChatKitServer.httpClient({
                                                url: Endpoint(conversationId, userId),
                                                method: Method
                                            })];
                                    case 1:
                                        _b.sent();
                                        return [2 /*return*/, true];
                                }
                            });
                        });
                    })];
            });
        });
    };
    ChatKitServer.deleteAllUsersInConversation = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var conversationId;
            return __generator(this, function (_a) {
                conversationId = params.conversationId;
                return [2 /*return*/, ChatKitServer.guard(function deleteAllUsersInConversation() {
                        return __awaiter(this, void 0, void 0, function () {
                            var _a, Endpoint, Method;
                            return __generator(this, function (_b) {
                                switch (_b.label) {
                                    case 0:
                                        _a = constants_1.Routes.DeleteAllUsersInConversation, Endpoint = _a.Endpoint, Method = _a.Method;
                                        return [4 /*yield*/, ChatKitServer.httpClient({
                                                url: Endpoint(conversationId),
                                                method: Method
                                            })];
                                    case 1:
                                        _b.sent();
                                        return [2 /*return*/, true];
                                }
                            });
                        });
                    })];
            });
        });
    };
    ChatKitServer.createGroupWithOwner = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, ChatKitServer.guard(function removeConversationOwner() {
                        return __awaiter(this, void 0, void 0, function () {
                            var _a, Endpoint, Method, data;
                            return __generator(this, function (_b) {
                                switch (_b.label) {
                                    case 0:
                                        _a = constants_1.Routes.CreateGroupWithOwner, Endpoint = _a.Endpoint, Method = _a.Method;
                                        return [4 /*yield*/, ChatKitServer.httpClient({
                                                url: Endpoint,
                                                method: Method,
                                                data: params
                                            })];
                                    case 1:
                                        data = (_b.sent()).data;
                                        return [2 /*return*/, mapper_1.ConversationMapper.toGenericConversationDomain(data)];
                                }
                            });
                        });
                    })];
            });
        });
    };
    ChatKitServer.updateGroupInfo = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var conversationId, body;
            return __generator(this, function (_a) {
                conversationId = params.conversationId, body = __rest(params, ["conversationId"]);
                return [2 /*return*/, ChatKitServer.guard(function updateGroupInfo() {
                        return __awaiter(this, void 0, void 0, function () {
                            var _a, Endpoint, Method;
                            return __generator(this, function (_b) {
                                switch (_b.label) {
                                    case 0:
                                        _a = constants_1.Routes.UpdateGroupInfo, Endpoint = _a.Endpoint, Method = _a.Method;
                                        return [4 /*yield*/, ChatKitServer.httpClient({
                                                url: Endpoint(conversationId),
                                                method: Method,
                                                data: body
                                            })];
                                    case 1:
                                        _b.sent();
                                        return [2 /*return*/, true];
                                }
                            });
                        });
                    })];
            });
        });
    };
    ChatKitServer.updateGroupImage = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var conversationId, body;
            return __generator(this, function (_a) {
                conversationId = params.conversationId, body = __rest(params, ["conversationId"]);
                return [2 /*return*/, ChatKitServer.guard(function updateGroupImage() {
                        return __awaiter(this, void 0, void 0, function () {
                            var _a, Endpoint, Method;
                            return __generator(this, function (_b) {
                                switch (_b.label) {
                                    case 0:
                                        _a = constants_1.Routes.UpdateGroupImage, Endpoint = _a.Endpoint, Method = _a.Method;
                                        return [4 /*yield*/, ChatKitServer.httpClient({
                                                url: Endpoint(conversationId),
                                                method: Method,
                                                data: body
                                            })];
                                    case 1:
                                        _b.sent();
                                        return [2 /*return*/, true];
                                }
                            });
                        });
                    })];
            });
        });
    };
    ChatKitServer.removeGroupImage = function (conversationId) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, ChatKitServer.guard(function removeGroupImage() {
                        return __awaiter(this, void 0, void 0, function () {
                            var _a, Endpoint, Method;
                            return __generator(this, function (_b) {
                                switch (_b.label) {
                                    case 0:
                                        _a = constants_1.Routes.RemoveGroupImage, Endpoint = _a.Endpoint, Method = _a.Method;
                                        return [4 /*yield*/, ChatKitServer.httpClient({
                                                url: Endpoint(conversationId),
                                                method: Method
                                            })];
                                    case 1:
                                        _b.sent();
                                        return [2 /*return*/, true];
                                }
                            });
                        });
                    })];
            });
        });
    };
    /**
     * Message APIs
     */
    ChatKitServer.getMessages = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, ChatKitServer.guard(function getMessages() {
                        var _a, _b;
                        return __awaiter(this, void 0, void 0, function () {
                            var _c, Endpoint, Method, data;
                            return __generator(this, function (_d) {
                                switch (_d.label) {
                                    case 0:
                                        _c = constants_1.Routes.GetMessages, Endpoint = _c.Endpoint, Method = _c.Method;
                                        return [4 /*yield*/, ChatKitServer.httpClient({
                                                url: Endpoint,
                                                method: Method,
                                                params: __assign(__assign({}, params), { fromDate: (_a = params.fromDate) === null || _a === void 0 ? void 0 : _a.toISOString(), toDate: (_b = params.toDate) === null || _b === void 0 ? void 0 : _b.toISOString() })
                                            })];
                                    case 1:
                                        data = (_d.sent()).data;
                                        return [2 /*return*/, data.map(mapper_1.MessageMapper.toMessageDomain)];
                                }
                            });
                        });
                    })];
            });
        });
    };
    ChatKitServer.deleteMessage = function (messageId) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, ChatKitServer.guard(function deleteMessage() {
                        return __awaiter(this, void 0, void 0, function () {
                            var _a, Endpoint, Method, data;
                            return __generator(this, function (_b) {
                                switch (_b.label) {
                                    case 0:
                                        _a = constants_1.Routes.DeleteMessage, Endpoint = _a.Endpoint, Method = _a.Method;
                                        return [4 /*yield*/, ChatKitServer.httpClient({
                                                url: Endpoint(messageId),
                                                method: Method
                                            })];
                                    case 1:
                                        data = (_b.sent()).data;
                                        return [2 /*return*/, mapper_1.MessageMapper.toMessageDeletedDomain(data)];
                                }
                            });
                        });
                    })];
            });
        });
    };
    /**
     * Media APIs
     */
    ChatKitServer.getGroupProfileImage = function (options) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, ChatKitServer.guard(function getGroupProfileImage() {
                        return __awaiter(this, void 0, void 0, function () {
                            var _a, Endpoint, Method, url, data_2, data;
                            return __generator(this, function (_b) {
                                switch (_b.label) {
                                    case 0:
                                        _a = constants_1.Routes.GetConversationGroupImage, Endpoint = _a.Endpoint, Method = _a.Method;
                                        url = Endpoint(options.conversationId);
                                        if (!options.stream) return [3 /*break*/, 2];
                                        return [4 /*yield*/, ChatKitServer.streamFile(url, Method)];
                                    case 1:
                                        data_2 = _b.sent();
                                        return [2 /*return*/, data_2];
                                    case 2: return [4 /*yield*/, ChatKitServer.httpClient({
                                            url: url,
                                            method: Method,
                                            params: {
                                                noRedirect: true
                                            }
                                        })];
                                    case 3:
                                        data = (_b.sent()).data;
                                        return [2 /*return*/, data.url];
                                }
                            });
                        });
                    })];
            });
        });
    };
    ChatKitServer.getFileMessage = function (options) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, ChatKitServer.guard(function getGroupProfileImage() {
                        return __awaiter(this, void 0, void 0, function () {
                            var _a, Endpoint, Method, url, data_3, data;
                            return __generator(this, function (_b) {
                                switch (_b.label) {
                                    case 0:
                                        _a = constants_1.Routes.GetFileMessage, Endpoint = _a.Endpoint, Method = _a.Method;
                                        url = Endpoint(options.messageId, options.fileLocalIdName);
                                        if (!options.stream) return [3 /*break*/, 2];
                                        return [4 /*yield*/, ChatKitServer.streamFile(url, Method)];
                                    case 1:
                                        data_3 = _b.sent();
                                        return [2 /*return*/, data_3];
                                    case 2: return [4 /*yield*/, ChatKitServer.httpClient({
                                            url: url,
                                            method: Method,
                                            params: {
                                                noRedirect: true
                                            }
                                        })];
                                    case 3:
                                        data = (_b.sent()).data;
                                        return [2 /*return*/, data.url];
                                }
                            });
                        });
                    })];
            });
        });
    };
    ChatKitServer.getVideoThumbnail = function (options) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, ChatKitServer.guard(function getGroupProfileImage() {
                        return __awaiter(this, void 0, void 0, function () {
                            var _a, Endpoint, Method, url, data_4, data;
                            return __generator(this, function (_b) {
                                switch (_b.label) {
                                    case 0:
                                        _a = constants_1.Routes.GetFileMessage, Endpoint = _a.Endpoint, Method = _a.Method;
                                        url = Endpoint(options.messageId, options.fileLocalIdName);
                                        if (!options.stream) return [3 /*break*/, 2];
                                        return [4 /*yield*/, ChatKitServer.streamFile(url, Method)];
                                    case 1:
                                        data_4 = _b.sent();
                                        return [2 /*return*/, data_4];
                                    case 2: return [4 /*yield*/, ChatKitServer.httpClient({
                                            url: url,
                                            method: Method,
                                            params: {
                                                noRedirect: true
                                            }
                                        })];
                                    case 3:
                                        data = (_b.sent()).data;
                                        return [2 /*return*/, data.url];
                                }
                            });
                        });
                    })];
            });
        });
    };
    ChatKitServer._initialized = false;
    return ChatKitServer;
}());
exports.default = ChatKitServer;
//# sourceMappingURL=sayhey-chat-kit.js.map