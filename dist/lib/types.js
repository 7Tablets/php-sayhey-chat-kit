"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var _a, _b, _c;
Object.defineProperty(exports, "__esModule", { value: true });
exports.Domain = exports.DTO = exports.ConversationBase = exports.ConversationType = exports.MessageBase = exports.ReactionsObject = exports.ReactionType = exports.MessageType = exports.SystemMessageBase = exports.UserSimpleInfo = exports.SystemMessageType = exports.DocumentBase = exports.AudioBase = exports.VideoBase = exports.ImageBase = exports.MediaBase = exports.ConversationUsersBatchBase = exports.ImageInfoMedia = exports.SubImageMinimal = exports.ImageSizes = exports.FileType = exports.AuthBase = exports.DefaultGroupProfileImageBase = exports.AppError = exports.ErrorType = exports.HttpMethod = void 0;
var HttpMethod;
(function (HttpMethod) {
    HttpMethod["Post"] = "post";
    HttpMethod["Get"] = "get";
    HttpMethod["Delete"] = "delete";
    HttpMethod["Patch"] = "patch";
    HttpMethod["Put"] = "put";
})(HttpMethod = exports.HttpMethod || (exports.HttpMethod = {}));
var ErrorType;
(function (ErrorType) {
    var InternalErrorType;
    (function (InternalErrorType) {
        InternalErrorType["Unknown"] = "InternalUnknown";
        InternalErrorType["Uninitialized"] = "InternalUninitialized";
        InternalErrorType["AuthMissing"] = "InternalAuthMissing";
        InternalErrorType["BadData"] = "InternalBadData";
        InternalErrorType["MissingParameter"] = "InternalMissingParameter";
    })(InternalErrorType = ErrorType.InternalErrorType || (ErrorType.InternalErrorType = {}));
    var AuthErrorType;
    (function (AuthErrorType) {
        AuthErrorType["InvalidKeyErrorType"] = "AuthErrorTypeInvalidKey";
        AuthErrorType["InvalidUserType"] = "AuthErrorTypeInvalidUserType";
        AuthErrorType["NotFoundErrorType"] = "AuthErrorTypeNotFound";
        AuthErrorType["MissingParamsErrorType"] = "AuthErrorTypeMissingParams";
        AuthErrorType["InvalidPassword"] = "AuthErrorTypeInvalidPassword";
        AuthErrorType["TokenExpiredType"] = "AuthErrorTypeTokenExpired";
        AuthErrorType["AppHasNoNotificationSetup"] = "AuthErrorTypeAppHasNoNotificationSetup";
        AuthErrorType["NotPartOfApp"] = "AuthErrorTypeNotPartOfApp";
        AuthErrorType["InvalidWebhookKey"] = "AuthErrorTypeInvalidWebhookKey";
        AuthErrorType["NoAccessToUser"] = "AuthErrorTypeNoAccessToUser";
        AuthErrorType["ServerNoAccessToUser"] = "AuthErrorTypeServerNoAccessToUser";
        AuthErrorType["IncorrectPassword"] = "AuthErrorTypeIncorrectPassword";
        AuthErrorType["NoSamePassword"] = "AuthErrorTypeNoSamePassword";
        AuthErrorType["NoAppAccessToUser"] = "AuthErrorTypeNoAppAccessToUser";
        AuthErrorType["TokenNotStarted"] = "AuthErrorTypeTokenNotStarted";
        AuthErrorType["AccountAlreadyExists"] = "AuthErrorTypeAccountAlreadyExists";
        AuthErrorType["UserNotFound"] = "AuthErrorTypeUserNotFound";
        AuthErrorType["InvalidToken"] = "AuthErrorTypeInvalidToken";
        AuthErrorType["MissingAuthHeaderField"] = "AuthErrorTypeMissingAuthHeaderField";
        AuthErrorType["InvalidCredentials"] = "AuthErrorTypeInvalidCredentials";
        AuthErrorType["HashGeneration"] = "AuthErrorTypeHashGeneration";
        AuthErrorType["HashComparison"] = "AuthErrorTypeHashComparison";
        AuthErrorType["RefreshTokenAlreadyExchanged"] = "AuthErrorTypeRefreshTokenAlreadyExchanged";
        AuthErrorType["RefreshTokenNotFound"] = "AuthErrorTypeRefreshTokenNotFound";
        AuthErrorType["UserDisabled"] = "AuthErrorTypeUserDisabled";
    })(AuthErrorType = ErrorType.AuthErrorType || (ErrorType.AuthErrorType = {}));
    var FileErrorType;
    (function (FileErrorType) {
        FileErrorType["SizeLimitExceeded"] = "FileErrorTypeSizeLimitExceeded";
        FileErrorType["NoFileSentType"] = "FileErrorTypeNoFileSentType";
        FileErrorType["InvalidFieldNameForFile"] = "FileErrorTypeInvalidFieldNameForFile";
        FileErrorType["InvalidMetadata"] = "FileErrorTypeInvalidMetadata";
        FileErrorType["FileSizeLimitExceeded"] = "FileErrorTypeFileSizeLimitExceeded";
        FileErrorType["InvalidFileName"] = "FileErrorTypeInvalidFileName";
        FileErrorType["NotSent"] = "FileErrorTypeNotSent";
        FileErrorType["NotFound"] = "FileErrorTypeNotFound";
        FileErrorType["NoThumbnailFound"] = "FileErrorTypeNoThumbnailFound";
        FileErrorType["ImageNotForConversation"] = "FileErrorTypeImageNotForConversation";
        FileErrorType["ImageNotForUser"] = "FileErrorTypeImageNotForUser";
        FileErrorType["FileNotForMessage"] = "FileErrorTypeFileNotForMessage";
        FileErrorType["SizeNotFound"] = "FileErrorTypeSizeNotFound";
        FileErrorType["NotForApp"] = "FileErrorTypeNotForApp";
        FileErrorType["IncorrectFileType"] = "FileErrorTypeIncorrectFileType";
    })(FileErrorType = ErrorType.FileErrorType || (ErrorType.FileErrorType = {}));
    var ServerErrorType;
    (function (ServerErrorType) {
        ServerErrorType["UnknownError"] = "UnknownError";
        ServerErrorType["InternalServerError"] = "InternalServerError";
    })(ServerErrorType = ErrorType.ServerErrorType || (ErrorType.ServerErrorType = {}));
    var GeneralErrorType;
    (function (GeneralErrorType) {
        GeneralErrorType["MissingQueryParams"] = "GeneralErrorTypeMissingQueryParams";
        GeneralErrorType["RouteNotFound"] = "GeneralErrorTypeRouteNotFound";
        GeneralErrorType["LimitNotValid"] = "GeneralErrorTypeLimitNotValid";
        GeneralErrorType["OffsetNotValid"] = "GeneralErrorTypeOffsetNotValid";
    })(GeneralErrorType = ErrorType.GeneralErrorType || (ErrorType.GeneralErrorType = {}));
    var MessageErrorType;
    (function (MessageErrorType) {
        MessageErrorType["InvalidDate"] = "MessageErrorTypeInvalidDate";
        MessageErrorType["NotFound"] = "MessageErrorTypeNotFound";
        MessageErrorType["NotFoundForUser"] = "MessageErrorTypeNotFoundForUser";
        MessageErrorType["MessageAlreadyRead"] = "MessageErrorTypeMessageAlreadyRead";
        MessageErrorType["CannotDeleteSystemMessage"] = "MessageErrorTypeCannotDeleteSystemMessage";
    })(MessageErrorType = ErrorType.MessageErrorType || (ErrorType.MessageErrorType = {}));
    var DateErrorType;
    (function (DateErrorType) {
        DateErrorType["InvalidFromDateError"] = "DateErrorTypeInvalidFromDateError";
        DateErrorType["InvalidToDateError"] = "DateErrorTypeInvalidToDateError";
    })(DateErrorType = ErrorType.DateErrorType || (ErrorType.DateErrorType = {}));
    var UserErrorType;
    (function (UserErrorType) {
        UserErrorType["AlreadyMuted"] = "UserErrorTypeAlreadyMuted";
        UserErrorType["NotOnMute"] = "UserErrorTypeNotOnMute";
        UserErrorType["AlreadyEnabled"] = "UserErrorTypeAlreadyEnabled";
        UserErrorType["AlreadyDisabled"] = "UserErrorTypeAlreadyDisabled";
    })(UserErrorType = ErrorType.UserErrorType || (ErrorType.UserErrorType = {}));
    var ConversationErrorType;
    (function (ConversationErrorType) {
        ConversationErrorType["NotFound"] = "ConversationErrorTypeNotFound";
        ConversationErrorType["CannotConverseWithUser"] = "ConversationErrorTypeCannotConverseWithUser";
        ConversationErrorType["NotAnOwner"] = "ConversationErrorTypeNotAnOwner";
        ConversationErrorType["AlreadyAnOwner"] = "ConversationErrorTypeAlreadyAnOwner";
        ConversationErrorType["NotFoundForApp"] = "ConversationErrorTypeNotFoundForApp";
        ConversationErrorType["InvalidType"] = "ConversationErrorTypeInvalidType";
        ConversationErrorType["InvalidConversationUserType"] = "ConversationErrorTypeInvalidConversationUserType";
        ConversationErrorType["MustBeOwner"] = "ConversationErrorTypeMustBeOwner";
        ConversationErrorType["NotPartOfApp"] = "ConversationErrorTypeNotPartOfApp";
        ConversationErrorType["NotFoundForUser"] = "ConversationErrorTypeNotFoundForUser";
        ConversationErrorType["AlreadyExists"] = "ConversationErrorTypeAlreadyExists";
        ConversationErrorType["CannotChatWithSelf"] = "ConversationErrorTypeCannotChatWithSelf";
        ConversationErrorType["AlreadyMuted"] = "ConversationErrorTypeAlreadyMuted";
        ConversationErrorType["NotOnMute"] = "ConversationErrorTypeNotOnMute";
        ConversationErrorType["AlreadyPinned"] = "ConversationErrorTypeAlreadyPinned";
        ConversationErrorType["NotPinned"] = "ConversationErrorTypeNotPinned";
        ConversationErrorType["UsersNotInApp"] = "ConversationErrorTypeUsersNotInApp";
        ConversationErrorType["NoValidUsersToAdd"] = "ConversationErrorTypeNoValidUsersToAdd";
        ConversationErrorType["NoValidUsersToRemove"] = "ConversationErrorTypeNoValidUsersToRemove";
        ConversationErrorType["CannotChangeAccessToSelf"] = "ConversationErrorTypeCannotChangeAccessToSelf";
        ConversationErrorType["CannotAddExistingUsers"] = "ConversationErrorTypeCannotAddExistingUsers";
        ConversationErrorType["AlreadyRemovedUsers"] = "ConversationErrorTypeAlreadyRemovedUsers";
        ConversationErrorType["UsersNotInConversation"] = "ConversationErrorTypeUsersNotInConversation";
        ConversationErrorType["CannotRemoveOwners"] = "ConversationErrorTypeCannotRemoveOwners";
    })(ConversationErrorType = ErrorType.ConversationErrorType || (ErrorType.ConversationErrorType = {}));
})(ErrorType = exports.ErrorType || (exports.ErrorType = {}));
var AppError = /** @class */ (function () {
    function AppError(err, type) {
        var _this = this;
        this.type = ErrorType.InternalErrorType.Unknown;
        this.setErrorActions = function (params) {
            var authFailed = params.authFailed, exchange = params.exchange;
            _this.exchange = exchange;
            _this.authFailed = authFailed;
        };
        this.setStatusCode = function (statusCode) {
            _this.statusCode = statusCode;
        };
        this.setMessage = function (msg) {
            _this.message = msg;
        };
        if (type) {
            this.type = type;
        }
        if (err instanceof Error) {
            this.error = err;
            this.message = err.message;
        }
        else {
            this.message = err;
        }
    }
    return AppError;
}());
exports.AppError = AppError;
var DefaultGroupProfileImageBase = /** @class */ (function () {
    function DefaultGroupProfileImageBase() {
    }
    return DefaultGroupProfileImageBase;
}());
exports.DefaultGroupProfileImageBase = DefaultGroupProfileImageBase;
var AuthBase = /** @class */ (function () {
    function AuthBase() {
    }
    return AuthBase;
}());
exports.AuthBase = AuthBase;
var UserBase = /** @class */ (function () {
    function UserBase() {
    }
    return UserBase;
}());
var UserDetail = /** @class */ (function (_super) {
    __extends(UserDetail, _super);
    function UserDetail() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return UserDetail;
}(UserBase));
var FileType;
(function (FileType) {
    FileType["Video"] = "video";
    FileType["Image"] = "image";
    FileType["Audio"] = "audio";
    FileType["Other"] = "other";
})(FileType = exports.FileType || (exports.FileType = {}));
var ImageSizes;
(function (ImageSizes) {
    ImageSizes["Tiny"] = "tiny";
    ImageSizes["Small"] = "small";
    ImageSizes["Medium"] = "medium";
    ImageSizes["Large"] = "large";
    ImageSizes["XLarge"] = "xlarge";
})(ImageSizes = exports.ImageSizes || (exports.ImageSizes = {}));
var SubImageMinimal = /** @class */ (function () {
    function SubImageMinimal() {
    }
    return SubImageMinimal;
}());
exports.SubImageMinimal = SubImageMinimal;
var ImageInfoMedia = /** @class */ (function () {
    function ImageInfoMedia() {
    }
    return ImageInfoMedia;
}());
exports.ImageInfoMedia = ImageInfoMedia;
var Paginatable = /** @class */ (function () {
    function Paginatable() {
    }
    return Paginatable;
}());
var ConversationConversableUsersBatchMeta = /** @class */ (function (_super) {
    __extends(ConversationConversableUsersBatchMeta, _super);
    function ConversationConversableUsersBatchMeta() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return ConversationConversableUsersBatchMeta;
}(Paginatable));
var ConversationUsersBatchMeta = /** @class */ (function (_super) {
    __extends(ConversationUsersBatchMeta, _super);
    function ConversationUsersBatchMeta() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return ConversationUsersBatchMeta;
}(Paginatable));
var ConversationUserQueryType;
(function (ConversationUserQueryType) {
    ConversationUserQueryType["All"] = "all";
    ConversationUserQueryType["Current"] = "current";
    ConversationUserQueryType["Removed"] = "removed";
})(ConversationUserQueryType || (ConversationUserQueryType = {}));
var ConversationUsersBatchBase = /** @class */ (function (_super) {
    __extends(ConversationUsersBatchBase, _super);
    function ConversationUsersBatchBase() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return ConversationUsersBatchBase;
}(ConversationUsersBatchMeta));
exports.ConversationUsersBatchBase = ConversationUsersBatchBase;
var MediaBase = /** @class */ (function () {
    function MediaBase() {
    }
    return MediaBase;
}());
exports.MediaBase = MediaBase;
var ImageBase = /** @class */ (function (_super) {
    __extends(ImageBase, _super);
    function ImageBase() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.fileType = FileType.Image;
        _this.videoInfo = null;
        return _this;
    }
    return ImageBase;
}(MediaBase));
exports.ImageBase = ImageBase;
var VideoBase = /** @class */ (function (_super) {
    __extends(VideoBase, _super);
    function VideoBase() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.fileType = FileType.Video;
        _this.imageInfo = null;
        return _this;
    }
    return VideoBase;
}(MediaBase));
exports.VideoBase = VideoBase;
var AudioBase = /** @class */ (function (_super) {
    __extends(AudioBase, _super);
    function AudioBase() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.fileType = FileType.Audio;
        _this.imageInfo = null;
        return _this;
    }
    return AudioBase;
}(MediaBase));
exports.AudioBase = AudioBase;
var DocumentBase = /** @class */ (function (_super) {
    __extends(DocumentBase, _super);
    function DocumentBase() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.fileType = FileType.Other;
        _this.imageInfo = null;
        _this.videoInfo = null;
        return _this;
    }
    return DocumentBase;
}(MediaBase));
exports.DocumentBase = DocumentBase;
var SystemMessageType;
(function (SystemMessageType) {
    SystemMessageType["MemberRemoved"] = "MemberRemoved";
    SystemMessageType["MemberAdded"] = "MemberAdded";
    SystemMessageType["OwnerRemoved"] = "OwnerRemoved";
    SystemMessageType["OwnerAdded"] = "OwnerAdded";
    SystemMessageType["GroupPictureChanged"] = "GroupPictureChanged";
    SystemMessageType["GroupNameChanged"] = "GroupNameChanged";
    SystemMessageType["GroupDisabled"] = "GroupDisabled";
    SystemMessageType["GroupCreated"] = "GroupCreated";
    SystemMessageType["IndividualChatCreated"] = "IndividualChatCreated";
})(SystemMessageType = exports.SystemMessageType || (exports.SystemMessageType = {}));
var UserSimpleInfo = /** @class */ (function () {
    function UserSimpleInfo() {
    }
    return UserSimpleInfo;
}());
exports.UserSimpleInfo = UserSimpleInfo;
var SystemMessageBase = /** @class */ (function () {
    function SystemMessageBase() {
    }
    return SystemMessageBase;
}());
exports.SystemMessageBase = SystemMessageBase;
var MessageType;
(function (MessageType) {
    MessageType["System"] = "system";
    MessageType["Text"] = "text";
    MessageType["Image"] = "image";
    MessageType["Video"] = "video";
    MessageType["Document"] = "document";
    MessageType["Gif"] = "gif";
    MessageType["Emoji"] = "emoji";
    MessageType["Audio"] = "audio";
})(MessageType = exports.MessageType || (exports.MessageType = {}));
var ReactionType;
(function (ReactionType) {
    ReactionType["Thumb"] = "Thumb";
    ReactionType["Heart"] = "Heart";
    ReactionType["Fist"] = "Fist";
})(ReactionType = exports.ReactionType || (exports.ReactionType = {}));
var ReactionsObject = /** @class */ (function () {
    function ReactionsObject() {
        this[_a] = 0;
        this[_b] = 0;
        this[_c] = 0;
    }
    return ReactionsObject;
}());
exports.ReactionsObject = ReactionsObject;
_a = ReactionType.Fist, _b = ReactionType.Heart, _c = ReactionType.Thumb;
var MessageBase = /** @class */ (function () {
    function MessageBase() {
    }
    return MessageBase;
}());
exports.MessageBase = MessageBase;
var ConversationType;
(function (ConversationType) {
    ConversationType["Group"] = "group";
    ConversationType["Individual"] = "individual";
})(ConversationType = exports.ConversationType || (exports.ConversationType = {}));
var ConversationBase = /** @class */ (function () {
    function ConversationBase() {
    }
    return ConversationBase;
}());
exports.ConversationBase = ConversationBase;
var DTO;
(function (DTO) {
    var DefaultGroupProfileImage = /** @class */ (function (_super) {
        __extends(DefaultGroupProfileImage, _super);
        function DefaultGroupProfileImage() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return DefaultGroupProfileImage;
    }(DefaultGroupProfileImageBase));
    DTO.DefaultGroupProfileImage = DefaultGroupProfileImage;
    var Auth = /** @class */ (function (_super) {
        __extends(Auth, _super);
        function Auth() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return Auth;
    }(AuthBase));
    DTO.Auth = Auth;
    var User = /** @class */ (function (_super) {
        __extends(User, _super);
        function User() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return User;
    }(UserDetail));
    DTO.User = User;
    var UserSelf = /** @class */ (function (_super) {
        __extends(UserSelf, _super);
        function UserSelf() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return UserSelf;
    }(User));
    DTO.UserSelf = UserSelf;
    var ConversationConversableUsersBatch = /** @class */ (function (_super) {
        __extends(ConversationConversableUsersBatch, _super);
        function ConversationConversableUsersBatch() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return ConversationConversableUsersBatch;
    }(ConversationConversableUsersBatchMeta));
    DTO.ConversationConversableUsersBatch = ConversationConversableUsersBatch;
    var ConversationUser = /** @class */ (function (_super) {
        __extends(ConversationUser, _super);
        function ConversationUser() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return ConversationUser;
    }(User));
    DTO.ConversationUser = ConversationUser;
    var ConversationUsersBatch = /** @class */ (function (_super) {
        __extends(ConversationUsersBatch, _super);
        function ConversationUsersBatch() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return ConversationUsersBatch;
    }(ConversationUsersBatchBase));
    DTO.ConversationUsersBatch = ConversationUsersBatch;
    var SystemMessage = /** @class */ (function (_super) {
        __extends(SystemMessage, _super);
        function SystemMessage() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return SystemMessage;
    }(SystemMessageBase));
    DTO.SystemMessage = SystemMessage;
    var Message = /** @class */ (function (_super) {
        __extends(Message, _super);
        function Message() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return Message;
    }(MessageBase));
    DTO.Message = Message;
    var ConversationInfo = /** @class */ (function () {
        function ConversationInfo() {
        }
        return ConversationInfo;
    }());
    DTO.ConversationInfo = ConversationInfo;
    var GroupConversationInfo = /** @class */ (function (_super) {
        __extends(GroupConversationInfo, _super);
        function GroupConversationInfo() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return GroupConversationInfo;
    }(ConversationInfo));
    DTO.GroupConversationInfo = GroupConversationInfo;
    var UserConversationInfo = /** @class */ (function (_super) {
        __extends(UserConversationInfo, _super);
        function UserConversationInfo() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return UserConversationInfo;
    }(ConversationInfo));
    DTO.UserConversationInfo = UserConversationInfo;
    var Conversation = /** @class */ (function (_super) {
        __extends(Conversation, _super);
        function Conversation() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return Conversation;
    }(ConversationBase));
    DTO.Conversation = Conversation;
    var GroupConversation = /** @class */ (function (_super) {
        __extends(GroupConversation, _super);
        function GroupConversation() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return GroupConversation;
    }(Conversation));
    DTO.GroupConversation = GroupConversation;
    var IndividualConversation = /** @class */ (function (_super) {
        __extends(IndividualConversation, _super);
        function IndividualConversation() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return IndividualConversation;
    }(Conversation));
    DTO.IndividualConversation = IndividualConversation;
    var MessageDeleted = /** @class */ (function () {
        function MessageDeleted() {
        }
        return MessageDeleted;
    }());
    DTO.MessageDeleted = MessageDeleted;
    var ConversationUsersBasic = /** @class */ (function () {
        function ConversationUsersBasic() {
        }
        return ConversationUsersBasic;
    }());
    DTO.ConversationUsersBasic = ConversationUsersBasic;
})(DTO = exports.DTO || (exports.DTO = {}));
var Domain;
(function (Domain) {
    var DefaultGroupProfileImage = /** @class */ (function (_super) {
        __extends(DefaultGroupProfileImage, _super);
        function DefaultGroupProfileImage() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return DefaultGroupProfileImage;
    }(DefaultGroupProfileImageBase));
    Domain.DefaultGroupProfileImage = DefaultGroupProfileImage;
    var Auth = /** @class */ (function (_super) {
        __extends(Auth, _super);
        function Auth() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return Auth;
    }(AuthBase));
    Domain.Auth = Auth;
    var User = /** @class */ (function (_super) {
        __extends(User, _super);
        function User() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return User;
    }(UserDetail));
    Domain.User = User;
    var UserSelf = /** @class */ (function (_super) {
        __extends(UserSelf, _super);
        function UserSelf() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return UserSelf;
    }(User));
    Domain.UserSelf = UserSelf;
    var ConversationConversableUsersBatch = /** @class */ (function (_super) {
        __extends(ConversationConversableUsersBatch, _super);
        function ConversationConversableUsersBatch() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return ConversationConversableUsersBatch;
    }(ConversationConversableUsersBatchMeta));
    Domain.ConversationConversableUsersBatch = ConversationConversableUsersBatch;
    var ConversationUser = /** @class */ (function (_super) {
        __extends(ConversationUser, _super);
        function ConversationUser() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return ConversationUser;
    }(User));
    Domain.ConversationUser = ConversationUser;
    var ConversationUsersBatch = /** @class */ (function (_super) {
        __extends(ConversationUsersBatch, _super);
        function ConversationUsersBatch() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return ConversationUsersBatch;
    }(ConversationUsersBatchBase));
    Domain.ConversationUsersBatch = ConversationUsersBatch;
    var SystemMessage = /** @class */ (function (_super) {
        __extends(SystemMessage, _super);
        function SystemMessage() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return SystemMessage;
    }(SystemMessageBase));
    Domain.SystemMessage = SystemMessage;
    var Message = /** @class */ (function (_super) {
        __extends(Message, _super);
        function Message() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return Message;
    }(MessageBase));
    Domain.Message = Message;
    var ConversationInfo = /** @class */ (function () {
        function ConversationInfo() {
        }
        return ConversationInfo;
    }());
    Domain.ConversationInfo = ConversationInfo;
    var GroupConversationInfo = /** @class */ (function (_super) {
        __extends(GroupConversationInfo, _super);
        function GroupConversationInfo() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return GroupConversationInfo;
    }(ConversationInfo));
    Domain.GroupConversationInfo = GroupConversationInfo;
    var UserConversationInfo = /** @class */ (function (_super) {
        __extends(UserConversationInfo, _super);
        function UserConversationInfo() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return UserConversationInfo;
    }(ConversationInfo));
    Domain.UserConversationInfo = UserConversationInfo;
    var Conversation = /** @class */ (function (_super) {
        __extends(Conversation, _super);
        function Conversation() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return Conversation;
    }(ConversationBase));
    Domain.Conversation = Conversation;
    var GroupConversation = /** @class */ (function (_super) {
        __extends(GroupConversation, _super);
        function GroupConversation() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return GroupConversation;
    }(Conversation));
    Domain.GroupConversation = GroupConversation;
    var IndividualConversation = /** @class */ (function (_super) {
        __extends(IndividualConversation, _super);
        function IndividualConversation() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return IndividualConversation;
    }(Conversation));
    Domain.IndividualConversation = IndividualConversation;
    var MessageDeleted = /** @class */ (function () {
        function MessageDeleted() {
        }
        return MessageDeleted;
    }());
    Domain.MessageDeleted = MessageDeleted;
    var ConversationUsersBasic = /** @class */ (function () {
        function ConversationUsersBasic() {
        }
        return ConversationUsersBasic;
    }());
    Domain.ConversationUsersBasic = ConversationUsersBasic;
})(Domain = exports.Domain || (exports.Domain = {}));
//# sourceMappingURL=types.js.map