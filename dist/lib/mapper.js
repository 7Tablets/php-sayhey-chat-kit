"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConversationMapper = exports.MessageMapper = exports.MediaMapper = exports.UserMapper = exports.AppMapper = void 0;
var types_1 = require("./types");
var AppMapper = /** @class */ (function () {
    function AppMapper() {
    }
    AppMapper.toProfileImageDomainList = function (imageList) {
        return __spreadArrays(imageList);
    };
    AppMapper.toAuthDomain = function (authDetails) {
        return __assign({}, authDetails);
    };
    return AppMapper;
}());
exports.AppMapper = AppMapper;
var UserMapper = /** @class */ (function () {
    function UserMapper() {
    }
    UserMapper.toUserDomain = function (user) {
        return __assign(__assign({}, user), { deletedAt: user.deletedAt ? new Date(user.deletedAt) : null, picture: user.picture ? MediaMapper.toMediaDomain(user.picture) : null });
    };
    UserMapper.toConversationUserDomain = function (user) {
        return __assign(__assign(__assign({}, user), UserMapper.toUserDomain(user)), { deletedFromConversationAt: user.deletedFromConversationAt
                ? new Date(user.deletedFromConversationAt)
                : null });
    };
    UserMapper.toUserSelfDomain = function (user) {
        return __assign(__assign({}, UserMapper.toUserDomain(user)), { muteAllUntil: user.muteAllUntil ? new Date(user.muteAllUntil) : null });
    };
    UserMapper.toConversationUserBasicDomain = function (user) {
        var deletedFromConversationAt = user.deletedFromConversationAt, userDeletedAt = user.userDeletedAt;
        return __assign(__assign({}, user), { deletedFromConversationAt: deletedFromConversationAt
                ? new Date(deletedFromConversationAt)
                : null, userDeletedAt: userDeletedAt ? new Date(userDeletedAt) : null });
    };
    return UserMapper;
}());
exports.UserMapper = UserMapper;
var MediaMapper = /** @class */ (function () {
    function MediaMapper() {
    }
    MediaMapper.toMediaDomain = function (media) {
        return __assign({}, media);
    };
    return MediaMapper;
}());
exports.MediaMapper = MediaMapper;
var MessageMapper = /** @class */ (function () {
    function MessageMapper() {
    }
    MessageMapper.toMessageDomain = function (message) {
        var createdAt = message.createdAt, sender = message.sender, file = message.file, deletedAt = message.deletedAt;
        return __assign(__assign({}, message), { createdAt: new Date(createdAt), sender: sender ? UserMapper.toUserDomain(sender) : null, file: file ? MediaMapper.toMediaDomain(file) : null, deletedAt: deletedAt ? new Date(deletedAt) : null });
    };
    MessageMapper.toMessageDeletedDomain = function (messageEvent) {
        return __assign(__assign({}, messageEvent), { deletedAt: new Date(messageEvent.deletedAt) });
    };
    return MessageMapper;
}());
exports.MessageMapper = MessageMapper;
var ConversationMapper = /** @class */ (function () {
    function ConversationMapper() {
    }
    ConversationMapper.toConversationConversableUsersBatchDomain = function (batch) {
        return __assign(__assign({}, batch), { results: batch.results.map(UserMapper.toUserDomain) });
    };
    ConversationMapper.toConversationUsersBatchDomain = function (batch) {
        return __assign(__assign({}, batch), { results: batch.results.map(UserMapper.toConversationUserDomain) });
    };
    ConversationMapper.toGroupConversationInfoDomain = function (info) {
        return __assign({}, info);
    };
    ConversationMapper.toIndividualConversationInfoDomain = function (info) {
        return __assign({}, info);
    };
    ConversationMapper.toGenericConversationDomain = function (conversation) {
        var muteUntil = conversation.muteUntil, endBefore = conversation.endBefore, deletedFromConversationAt = conversation.deletedFromConversationAt, createdAt = conversation.createdAt, updatedAt = conversation.updatedAt, pinnedAt = conversation.pinnedAt, startAfter = conversation.startAfter, lastMessage = conversation.lastMessage;
        var common = {
            muteUntil: muteUntil ? new Date(muteUntil) : null,
            endBefore: endBefore ? new Date(endBefore) : null,
            deletedFromConversationAt: deletedFromConversationAt
                ? new Date(deletedFromConversationAt)
                : null,
            createdAt: new Date(createdAt),
            updatedAt: new Date(updatedAt),
            pinnedAt: pinnedAt ? new Date(pinnedAt) : null,
            startAfter: startAfter ? new Date(startAfter) : null,
            lastMessage: lastMessage ? MessageMapper.toMessageDomain(lastMessage) : null
        };
        if (conversation instanceof types_1.DTO.IndividualConversation) {
            return __assign(__assign(__assign({}, conversation), common), { user: ConversationMapper.toIndividualConversationInfoDomain(conversation.user) });
        }
        return __assign(__assign(__assign({}, conversation), common), { group: ConversationMapper.toGroupConversationInfoDomain(conversation.group) });
    };
    return ConversationMapper;
}());
exports.ConversationMapper = ConversationMapper;
//# sourceMappingURL=mapper.js.map