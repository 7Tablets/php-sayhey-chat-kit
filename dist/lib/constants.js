"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Routes = exports.API_VERSION = exports.API_KEY_NAME = void 0;
var types_1 = require("./types");
exports.API_KEY_NAME = 'x-sayhey-server-api-key';
exports.API_VERSION = 'v1';
exports.Routes = {
    GetDefaultProfileImagesForGroup: {
        Endpoint: '/default-group-images',
        Method: types_1.HttpMethod.Get
    },
    ViewDefaultProfileImageForGroup: {
        Endpoint: function (filename, queryString) {
            return "/default-group-images/" + filename + queryString;
        },
        Method: types_1.HttpMethod.Get
    },
    SignUpUser: {
        Endpoint: '/users/server-sign-up',
        Method: types_1.HttpMethod.Post
    },
    SignInUserWithEmail: {
        Endpoint: '/users/server-sign-in',
        Method: types_1.HttpMethod.Post
    },
    SignInUserWithId: {
        Endpoint: '/users/server-sign-in-id',
        Method: types_1.HttpMethod.Post
    },
    GetUserProfile: {
        Endpoint: function (userId) { return "users/" + userId + "/profile-info"; },
        Method: types_1.HttpMethod.Get
    },
    UpdateUserProfileInfo: {
        Endpoint: function (userId) { return "users/" + userId + "/profile-info"; },
        Method: types_1.HttpMethod.Put
    },
    UpdateUserProfileImage: {
        Endpoint: function (userId) { return "users/" + userId + "/profile-image"; },
        Method: types_1.HttpMethod.Put
    },
    RemoveUserProfileImage: {
        Endpoint: function (userId) { return "users/" + userId + "/profile-image"; },
        Method: types_1.HttpMethod.Delete
    },
    UploadFile: {
        Endpoint: '/files',
        Method: types_1.HttpMethod.Post
    },
    GetConversationConversableUsers: {
        Endpoint: function (conversationId) { return "conversations/" + conversationId + "/conversable-users"; },
        Method: types_1.HttpMethod.Get
    },
    GetConversationUsers: {
        Endpoint: function (conversationId) { return "conversations/" + conversationId + "/users"; },
        Method: types_1.HttpMethod.Get
    },
    AddUsersToConversation: {
        Endpoint: function (conversationId) { return "/conversations/" + conversationId + "/users"; },
        Method: types_1.HttpMethod.Post
    },
    RemoveUsersFromConversation: {
        Endpoint: function (conversationId) { return "/conversations/" + conversationId + "/delete-users"; },
        Method: types_1.HttpMethod.Post
    },
    MakeConversationOwner: {
        Endpoint: function (conversationId, userId) {
            return "conversations/" + conversationId + "/owners/" + userId;
        },
        Method: types_1.HttpMethod.Put
    },
    RemoveConversationOwner: {
        Endpoint: function (conversationId, userId) {
            return "conversations/" + conversationId + "/owners/" + userId;
        },
        Method: types_1.HttpMethod.Delete
    },
    DeleteAllUsersInConversation: {
        Endpoint: function (conversationId) { return "/conversations/" + conversationId + "/delete-all-users"; },
        Method: types_1.HttpMethod.Post
    },
    CreateGroupWithOwner: {
        Endpoint: '/conversations/group-with-owner',
        Method: types_1.HttpMethod.Post
    },
    UpdateGroupInfo: {
        Endpoint: function (conversationId) { return "conversations/" + conversationId + "/group-info"; },
        Method: types_1.HttpMethod.Put
    },
    UpdateGroupImage: {
        Endpoint: function (conversationId) { return "/conversations/" + conversationId + "/group-image"; },
        Method: types_1.HttpMethod.Put
    },
    RemoveGroupImage: {
        Endpoint: function (conversationId) { return "/conversations/" + conversationId + "/group-image"; },
        Method: types_1.HttpMethod.Delete
    },
    GetConversationBasicUsers: {
        Endpoint: function (conversationId) { return "/conversations/" + conversationId + "/basic-users"; },
        Method: types_1.HttpMethod.Get
    },
    DisableUser: {
        Endpoint: function (userId) { return "/users/" + userId + "/disabled"; },
        Method: types_1.HttpMethod.Put
    },
    EnableUser: {
        Endpoint: function (userId) { return "/users/" + userId + "/disabled"; },
        Method: types_1.HttpMethod.Delete
    },
    GetMessages: {
        Endpoint: '/messages',
        Method: types_1.HttpMethod.Get
    },
    DeleteMessage: {
        Endpoint: function (messageId) { return "/messages/" + messageId; },
        Method: types_1.HttpMethod.Delete
    },
    DeleteUser: {
        Endpoint: function (userId) { return "/users/" + userId; },
        Method: types_1.HttpMethod.Delete
    },
    GetConversationGroupImage: {
        Endpoint: function (conversationId) { return "/conversations/" + conversationId + "/group-image"; },
        Method: types_1.HttpMethod.Get
    },
    GetFileMessage: {
        Endpoint: function (messageId, fileLocalIdName) {
            return "/messages/" + messageId + "/files/" + fileLocalIdName;
        },
        Method: types_1.HttpMethod.Get
    },
    GetVideoThumbnail: {
        Endpoint: function (messageId, fileLocalIdName) {
            return "/messages/" + messageId + "/files/video-thumbnails/" + fileLocalIdName;
        },
        Method: types_1.HttpMethod.Get
    }
};
//# sourceMappingURL=constants.js.map