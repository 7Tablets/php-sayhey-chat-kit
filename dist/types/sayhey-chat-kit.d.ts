/// <reference types="node" />
import { AppError, Domain, HttpMethod, ConversationType } from './types';
import { Result } from 'neverthrow';
import { ReadStream } from 'fs';
export default class ChatKitServer {
    static apiKey: string;
    static appId: string;
    static apiVersion: string;
    private static _initialized;
    private static httpClient;
    private static guard;
    private constructor();
    static initialize(options: {
        sayheyAppId: string;
        sayheyApiKey: string;
        apiVersion?: string;
        sayheyUrl: string;
    }): void;
    static getFile(url: string, method: HttpMethod): Promise<Blob>;
    static streamFile(url: string, method: HttpMethod): Promise<ReadStream>;
    static uploadFile(file: Blob, filename: string): Promise<string>;
    static uploadFileWithStream(readableStream: ReadStream): Promise<string>;
    /**
     * App APIs
     */
    static getDefaultProfileImagesForGroup(): Promise<Result<Domain.DefaultGroupProfileImage[], AppError>>;
    static viewDefaultProfileImageForGroup<I extends boolean = false, R extends Blob | ReadStream = I extends true ? ReadStream : Blob>(options: {
        filename: string;
        original?: boolean;
        stream?: I;
    }): Promise<Result<R, AppError>>;
    /**
     * User APIs
     */
    static signUpUser(params: {
        email: string;
        password?: string;
        firstName: string;
        lastName: string;
        profileImageId?: string | null;
        refId?: string;
    }): Promise<Result<Domain.Auth, AppError>>;
    static signInUserWithEmail(email: string): Promise<Result<Domain.Auth, AppError>>;
    static signInUserWithId(id: string): Promise<Result<Domain.Auth, AppError>>;
    static getUserProfile(userId: string): Promise<Result<Domain.UserSelf, AppError>>;
    static updateUserProfileInfo(params: {
        userId: string;
        firstName?: string;
        lastName?: string;
        email?: string;
        refId?: string | null;
    }): Promise<Result<boolean, AppError>>;
    static updateUserProfileImage(params: {
        userId: string;
        profileImageId: string;
    }): Promise<Result<boolean, AppError>>;
    static removeUserProfileImage(userId: string): Promise<Result<boolean, AppError>>;
    static disableUser(userId: string): Promise<Result<boolean, AppError>>;
    static enableUser(userId: string): Promise<Result<boolean, AppError>>;
    static deleteUser(userId: string): Promise<Result<boolean, AppError>>;
    /**
     * Conversation APIs
     */
    static getConversationConversableUsers(params: {
        conversationId: string;
        offset?: number;
        limit?: number;
        search?: string;
        includeRefIdInSearch?: boolean;
    }): Promise<Result<Domain.ConversationConversableUsersBatch, AppError>>;
    static getConversationUsers(params: {
        conversationId: string;
        offset?: number;
        limit?: number;
        search?: string;
        type?: 'all' | 'current' | 'default';
        includeRefIdInSearch?: boolean;
    }): Promise<Result<Domain.ConversationUsersBatch, AppError>>;
    static getConversationBasicUsers(params: {
        conversationId: string;
        type?: 'all' | 'current' | 'default';
    }): Promise<Result<Domain.ConversationUsersBasic[], AppError>>;
    static addUsersToConversation(params: {
        conversationId: string;
        memberIds: string[];
        force?: boolean;
    }): Promise<Result<boolean, AppError>>;
    static removeUsersFromConversation(params: {
        conversationId: string;
        memberIds: string;
        force?: boolean;
    }): Promise<Result<boolean, AppError>>;
    static makeConversationOwner(params: {
        conversationId: string;
        userId: string;
    }): Promise<Result<boolean, AppError>>;
    static removeConversationOwner(params: {
        conversationId: string;
        userId: string;
    }): Promise<Result<boolean, AppError>>;
    static deleteAllUsersInConversation(params: {
        conversationId: string;
    }): Promise<Result<boolean, AppError>>;
    static createGroupWithOwner(params: {
        groupName: string;
        groupImageId: string | null;
        memberIds: string[];
        ownerId: string;
        serverManaged?: boolean;
    }): Promise<Result<Domain.GroupConversation, AppError>>;
    static updateGroupInfo(params: {
        conversationId: string;
        groupName: string;
    }): Promise<Result<boolean, AppError>>;
    static updateGroupImage(params: {
        conversationId: string;
        groupImageId: string;
    }): Promise<Result<boolean, AppError>>;
    static removeGroupImage(conversationId: string): Promise<Result<boolean, AppError>>;
    /**
     * Message APIs
     */
    static getMessages(params: {
        beforeSequence?: string;
        limit?: number;
        galleryOnly?: boolean;
        includeDeleted?: boolean;
        userId?: string;
        fromDate?: Date;
        toDate?: Date;
        conversationType?: ConversationType;
        search?: string;
        conversationId?: string;
    }): Promise<Result<Domain.Message[], AppError>>;
    static deleteMessage(messageId: string): Promise<Result<Domain.MessageDeleted, AppError>>;
    /**
     * Media APIs
     */
    static getGroupProfileImage<I extends boolean = false, R extends string | ReadStream = I extends true ? ReadStream : string>(options: {
        conversationId: string;
        stream?: I;
    }): Promise<Result<R, AppError>>;
    static getFileMessage<I extends boolean = false, R extends string | ReadStream = I extends true ? ReadStream : string>(options: {
        messageId: string;
        fileLocalIdName: string;
        stream?: I;
    }): Promise<Result<R, AppError>>;
    static getVideoThumbnail<I extends boolean = false, R extends string | ReadStream = I extends true ? ReadStream : string>(options: {
        messageId: string;
        fileLocalIdName: string;
        stream?: I;
    }): Promise<Result<R, AppError>>;
}
