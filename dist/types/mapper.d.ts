import { DTO, Domain } from './types';
export declare class AppMapper {
    static toProfileImageDomainList: (imageList: DTO.DefaultGroupProfileImage[]) => Domain.DefaultGroupProfileImage[];
    static toAuthDomain: (authDetails: DTO.Auth) => Domain.Auth;
}
export declare class UserMapper {
    static toUserDomain: (user: DTO.User) => Domain.User;
    static toConversationUserDomain: (user: DTO.ConversationUser) => Domain.ConversationUser;
    static toUserSelfDomain: (user: DTO.UserSelf) => Domain.UserSelf;
    static toConversationUserBasicDomain: (user: DTO.ConversationUsersBasic) => Domain.ConversationUsersBasic;
}
export declare class MediaMapper {
    static toMediaDomain: (media: DTO.Media) => Domain.Media;
}
export declare class MessageMapper {
    static toMessageDomain: (message: DTO.Message) => Domain.Message;
    static toMessageDeletedDomain: (messageEvent: DTO.MessageDeleted) => Domain.MessageDeleted;
}
export declare class ConversationMapper {
    static toConversationConversableUsersBatchDomain: (batch: DTO.ConversationConversableUsersBatch) => Domain.ConversationConversableUsersBatch;
    static toConversationUsersBatchDomain: (batch: DTO.ConversationUsersBatch) => Domain.ConversationUsersBatch;
    static toGroupConversationInfoDomain: (info: DTO.GroupConversationInfo) => Domain.GroupConversationInfo;
    static toIndividualConversationInfoDomain: (info: DTO.UserConversationInfo) => Domain.UserConversationInfo;
    static toGenericConversationDomain: (conversation: DTO.GenericConversation) => Domain.GenericConversation;
}
