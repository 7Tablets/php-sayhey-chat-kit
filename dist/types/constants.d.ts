import { HttpMethod } from './types';
export declare const API_KEY_NAME = "x-sayhey-server-api-key";
export declare const API_VERSION = "v1";
export declare const Routes: {
    GetDefaultProfileImagesForGroup: {
        Endpoint: string;
        Method: HttpMethod;
    };
    ViewDefaultProfileImageForGroup: {
        Endpoint: (filename: string, queryString: string) => string;
        Method: HttpMethod;
    };
    SignUpUser: {
        Endpoint: string;
        Method: HttpMethod;
    };
    SignInUserWithEmail: {
        Endpoint: string;
        Method: HttpMethod;
    };
    SignInUserWithId: {
        Endpoint: string;
        Method: HttpMethod;
    };
    GetUserProfile: {
        Endpoint: (userId: string) => string;
        Method: HttpMethod;
    };
    UpdateUserProfileInfo: {
        Endpoint: (userId: string) => string;
        Method: HttpMethod;
    };
    UpdateUserProfileImage: {
        Endpoint: (userId: string) => string;
        Method: HttpMethod;
    };
    RemoveUserProfileImage: {
        Endpoint: (userId: string) => string;
        Method: HttpMethod;
    };
    UploadFile: {
        Endpoint: string;
        Method: HttpMethod;
    };
    GetConversationConversableUsers: {
        Endpoint: (conversationId: string) => string;
        Method: HttpMethod;
    };
    GetConversationUsers: {
        Endpoint: (conversationId: string) => string;
        Method: HttpMethod;
    };
    AddUsersToConversation: {
        Endpoint: (conversationId: string) => string;
        Method: HttpMethod;
    };
    RemoveUsersFromConversation: {
        Endpoint: (conversationId: string) => string;
        Method: HttpMethod;
    };
    MakeConversationOwner: {
        Endpoint: (conversationId: string, userId: string) => string;
        Method: HttpMethod;
    };
    RemoveConversationOwner: {
        Endpoint: (conversationId: string, userId: string) => string;
        Method: HttpMethod;
    };
    DeleteAllUsersInConversation: {
        Endpoint: (conversationId: string) => string;
        Method: HttpMethod;
    };
    CreateGroupWithOwner: {
        Endpoint: string;
        Method: HttpMethod;
    };
    UpdateGroupInfo: {
        Endpoint: (conversationId: string) => string;
        Method: HttpMethod;
    };
    UpdateGroupImage: {
        Endpoint: (conversationId: string) => string;
        Method: HttpMethod;
    };
    RemoveGroupImage: {
        Endpoint: (conversationId: string) => string;
        Method: HttpMethod;
    };
    GetConversationBasicUsers: {
        Endpoint: (conversationId: string) => string;
        Method: HttpMethod;
    };
    DisableUser: {
        Endpoint: (userId: string) => string;
        Method: HttpMethod;
    };
    EnableUser: {
        Endpoint: (userId: string) => string;
        Method: HttpMethod;
    };
    GetMessages: {
        Endpoint: string;
        Method: HttpMethod;
    };
    DeleteMessage: {
        Endpoint: (messageId: string) => string;
        Method: HttpMethod;
    };
    DeleteUser: {
        Endpoint: (userId: string) => string;
        Method: HttpMethod;
    };
    GetConversationGroupImage: {
        Endpoint: (conversationId: string) => string;
        Method: HttpMethod;
    };
    GetFileMessage: {
        Endpoint: (messageId: string, fileLocalIdName: string) => string;
        Method: HttpMethod;
    };
    GetVideoThumbnail: {
        Endpoint: (messageId: string, fileLocalIdName: string) => string;
        Method: HttpMethod;
    };
};
