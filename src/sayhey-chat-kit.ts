import { AppError, ErrorType, DTO, Domain, HttpMethod, ConversationType } from './types'
import { Result, ok, err } from 'neverthrow'
import { API_VERSION, Routes } from './constants'
import { AxiosInstance } from 'axios'
import createHttpClient from './axios'
import { AppMapper, UserMapper, ConversationMapper, MessageMapper } from './mapper'
import { ReadStream } from 'fs'
export default class ChatKitServer {
  public static apiKey: string

  public static appId: string

  public static apiVersion: string

  private static _initialized: boolean = false

  private static httpClient: AxiosInstance

  private static async guard<T>(
    cb: (...args: any[]) => Promise<T | AppError>
  ): Promise<Result<T, AppError>> {
    if (!ChatKitServer._initialized) {
      return err(
        new AppError('ChatKit has not been initialized!', ErrorType.InternalErrorType.Uninitialized)
      )
    }
    try {
      const res = await cb()
      if (res instanceof AppError) {
        return err(res)
      }
      return ok(res)
    } catch (e) {
      if (e instanceof AppError) {
        return err(e)
      }
      return err(new AppError('Unknown error', ErrorType.InternalErrorType.Unknown))
    }
  }

  private constructor() {}

  public static initialize(options: {
    sayheyAppId: string
    sayheyApiKey: string
    apiVersion?: string
    sayheyUrl: string
  }) {
    const { sayheyApiKey, sayheyAppId, apiVersion, sayheyUrl } = options
    ChatKitServer.apiKey = sayheyApiKey
    ChatKitServer.appId = sayheyAppId
    ChatKitServer.apiVersion = apiVersion || API_VERSION
    ChatKitServer.httpClient = createHttpClient({
      baseURL: `${sayheyUrl}/${ChatKitServer.apiVersion}/apps/${ChatKitServer.appId}`,
      apiKey: ChatKitServer.apiKey
    })
    ChatKitServer._initialized = true
  }

  public static async getFile(url: string, method: HttpMethod): Promise<Blob> {
    const { data } = await ChatKitServer.httpClient({
      url,
      method,
      responseType: 'blob'
    })
    return data
  }

  public static async streamFile(url: string, method: HttpMethod): Promise<ReadStream> {
    const { data } = await ChatKitServer.httpClient({
      url,
      method,
      responseType: 'stream'
    })
    return data
  }

  public static async uploadFile(file: Blob, filename: string): Promise<string> {
    const {
      UploadFile: { Endpoint, Method }
    } = Routes
    const body = new FormData()
    body.append('file', file, filename)
    const { data } = await ChatKitServer.httpClient({
      url: Endpoint,
      method: Method,
      data: body
    })
    return data.id
  }

  public static async uploadFileWithStream(readableStream: ReadStream): Promise<string> {
    const {
      UploadFile: { Endpoint, Method }
    } = Routes
    const body = new FormData()
    body.append('file', readableStream as any)
    const { data } = await ChatKitServer.httpClient({
      url: Endpoint,
      method: Method,
      data: body
    })
    return data.id
  }

  /**
   * App APIs
   */

  public static async getDefaultProfileImagesForGroup(): Promise<
    Result<Domain.DefaultGroupProfileImage[], AppError>
  > {
    return ChatKitServer.guard(async function getDefaultProfileImagesForGroup() {
      const {
        GetDefaultProfileImagesForGroup: { Endpoint, Method }
      } = Routes
      const { data } = await ChatKitServer.httpClient({
        url: Endpoint,
        method: Method
      })
      return AppMapper.toProfileImageDomainList(data as DTO.DefaultGroupProfileImage[])
    })
  }

  public static async viewDefaultProfileImageForGroup<
    I extends boolean = false,
    R extends Blob | ReadStream = I extends true ? ReadStream : Blob
  >(options: { filename: string; original?: boolean; stream?: I }): Promise<Result<R, AppError>> {
    const { filename, original, stream } = options
    return ChatKitServer.guard(async function viewDefaultProfileImageForGroup() {
      const {
        ViewDefaultProfileImageForGroup: { Endpoint, Method }
      } = Routes
      const url = Endpoint(filename, !original ? '?size=thumbnail' : '')
      if (stream) {
        const data = await ChatKitServer.streamFile(url, Method)
        return data as R
      }
      const data = await ChatKitServer.getFile(url, Method)
      return data as R
    })
  }

  /**
   * User APIs
   */

  public static async signUpUser(params: {
    email: string
    password?: string
    firstName: string
    lastName: string
    profileImageId?: string | null
    refId?: string
  }): Promise<Result<Domain.Auth, AppError>> {
    return ChatKitServer.guard(async function signUpUser() {
      const {
        SignUpUser: { Endpoint, Method }
      } = Routes
      const { data } = await ChatKitServer.httpClient({
        url: Endpoint,
        method: Method,
        data: params
      })
      return AppMapper.toAuthDomain(data as DTO.Auth)
    })
  }

  public static async signInUserWithEmail(email: string): Promise<Result<Domain.Auth, AppError>> {
    return ChatKitServer.guard(async function signInUserWithEmail() {
      const {
        SignInUserWithEmail: { Endpoint, Method }
      } = Routes
      const { data } = await ChatKitServer.httpClient({
        url: Endpoint,
        method: Method,
        data: {
          email
        }
      })
      return AppMapper.toAuthDomain(data as DTO.Auth)
    })
  }

  public static async signInUserWithId(id: string): Promise<Result<Domain.Auth, AppError>> {
    return ChatKitServer.guard(async function signInUserWithId() {
      const {
        SignInUserWithId: { Endpoint, Method }
      } = Routes
      const { data } = await ChatKitServer.httpClient({
        url: Endpoint,
        method: Method,
        data: {
          id
        }
      })
      return AppMapper.toAuthDomain(data as DTO.Auth)
    })
  }

  public static async getUserProfile(userId: string): Promise<Result<Domain.UserSelf, AppError>> {
    return ChatKitServer.guard(async function getUserProfile() {
      const {
        GetUserProfile: { Endpoint, Method }
      } = Routes
      const { data } = await ChatKitServer.httpClient({
        url: Endpoint(userId),
        method: Method
      })
      return UserMapper.toUserSelfDomain(data as DTO.UserSelf)
    })
  }

  public static async updateUserProfileInfo(params: {
    userId: string
    firstName?: string
    lastName?: string
    email?: string
    refId?: string | null
  }): Promise<Result<boolean, AppError>> {
    const { userId, ...body } = params
    return ChatKitServer.guard(async function updateUserProfileInfo() {
      const {
        UpdateUserProfileInfo: { Endpoint, Method }
      } = Routes
      await ChatKitServer.httpClient({
        url: Endpoint(userId),
        method: Method,
        data: body
      })
      return true
    })
  }

  public static async updateUserProfileImage(params: {
    userId: string
    profileImageId: string
  }): Promise<Result<boolean, AppError>> {
    const { userId, ...body } = params
    return ChatKitServer.guard(async function updateUserProfileImage() {
      const {
        UpdateUserProfileImage: { Endpoint, Method }
      } = Routes
      await ChatKitServer.httpClient({
        url: Endpoint(userId),
        method: Method,
        data: body
      })
      return true
    })
  }

  public static async removeUserProfileImage(userId: string): Promise<Result<boolean, AppError>> {
    return ChatKitServer.guard(async function removeUserProfileImage() {
      const {
        UpdateUserProfileImage: { Endpoint, Method }
      } = Routes
      await ChatKitServer.httpClient({
        url: Endpoint(userId),
        method: Method
      })
      return true
    })
  }

  public static async disableUser(userId: string): Promise<Result<boolean, AppError>> {
    return ChatKitServer.guard(async function disableUser() {
      const {
        DisableUser: { Endpoint, Method }
      } = Routes
      await ChatKitServer.httpClient({
        url: Endpoint(userId),
        method: Method
      })
      return true
    })
  }

  public static async enableUser(userId: string): Promise<Result<boolean, AppError>> {
    return ChatKitServer.guard(async function enableUser() {
      const {
        EnableUser: { Endpoint, Method }
      } = Routes
      await ChatKitServer.httpClient({
        url: Endpoint(userId),
        method: Method
      })
      return true
    })
  }

  public static async deleteUser(userId: string): Promise<Result<boolean, AppError>> {
    return ChatKitServer.guard(async function deleteUser() {
      const {
        DeleteUser: { Endpoint, Method }
      } = Routes
      await ChatKitServer.httpClient({
        url: Endpoint(userId),
        method: Method
      })
      return true
    })
  }

  /**
   * Conversation APIs
   */

  public static async getConversationConversableUsers(params: {
    conversationId: string
    offset?: number
    limit?: number
    search?: string
    includeRefIdInSearch?: boolean
  }): Promise<Result<Domain.ConversationConversableUsersBatch, AppError>> {
    const { conversationId, ...queryString } = params
    return ChatKitServer.guard(async function getConversationConversableUsers() {
      const {
        GetConversationConversableUsers: { Endpoint, Method }
      } = Routes
      const { data } = await ChatKitServer.httpClient({
        url: Endpoint(conversationId),
        method: Method,
        params: queryString
      })
      return ConversationMapper.toConversationConversableUsersBatchDomain(
        data as DTO.ConversationConversableUsersBatch
      )
    })
  }

  public static async getConversationUsers(params: {
    conversationId: string
    offset?: number
    limit?: number
    search?: string
    type?: 'all' | 'current' | 'default'
    includeRefIdInSearch?: boolean
  }): Promise<Result<Domain.ConversationUsersBatch, AppError>> {
    const { conversationId, type = 'current', ...otherParams } = params
    return ChatKitServer.guard(async function getConversationUsers() {
      const {
        GetConversationUsers: { Endpoint, Method }
      } = Routes
      const { data } = await ChatKitServer.httpClient({
        url: Endpoint(conversationId),
        method: Method,
        params: {
          ...otherParams,
          type
        }
      })
      return ConversationMapper.toConversationUsersBatchDomain(data as DTO.ConversationUsersBatch)
    })
  }

  public static async getConversationBasicUsers(params: {
    conversationId: string
    type?: 'all' | 'current' | 'default'
  }): Promise<Result<Domain.ConversationUsersBasic[], AppError>> {
    const { conversationId, type = 'current' } = params
    return ChatKitServer.guard(async function getConversationUsers() {
      const {
        GetConversationBasicUsers: { Endpoint, Method }
      } = Routes
      const { data } = await ChatKitServer.httpClient({
        url: Endpoint(conversationId),
        method: Method,
        params: {
          type
        }
      })
      return data.map(UserMapper.toConversationUserBasicDomain) as Domain.ConversationUsersBasic[]
    })
  }

  public static async addUsersToConversation(params: {
    conversationId: string
    memberIds: string[]
    force?: boolean
  }): Promise<Result<boolean, AppError>> {
    const { conversationId, force, memberIds } = params
    return ChatKitServer.guard(async function addUsersToConversation() {
      const {
        AddUsersToConversation: { Endpoint, Method }
      } = Routes
      await ChatKitServer.httpClient({
        url: Endpoint(conversationId),
        method: Method,
        data: {
          memberIds
        },
        params: {
          force
        }
      })
      return true
    })
  }

  public static async removeUsersFromConversation(params: {
    conversationId: string
    memberIds: string
    force?: boolean
  }): Promise<Result<boolean, AppError>> {
    const { conversationId, force, memberIds } = params
    return ChatKitServer.guard(async function removeUsersToConversation() {
      const {
        RemoveUsersFromConversation: { Endpoint, Method }
      } = Routes
      await ChatKitServer.httpClient({
        url: Endpoint(conversationId),
        method: Method,
        data: {
          memberIds
        },
        params: {
          force
        }
      })
      return true
    })
  }

  public static async makeConversationOwner(params: {
    conversationId: string
    userId: string
  }): Promise<Result<boolean, AppError>> {
    const { conversationId, userId } = params
    return ChatKitServer.guard(async function makeConversationOwner() {
      const {
        MakeConversationOwner: { Endpoint, Method }
      } = Routes
      await ChatKitServer.httpClient({
        url: Endpoint(conversationId, userId),
        method: Method
      })
      return true
    })
  }

  public static async removeConversationOwner(params: {
    conversationId: string
    userId: string
  }): Promise<Result<boolean, AppError>> {
    const { conversationId, userId } = params
    return ChatKitServer.guard(async function removeConversationOwner() {
      const {
        RemoveConversationOwner: { Endpoint, Method }
      } = Routes
      await ChatKitServer.httpClient({
        url: Endpoint(conversationId, userId),
        method: Method
      })
      return true
    })
  }

  public static async deleteAllUsersInConversation(params: {
    conversationId: string
  }): Promise<Result<boolean, AppError>> {
    const { conversationId } = params
    return ChatKitServer.guard(async function deleteAllUsersInConversation() {
      const {
        DeleteAllUsersInConversation: { Endpoint, Method }
      } = Routes
      await ChatKitServer.httpClient({
        url: Endpoint(conversationId),
        method: Method
      })
      return true
    })
  }

  public static async createGroupWithOwner(params: {
    groupName: string
    groupImageId: string | null
    memberIds: string[]
    ownerId: string
    serverManaged?: boolean
  }): Promise<Result<Domain.GroupConversation, AppError>> {
    return ChatKitServer.guard(async function removeConversationOwner() {
      const {
        CreateGroupWithOwner: { Endpoint, Method }
      } = Routes
      const { data } = await ChatKitServer.httpClient({
        url: Endpoint,
        method: Method,
        data: params
      })
      return ConversationMapper.toGenericConversationDomain(
        data as DTO.GroupConversation
      ) as Domain.GroupConversation
    })
  }

  public static async updateGroupInfo(params: {
    conversationId: string
    groupName: string
  }): Promise<Result<boolean, AppError>> {
    const { conversationId, ...body } = params
    return ChatKitServer.guard(async function updateGroupInfo() {
      const {
        UpdateGroupInfo: { Endpoint, Method }
      } = Routes
      await ChatKitServer.httpClient({
        url: Endpoint(conversationId),
        method: Method,
        data: body
      })
      return true
    })
  }

  public static async updateGroupImage(params: {
    conversationId: string
    groupImageId: string
  }): Promise<Result<boolean, AppError>> {
    const { conversationId, ...body } = params
    return ChatKitServer.guard(async function updateGroupImage() {
      const {
        UpdateGroupImage: { Endpoint, Method }
      } = Routes
      await ChatKitServer.httpClient({
        url: Endpoint(conversationId),
        method: Method,
        data: body
      })
      return true
    })
  }

  public static async removeGroupImage(conversationId: string): Promise<Result<boolean, AppError>> {
    return ChatKitServer.guard(async function removeGroupImage() {
      const {
        RemoveGroupImage: { Endpoint, Method }
      } = Routes
      await ChatKitServer.httpClient({
        url: Endpoint(conversationId),
        method: Method
      })
      return true
    })
  }

  /**
   * Message APIs
   */
  public static async getMessages(params: {
    beforeSequence?: string
    limit?: number
    galleryOnly?: boolean
    includeDeleted?: boolean
    userId?: string
    fromDate?: Date
    toDate?: Date
    conversationType?: ConversationType
    search?: string
    conversationId?: string
  }): Promise<Result<Domain.Message[], AppError>> {
    return ChatKitServer.guard(async function getMessages() {
      const {
        GetMessages: { Endpoint, Method }
      } = Routes
      const { data } = await ChatKitServer.httpClient({
        url: Endpoint,
        method: Method,
        params: {
          ...params,
          fromDate: params.fromDate?.toISOString(),
          toDate: params.toDate?.toISOString()
        }
      })
      return data.map(MessageMapper.toMessageDomain) as Domain.Message[]
    })
  }

  public static async deleteMessage(
    messageId: string
  ): Promise<Result<Domain.MessageDeleted, AppError>> {
    return ChatKitServer.guard(async function deleteMessage() {
      const {
        DeleteMessage: { Endpoint, Method }
      } = Routes
      const { data } = await ChatKitServer.httpClient({
        url: Endpoint(messageId),
        method: Method
      })
      return MessageMapper.toMessageDeletedDomain(data)
    })
  }

  /**
   * Media APIs
   */
  public static async getGroupProfileImage<
    I extends boolean = false,
    R extends string | ReadStream = I extends true ? ReadStream : string
  >(options: { conversationId: string; stream?: I }): Promise<Result<R, AppError>> {
    return ChatKitServer.guard(async function getGroupProfileImage() {
      const {
        GetConversationGroupImage: { Endpoint, Method }
      } = Routes
      const url = Endpoint(options.conversationId)
      if (options.stream) {
        const data = await ChatKitServer.streamFile(url, Method)
        return data as R
      }
      const { data } = await ChatKitServer.httpClient({
        url,
        method: Method,
        params: {
          noRedirect: true
        }
      })
      return data.url as R
    })
  }

  public static async getFileMessage<
    I extends boolean = false,
    R extends string | ReadStream = I extends true ? ReadStream : string
  >(options: {
    messageId: string
    fileLocalIdName: string
    stream?: I
  }): Promise<Result<R, AppError>> {
    return ChatKitServer.guard(async function getGroupProfileImage() {
      const {
        GetFileMessage: { Endpoint, Method }
      } = Routes
      const url = Endpoint(options.messageId, options.fileLocalIdName)
      if (options.stream) {
        const data = await ChatKitServer.streamFile(url, Method)
        return data as R
      }
      const { data } = await ChatKitServer.httpClient({
        url,
        method: Method,
        params: {
          noRedirect: true
        }
      })
      return data.url as R
    })
  }

  public static async getVideoThumbnail<
    I extends boolean = false,
    R extends string | ReadStream = I extends true ? ReadStream : string
  >(options: {
    messageId: string
    fileLocalIdName: string
    stream?: I
  }): Promise<Result<R, AppError>> {
    return ChatKitServer.guard(async function getGroupProfileImage() {
      const {
        GetFileMessage: { Endpoint, Method }
      } = Routes
      const url = Endpoint(options.messageId, options.fileLocalIdName)
      if (options.stream) {
        const data = await ChatKitServer.streamFile(url, Method)
        return data as R
      }
      const { data } = await ChatKitServer.httpClient({
        url,
        method: Method,
        params: {
          noRedirect: true
        }
      })
      return data.url as R
    })
  }
}
