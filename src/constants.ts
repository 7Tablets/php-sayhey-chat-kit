import { HttpMethod } from './types'

export const API_KEY_NAME = 'x-sayhey-server-api-key'
export const API_VERSION = 'v1'

export const Routes = {
  GetDefaultProfileImagesForGroup: {
    Endpoint: '/default-group-images',
    Method: HttpMethod.Get
  },
  ViewDefaultProfileImageForGroup: {
    Endpoint: (filename: string, queryString: string) =>
      `/default-group-images/${filename}${queryString}`,
    Method: HttpMethod.Get
  },
  SignUpUser: {
    Endpoint: '/users/server-sign-up',
    Method: HttpMethod.Post
  },
  SignInUserWithEmail: {
    Endpoint: '/users/server-sign-in',
    Method: HttpMethod.Post
  },
  SignInUserWithId: {
    Endpoint: '/users/server-sign-in-id',
    Method: HttpMethod.Post
  },
  GetUserProfile: {
    Endpoint: (userId: string) => `users/${userId}/profile-info`,
    Method: HttpMethod.Get
  },
  UpdateUserProfileInfo: {
    Endpoint: (userId: string) => `users/${userId}/profile-info`,
    Method: HttpMethod.Put
  },
  UpdateUserProfileImage: {
    Endpoint: (userId: string) => `users/${userId}/profile-image`,
    Method: HttpMethod.Put
  },
  RemoveUserProfileImage: {
    Endpoint: (userId: string) => `users/${userId}/profile-image`,
    Method: HttpMethod.Delete
  },
  UploadFile: {
    Endpoint: '/files',
    Method: HttpMethod.Post
  },
  GetConversationConversableUsers: {
    Endpoint: (conversationId: string) => `conversations/${conversationId}/conversable-users`,
    Method: HttpMethod.Get
  },
  GetConversationUsers: {
    Endpoint: (conversationId: string) => `conversations/${conversationId}/users`,
    Method: HttpMethod.Get
  },
  AddUsersToConversation: {
    Endpoint: (conversationId: string) => `/conversations/${conversationId}/users`,
    Method: HttpMethod.Post
  },
  RemoveUsersFromConversation: {
    Endpoint: (conversationId: string) => `/conversations/${conversationId}/delete-users`,
    Method: HttpMethod.Post
  },
  MakeConversationOwner: {
    Endpoint: (conversationId: string, userId: string) =>
      `conversations/${conversationId}/owners/${userId}`,
    Method: HttpMethod.Put
  },
  RemoveConversationOwner: {
    Endpoint: (conversationId: string, userId: string) =>
      `conversations/${conversationId}/owners/${userId}`,
    Method: HttpMethod.Delete
  },
  DeleteAllUsersInConversation: {
    Endpoint: (conversationId: string) => `/conversations/${conversationId}/delete-all-users`,
    Method: HttpMethod.Post
  },
  CreateGroupWithOwner: {
    Endpoint: '/conversations/group-with-owner',
    Method: HttpMethod.Post
  },
  UpdateGroupInfo: {
    Endpoint: (conversationId: string) => `conversations/${conversationId}/group-info`,
    Method: HttpMethod.Put
  },
  UpdateGroupImage: {
    Endpoint: (conversationId: string) => `/conversations/${conversationId}/group-image`,
    Method: HttpMethod.Put
  },
  RemoveGroupImage: {
    Endpoint: (conversationId: string) => `/conversations/${conversationId}/group-image`,
    Method: HttpMethod.Delete
  },
  GetConversationBasicUsers: {
    Endpoint: (conversationId: string) => `/conversations/${conversationId}/basic-users`,
    Method: HttpMethod.Get
  },
  DisableUser: {
    Endpoint: (userId: string) => `/users/${userId}/disabled`,
    Method: HttpMethod.Put
  },
  EnableUser: {
    Endpoint: (userId: string) => `/users/${userId}/disabled`,
    Method: HttpMethod.Delete
  },
  GetMessages: {
    Endpoint: '/messages',
    Method: HttpMethod.Get
  },
  DeleteMessage: {
    Endpoint: (messageId: string) => `/messages/${messageId}`,
    Method: HttpMethod.Delete
  },
  DeleteUser: {
    Endpoint: (userId: string) => `/users/${userId}`,
    Method: HttpMethod.Delete
  },
  GetConversationGroupImage: {
    Endpoint: (conversationId: string) => `/conversations/${conversationId}/group-image`,
    Method: HttpMethod.Get
  },
  GetFileMessage: {
    Endpoint: (messageId: string, fileLocalIdName: string) =>
      `/messages/${messageId}/files/${fileLocalIdName}`,
    Method: HttpMethod.Get
  },
  GetVideoThumbnail: {
    Endpoint: (messageId: string, fileLocalIdName: string) =>
      `/messages/${messageId}/files/video-thumbnails/${fileLocalIdName}`,
    Method: HttpMethod.Get
  }
}
