import { DTO, Domain } from './types'

export class AppMapper {
  static toProfileImageDomainList = (
    imageList: DTO.DefaultGroupProfileImage[]
  ): Domain.DefaultGroupProfileImage[] => {
    return [...imageList]
  }

  static toAuthDomain = (authDetails: DTO.Auth): Domain.Auth => {
    return { ...authDetails }
  }
}

export class UserMapper {
  static toUserDomain = (user: DTO.User): Domain.User => {
    return {
      ...user,
      deletedAt: user.deletedAt ? new Date(user.deletedAt) : null,
      picture: user.picture ? MediaMapper.toMediaDomain(user.picture) : null
    }
  }

  static toConversationUserDomain = (user: DTO.ConversationUser): Domain.ConversationUser => {
    return {
      ...user,
      ...UserMapper.toUserDomain(user),
      deletedFromConversationAt: user.deletedFromConversationAt
        ? new Date(user.deletedFromConversationAt)
        : null
    }
  }

  static toUserSelfDomain = (user: DTO.UserSelf): Domain.UserSelf => {
    return {
      ...UserMapper.toUserDomain(user),
      muteAllUntil: user.muteAllUntil ? new Date(user.muteAllUntil) : null
    }
  }

  static toConversationUserBasicDomain = (
    user: DTO.ConversationUsersBasic
  ): Domain.ConversationUsersBasic => {
    const { deletedFromConversationAt, userDeletedAt } = user
    return {
      ...user,
      deletedFromConversationAt: deletedFromConversationAt
        ? new Date(deletedFromConversationAt)
        : null,
      userDeletedAt: userDeletedAt ? new Date(userDeletedAt) : null
    }
  }
}

export class MediaMapper {
  static toMediaDomain = (media: DTO.Media): Domain.Media => {
    return {
      ...media
    }
  }
}

export class MessageMapper {
  static toMessageDomain = (message: DTO.Message): Domain.Message => {
    const { createdAt, sender, file, deletedAt } = message
    return {
      ...message,
      createdAt: new Date(createdAt),
      sender: sender ? UserMapper.toUserDomain(sender) : null,
      file: file ? MediaMapper.toMediaDomain(file) : null,
      deletedAt: deletedAt ? new Date(deletedAt) : null
    }
  }

  static toMessageDeletedDomain = (messageEvent: DTO.MessageDeleted): Domain.MessageDeleted => {
    return {
      ...messageEvent,
      deletedAt: new Date(messageEvent.deletedAt)
    }
  }
}

export class ConversationMapper {
  static toConversationConversableUsersBatchDomain = (
    batch: DTO.ConversationConversableUsersBatch
  ): Domain.ConversationConversableUsersBatch => {
    return {
      ...batch,
      results: batch.results.map(UserMapper.toUserDomain)
    }
  }

  static toConversationUsersBatchDomain = (
    batch: DTO.ConversationUsersBatch
  ): Domain.ConversationUsersBatch => {
    return {
      ...batch,
      results: batch.results.map(UserMapper.toConversationUserDomain)
    }
  }

  static toGroupConversationInfoDomain = (
    info: DTO.GroupConversationInfo
  ): Domain.GroupConversationInfo => {
    return {
      ...info
    }
  }

  static toIndividualConversationInfoDomain = (
    info: DTO.UserConversationInfo
  ): Domain.UserConversationInfo => {
    return {
      ...info
    }
  }

  static toGenericConversationDomain = (
    conversation: DTO.GenericConversation
  ): Domain.GenericConversation => {
    const {
      muteUntil,
      endBefore,
      deletedFromConversationAt,
      createdAt,
      updatedAt,
      pinnedAt,
      startAfter,
      lastMessage
    } = conversation
    const common = {
      muteUntil: muteUntil ? new Date(muteUntil) : null,
      endBefore: endBefore ? new Date(endBefore) : null,
      deletedFromConversationAt: deletedFromConversationAt
        ? new Date(deletedFromConversationAt)
        : null,
      createdAt: new Date(createdAt),
      updatedAt: new Date(updatedAt),
      pinnedAt: pinnedAt ? new Date(pinnedAt) : null,
      startAfter: startAfter ? new Date(startAfter) : null,
      lastMessage: lastMessage ? MessageMapper.toMessageDomain(lastMessage) : null
    }
    if (conversation instanceof DTO.IndividualConversation) {
      return {
        ...conversation,
        ...common,
        user: ConversationMapper.toIndividualConversationInfoDomain(conversation.user)
      }
    }
    return {
      ...conversation,
      ...common,
      group: ConversationMapper.toGroupConversationInfoDomain(conversation.group)
    }
  }
}
